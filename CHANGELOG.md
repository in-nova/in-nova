### v0.5.0 ###
**Signal Processing - Generic**\
\* Fix bug returning wrong length filter when negative delays are use in `winSincFracDel()`.

**Signal Processing - Measurements**\
\* Fix error in `invSweep()` throwing an error when the `causIrLen` input argument was not provided.

**Signal Processing - Array Processing**\
\+ Add function `arrManTd()` to calculate the time domain equivalent of the *array manifold* of an arbitrary array.\
\+ Add function `firstOrderDmaTd()` to calculate the FIR filter and filtered output(s) for a first order Differential Microphone Array (DMA) in the time domain.\
\* Changed the name of `firstOrderDMA()` to `firstOrderDma()` (cameltoe). This change **breaks** backward compatibility!\
\* Modify `firstOrderDma()` to return the filters as the first argument, in order to avoid calculating the output if it is not required.\
\* Correct error in `firstOrderDma()` header (documentation) and changed name of local array manifold calculation function to clarify that a local function is called.\
\* Correct erroneous output calculations in `firstOrderDma()`.\
\* Moved the `input` input argument to the last of the input arguments list in `firstOrderDma()` and updated the input argument checks. This change **breaks backward compatibility**.

**Optimisation - Memetic Algorithm**\
\* Removed redundant code to handle serial for-loop if parfor is not available.

**Sound Fields**\
\+ Add `lineSrcField()` to calculate the sound field due to line sources in the frequency domain (tonal).
\* Parallelize the for-loops in `ptSrcField()` and `lineSrcField()`. While execution may be slower for a small number of sources and receiver positions, the overall execution time will not increase significantly due to the functions' generally fast performance.\
\* Rename the following functions (this **breaks** backward compatibility):
  - `extrpFieldSH()` to `extrpFieldSh()`
  - `planeWaveSH()` to `planeWaveSh()`
  - `ptSrcFieldSH()` to `ptSrcFieldSh()`
  - `ptSrcFieldTD()` to `ptSrcFieldTd()`

**Utilities - Geometries**\
\* Fix error in `rcvGeo()` applying incorrect rotation to the elements in the Uniform Circular Array.\
\* Fix error in `virtMicGeo()` not returning the z coordinates of the virtual microphones in the mesh. This fix **breaks backward compatibility**.\
\* Change name of `virtMicGeo()` to `vMicGeo()`. This **breaks** backward compatibility.

**Virtual Sensing - Remote Microphone Technique**\
\* Parallelise the for-loops in `obsFiltEst()`. While execution may be slower for a small number of sources and receiver positions, the overall execution time will not increase significantly due to the functions' generally fast performance.\
\* Parallelise on of the for-loops in `obsFiltTD()`. While execution may be slower for a small number of sources and receiver positions, the overall execution time will not increase significantly due to the functions' generally fast performance.\
\* Update `obsFilt()` to return the condition of the perturbed power spectral density matrix.
\* Rename the following functions (this **breaks** backward compatibility):
  - `obsFiltTD()` to `obsFiltTd()`
  - `obsFiltEstTD()` to `obsFiltEstTd()`

-------------------------------------------


### v0.4.1 ###
** Optimisation - Memetic Algorithm**\
\* Update `mutPop()` to use the parallel for-loop (`parfor`) whenever it is available.\
\* Update `selParents()` to use the parallel for-loop (`parfor`) whenever it is available.

-------------------------------------------


### v0.4.0 ###
**Utilities - Generic**\
\* Change the interface of `rotMat3d()`, so that the last argument is a boolean (`logical` in MATLAB), declaring whether the given angles are in degrees (if false, the angles are treated as radians). This is a **backwards incompatible** change.\
\* The following functions are changed internally to apply the changes in `rotMat3d()`:
  - `rcvGeo()`
  - `srcGeo()`
  - `virtMicGeo()`

-------------------------------------------


### v0.3.2 ###
**Signal Processing - Generic**\
\+ Add option to use negative delay values with the `winSincFracDel()` function.

**Signal Processing - Array Processing**\
\* Fix error in `arrMan()` in the inner product of the source directions matrix and the sensor position vectors.\
\* Fix error in `arrMan()` where the returned array manifold had incorrect dimensions. It was $N_{\textrm{d}} \times N_{\textrm{m}} \times N_{\textrm{k}}$, where $N_{\textrm{d}}$ is the number of source directions, $N_{\textrm{m}}$ the number of sensors and $N_{\textrm{k}}$ the number of frequencies. Now it is $N_{\textrm{m}} \times N_{\textrm{d}} \times N_{\textrm{k}}$.

**Utilities - Geometries**\
\* Fix bug in `virtMicGeo()` where translation and rotation were not applied to the geometry mesh coordinates array.

**Virtual Sensing - Remote Microphone Technique**\
\* Update `obsFiltEstTD()`, removing the observatoin filter replication when a single filter is provided for many trials/sound field realisations to reduce the memory footprint of the function.\
\* Update `obsFiltTD()`, performing the delay operation for the virtual microphone signals only when needed and added check for $SNR = \infty$ to calculate the SNR matrix appropriately.
\* Fix bug in `obsFiltTD()`, returning the condition number without taking into account the regularisation and SNR values.

-------------------------------------------


### v0.3.1 ###
**Virtual Sensing - Remote Microphone Technique**\
\+ Add the option to add "noise" to the monitoring microphone signals in the time domain estimation of the optimal observation filters in `obsFiltTD()` to replicate the feature of `obsFilt()`. This change **breaks** backwards compatibility.

-------------------------------------------  


### v0.3.0 ###
**General**\
\* Convert the project to accept column position and direction vectors as input. The affected functions are:
- `srcGeo()`
- `rcvGeo()`
- `virtMicGeo()`
- `arrMan()`
- `firstOrderDMA()`
- `ptsOnSphere()`
- `twoPtDist()`
- `circHarm()`
- `sphHarm()`
- `planeWave()`
- `planeWaveSH()`
- `ptSrcField()`
- `ptSrcFieldSH()`
- `ptSrcFieldTD()`

-------------------------------------------


### v0.2.8 ###
**Utilities - Generic**\
\+ Add the option to pick the order the rotations will be performed in the `rotMat3d()` function. This results is a **backward incompatible** change.

**Utilities - Geometries**\
\* Update the geometry generating functions `rcvGeo()`, `srcGeo()` and `virtMicGeo` to accommodate the changes in `rotMat3d()`.

-------------------------------------------


### v0.2.7 ###
**Signal Processing - Measurements**\
\+ Introduction of the *Measurements* "topic" in the Signal Processing part of the codebase.\
\+ Function to estimate the impulse responses from sweep measurements.

-------------------------------------------


### v0.2.6 ###
**Signal Processing - Generic**\
\+ Add function to perform fractional octave smoothing of spectra.

-------------------------------------------


### v0.2.5 ###
**Utilities - Geometries**\
\* Combine translation offsets to a single vector input argument for the `rcvGeo()` MATLAB function.\
\* Combine translation offsets to a single vector input argument for th `virtMicGeo()` MATLAB function.\
\* Add the ability to rotate the receiver geometries in `rcvGeo()` MATLAB function.

-------------------------------------------


### v0.2.4 ###
**Virtual Sensing**\
\* Corrected time-domain observation filter calculations.\
\* Removed scaling from the cross-correlation calculations in the time-domain optimal observation filters.\
\* Fixed bug in time-domain observation filter that screwed up dimensions of the filters when only one source was present.

**Sound Fields**\
\* Fixed bug in time-domain point source signal calculations that screwed up dimensions of the resulting arrays when only one source was present.

**Utilities - Geometries**\
\* The virtual microphone geometry generation function has been heavily modified and its interface has changed in a **backward incompatible** way. It is now more generic and allows for "arbitrary" size of each Cartesian dimension of the geometries as well as different number of sensors along each dimension.

-------------------------------------------


### v0.2.3 ###
**Virtual Sensing**\
\+ Added function to estimate the observation filters in the time-domain.\
\+ Added function to perform estimation with observation filters in the time-domain.

**Signal Processing - Generic**\
\* Fixed bugs, improved calculations and removed output arguments for the windowed sinc FIR fractional delay filters
\+ Added four window functions in the calculation of windowed sinc FIR fractional delay filters.\

**Sound Fields**\
\+ Added function to calculate signals generated by ideal point sources in the time-domain.

-------------------------------------------


### v0.2.2 ###
**Virtual Sensing**\
\* Update the observation filter and estimation with observation filter functions with better input argument checking and changed the name of variables to match those in the literature.

-------------------------------------------


### v0.2.1 ###
**Virtual Sensing**\
\* Fix a bug where noise was added to the power spectral density matrix for the optimal observation filter calculations in the noiseless case.

-------------------------------------------


### v0.2.0 ###
**Utilities**\
\+ Added a bisection method for single-valued functions.\
\* Improved rotation matrix calculation function with degrees and radian calculations available.\
\+ Added function to calculate a double-sided spectrum from a single-sided spectrum.\
\+ Added a function to calculate the intersection of three vectors.\
\+ Added a pair of functions to check for even or odd elements.\
\+ Added a function to pick values from a vector based on given probabilities.\
\+ Added a function to pick randomly unique rows from a matrix.\
\+ Added a Heaviside step function.\
\+ Added function to swap argument values.

**Optimisation**\
\+ Added a MATLAB Memetic Algorithm (MA) implementation.\
\* The algorithm calls provided functions and can solve generic problems.

**Control**\
\+ Added tonal control in the frequency domain.\
\* Control is contained in a single function.\
\* Can be used with or without virtual sensing.\
\* Implementations of optimal control and FxLMS (still frequency domain) calculations are available.

**Signal Processing - Array Processing**\
\+ Added array manifold (steering vector) calculation function.

**Signal Processing - Generic**\
\+ Added frequency band calculation function with 1/1 octave and 1/3 octave bands.\
\+ Add fractional delay filter impulse response generation function.

**Virtual Sensing - Generic**\
\+ Added a multiple coherence calculation function.

**Virtual Sensing - Remote Microphone Technique**\
\* Divide the observation filter and estimation with the observation filter in two functions. The return arguments are split appropriately to the function they relate.\
\+ Added option to include noise in the monitoring microphone power spectral density matrix with specified SNR value.

**Sound Fields**\
\+ Added plane wave calculation function.\
\+ Added Circular Harmonics calculation function.\
\+ Added Spherical Harmonics calculation function.\
\+ Added Discrete Circular Fourier Transform (DCFT) calculation function.\
\+ Added Inverse Discrete Circular Fourier Transform (IDCFT) calculation function.\
\+ Added Discrete Spherical Fourier Transform (DSFT) calculation function.\
\+ Added Inverse Discrete Spherical Fourier Transform (IDSFT) calculation function.\
\+ Added function to extrapolate a sound field in the Spherical Harmonics domain.\
\+ Added function to calculate sound field generated by a point source in the Spherical Harmonics domain (truncated order).\
\+ Added function to calculate sound field generated by a plane wave in the Spherical Harmonics domain (truncated order).

-------------------------------------------


### v0.1.0 ###
This is the initial version of the project.
