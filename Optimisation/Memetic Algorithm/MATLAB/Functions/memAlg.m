%% Memetic algorithm optimisation function
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 09/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Perform optimisation with a memetic algorithm.
% --------------------------------------------------
% Input arguments
%
% fitFunc [function_handle]: The function with which to calculate the
%                            fitnesss of the individuals in each
%                            generation. The function can return a single
%                            value, or a vector of values. In the second
%                            case, the function can be used to perform
%                            vectorised calculations which significantly
%                            decreases computation times.
% 
% constrFunc [function_handle]: The function to calculate whether an
%                               individual meets the constraint
%                               requirements or not. The value must return
%                               logical values (true/false). It can return
%                               a single value or a vector of values. In
%                               the second case, it can be used to perform
%                               vectorised operations which can
%                               significantly decrease computation times.
%                               It can be left empty if no constraints are
%                               imposed.
% 
% divFunc [function_handle]: The function to calculate the diversity of the
%                            population. This is used only if any of the
%                            selection mechanisms chosen in "FitAndDiv". In
%                            any other case, it is omitted and can be left
%                            empty. The function must return a scalar value
%                            or a vector with diversity values for each
%                            individual. In the latter case, vectorised
%                            computations can be used which can
%                            significantly reduce computation times.
% 
% nVars [numeric]: The number of variables in each individual's DNA.
% 
% divWeight [numeric] (Optional): The weight to be applied to the diversity
%                                 value of the individuals. This value is
%                                 used to balance the focus towards the
%                                 fitness function or diversity. If left
%                                 empty (or set equalt to 1) the scale of
%                                 the diversity will change to match that
%                                 of the fitness. This value has to be non
%                                 negative. [Default: 1].
% 
% varRange [numeric] (Optional): This is a 2xnVars vector containing the
%                                minimum and maximum values of the
%                                variables.
%                                [Default: ones(2, nVars) * [-1; 1]].
% 
% popSize [numeric] (Optional): The size of the population. If "initPop" is
%                               not empty, this argument is ignored and the
%                               population size equals the number of rows
%                               in "initPop". [Default: 100].
% 
% initPop [numeric] (Optional): The initial population. If provided, the
%                               number of columns must be equal to "nVars".
%                               [Default: []].
% 
% xOverConstr [char/string] (Optional): This variables declares how the
%                                       individuals that do not meet the
%                                       constraints are treated when the
%                                       "parents" for the crossover process
%                                       are picked. Possible values are
%                                       "Discard" and "Replace". In the
%                                       former case, the individuals are
%                                       discarded and are not included in
%                                       the population (like Spartans
%                                       throwing their babies to the Keadas
%                                       pit). When "Replace" is chosen, new
%                                       individuals will be picked until
%                                       none of them violates the
%                                       constraints (see Notes for more
%                                       information). If the "constrFunc"
%                                       is empty, this argument is ignored.
%                                       [Default: Discard]
% 
% mutPopConstr [char/string] (Optional): This variables declares how the
%                                       individuals that do not meet the
%                                       constraints are treated when
%                                       individuals are picked for
%                                       mutation. Possible values are
%                                       "Discard" and "Replace". In the
%                                       former case, the individuals are
%                                       discarded and are not included in
%                                       the population (like Spartans
%                                       throwing their babies to Keadas
%                                       pit). When "Replace" is chosen, new
%                                       individuals will be picked until
%                                       none of them violates the
%                                       constraints (see Notes for more
%                                       information). If the "constrFunc"
%                                       is empty, this argument is ignored.
%                                       [Default: Discard].
% 
% mutOffsprConstr [char/string] (Optional): Same as "mutPopConstr" but this
%                                           refers to the treatment of the
%                                           mutated offsprings.
%                                           [Default: Discard].
% 
% locSearchConstr [char/string] (Optional): This variables declares how the
%                                           individuals that do not meet
%                                           the constraints are treated
%                                           when individuals are picked for
%                                           local search. Possible values
%                                           are "Discard" and "Replace". In
%                                           the former case, the
%                                           individuals are discarded and
%                                           are not included in the
%                                           population (like Spartans
%                                           throwing their babies to Keadas
%                                           pit). When "Replace" is chosen,
%                                           new individuals will be picked
%                                           until none of them violates the
%                                           constraints (see Notes for more
%                                           information). If the
%                                           "constrFunc" is empty, this
%                                           argument is ignored.
%                                           [Default: Discard]
% 
% newGenConstr [char/string] (Optional): This variables declares how the
%                                        individuals that do not meet the
%                                        constraints are treated when
%                                        individuals are picked to pass to
%                                        the new generation. Possible
%                                        values are "Discard" and
%                                        Replace". In the former case, the
%                                        individuals are discarded and are
%                                        not included in the population
%                                        (like Spartans throwing their
%                                        babies to Keadas pit). When
%                                        "Replace" is chosen, new
%                                        individuals will be picked until
%                                        none of them violates the
%                                        constraints (see Notes for more
%                                        information). If the "constrFunc"
%                                        is empty, this argument is
%                                        ignored. [Default: Discard]
% 
% xOverPerc [numeric] (Optional): The percentage of the population to
%                                 undergoe the crossover operation.
%                                 [Default: 30].
% 
% xOverSel [char/string] (Optional): The method used to select the
%                                    population to undergo the crossover
%                                    process. Available choices are
%                                    "Random", "Roulette", "RankSpace" and
%                                    "FitAndDiv". For more information on
%                                    the methods see the accompanying
%                                    documentation. [Default: "Roulette"].
% 
% xOverRankSpace [numeric] (Optional): This is the probability set to the
%                                      fittest individual when "RankSpace"
%                                      is selected as "xOverSel". For all
%                                      other mechanisms this argument is
%                                      ignored. [Default: 10].
% 
% nParents [numeric] (Optional): The number of individuals that will
%                                contribute to the crossover process from
%                                which a new individual will result.
%                                [Default: 2].
% 
% xOverProb [numeric] (Optional): This is an (M - 1)xK matrix where M is
%                                 the number of variables per individual
%                                 and K is the number of parents per
%                                 offspring. Each row corresponds to the
%                                 probabilities of each position being
%                                 chosen as crossover point. For example,
%                                 with a 3 variable DNA, the probabilities
%                                 could be [40, 60], stating that there
%                                 is a probability of 40% the crossover
%                                 point being after the first element and
%                                 60% being after the second element. The
%                                 probabilities at each row must sum up to
%                                 1 with a tolerance of about 1000 * eps.
%                                 For an explanation of how crossover
%                                 points are chosen when multiple parents
%                                 are used see the notes below. This
%                                 argument can also be a column vector
%                                 with increasing integer values in the
%                                 range [1, M - 1]. These then correspond
%                                 to the crossover points for each parent
%                                 and are constant for all offsprings.
%                                 [Default: ones(M, K)/M].
% 
% mutPopPerc [numeric] (Optional): The percentage of the population to
%                                  undergoe the mutation operation.
%                                  [Default: 30].
% 
% mutPopSel [char/string] (Optional): The method used to select the
%                                     population to undergo the mutation
%                                     process. Available choices are
%                                     "Random", "Roulette", "RankSpace" and
%                                     "FitAndDiv". For more information on
%                                     the methods see the accompanying
%                                     documentation. [Default: "Roulette"].
% 
% mutPopRankSpace [numeric] (Optional): This is the probability set to the
%                                       fittest individual when "RankSpace"
%                                       is selected as "mutSel". For all
%                                       other mechanisms this argument is
%                                       ignored. [Default: 10].
% 
% mutPopProb [numeric] (Optional): The probability of mutating each
%                                  variable in an individual's DNA. This
%                                  argument must be either a vector with a
%                                  maximum number of elements equal to the
%                                  variables in an individual, or a scalar.
%                                  If a vector is passed, the elements must
%                                  lie in the range [0, 100], denoting the
%                                  probability of this variable to be
%                                  mutated. When the number of elements is
%                                  smaller than the variables in an
%                                  individual's DNA, the last elements are
%                                  filled with zeros. In case the argument
%                                  is a scalar, it denotes the maximum
%                                  number of variables that will be mutated
%                                  in an individual. If the value is
%                                  negative, exactly this number of
%                                  variables will be mutated. The variables
%                                  to be mutated are chosen at random from
%                                  a uniform distribution. [Default: -1].
% 
% mutPopVals [numeric] (Optional): This argument must be either a vector
%                                  with N elements or a 2xN matrix with N
%                                  denoting the number of variables in an
%                                  individual's DNA or a scalar. In the
%                                  case the argument is a matrix, the two
%                                  rows denote the minimum and maximum
%                                  values to be added to the mutated
%                                  variable (corresponding to each column).
%                                  The vector is a special case where the
%                                  one of the values is 0 (either the
%                                  maximum or the minimum, which depends on
%                                  the values of the elements of the
%                                  vector). If a scalar is passed,
%                                  the value added to each mutated variable
%                                  will lie between 0 and this value drawn
%                                  from a uniform distribution.
%                                  [Default: 1].
% 
% mutOffsprPerc [numeric] (Optional): The percentage of the offsprings to
%                                     undergoe the mutation operation.
%                                     [Default: 30].
% 
% mutOffsprSel [char/string] (Optional): Same as the "mutPopSel" but this
%                                        refers to the mutation of the
%                                        offsprings. [Default: "Roulette"].
% 
% mutOffsprRankSpace [numeric] (Optional): Same as the "mutPopRankSpace"
%                                          but this refers to the mutation
%                                          of the offsprings.
%                                          [Default: 10].
% 
% mutOffsprProb [numeric] (Optional): Same as the "mutPopProb" but this
%                                     refers to the mutation of the
%                                     offsprings. [Default: -1].
% 
% mutOffsprVals [numeric] (Optional): Same as the "mutPopVals" but this
%                                     refers to the mutation of the
%                                     offsprings. [Default: 1].
% 
% locSearchPerc [numeric] (Optional): Percentage of population that will
%                                     undergo local search improvement
%                                     operation. [Default: 5]
% 
% locSearchSel [char/string] (Optional): The method used to select the
%                                        population to undergo the
%                                        local search operation.
%                                        Available choices are "Random",
%                                        "Roulette", "RankSpace",
%                                        "FitAndDiv" and "Best". For
%                                        more information on the methods
%                                        see the accompanying
%                                        documentation.
%                                        [Default: "Best"].
% 
% locSearchRankSpace [numeric] (Optional): This is the probability set to
%                                          the fittest individual when
%                                          "RankSpace" is selected as
%                                          "locSearchSel". For all other
%                                          mechanisms this argument is
%                                          ignored. [Default: 10].
% 
% locSearchFreq [numeric] (Optional): This argument declares every how
%                                     many generations local search
%                                     operation will be performed. Set
%                                     to 0 if no local search is to be
%                                     performed. [Default: 10].
% 
% locSearchMeth [char/string] (Optional): This is the method to be used
%                                         to pick the neighbouring
%                                         points in the local search.
%                                         The available options are
%                                         "Full" and "Greedy". When
%                                         "Full" is picked,
%                                         "nNeighbourPts" are selected
%                                         and the one that provides the
%                                         highest improvement in the
%                                         fitness value will be selected
%                                         as the new individual, acting
%                                         as the starting point for the
%                                         new iteration (or final best
%                                         if no iterations left). If
%                                         "Greedy" is picked, when an
%                                         individual that performs
%                                         better than the current one is
%                                         found the iteration is
%                                         finished and this new value
%                                         act as the starting point for
%                                         the new iteration (or final
%                                         best if no iterations left).
%                                         [Default: "Full"].
% 
% nNeighbourPts [numeric] (Optional): Number of neighbouring points to
%                                     investigate for a better value in
%                                     the local search. [Default: 100].
% 
% nIters [numeric] (Optional): Number of iterations to be performed.
%                              The iterations will continue until
%                              "nIters" have been performed or the
%                              neighbouring individuals do not
%                              outperform the current one. [Default: 100].
% 
% stepSize [numeric] (Optional): The maximum and minimum step size(s) in
%                                each direction/variable. This must be
%                                either a scalar, a two element vector,
%                                a vector with number of elements equal
%                                to the variables in each individual, or
%                                a 2xN matrix with N denoting the number
%                                of variables in each individual. When a
%                                scalar is provided, it denotes the
%                                fixed step size in all directions. A
%                                vector with two elements provides the
%                                minimum and maximum step sizes for all
%                                direction. A vector with N elements
%                                provides the fixed step for each
%                                direction. Finally, a 2xN matrix
%                                provides the minimum and maximum step
%                                for each direction. [Default: 0.1].
% 
% newGenSel [char/string] (Optional): The method used to select the
%                                     population that will progress to the
%                                     next generation. Available choices
%                                     are "Random", "Roulette", "RankSpace"
%                                     and "FitAndDiv". For more information
%                                     on the methods see the accompanying
%                                     documentation. [Default: "Roulette"].
% 
% newGenRankSpace [numeric] (Optional): This is the probability set to
%                                       the fittest individual when
%                                       "RankSpace" is selected as
%                                       "newGenSel". For all other
%                                       mechanisms this argument is
%                                       ignored. [Default: 10].
% 
% elitePerc [numeric] (Optional): The percentage of the fittest in the
%                                 final population that will pass directly
%                                 to the new generation. [Default: 5].
% 
% resetFreq [numeric] (Optional): After how many generations the population
%                                 will be reset. This can help getting out
%                                 of a local minimum. Set to 0 if you don't
%                                 want the population to be reset.
%                                 [Default: 0]
% 
% maxGens [numeric] (Optional): The maximum number of generations.
%                               [Default: 200].
% 
% maxTime [numeric] (Optional): The maximum number of time for the
%                               optimisation to be run in seconds. If the
%                               algorithm is to run indefinitely (must be
%                               stopped with one of the other two stop
%                               criteria) set to Inf. [Default: 600].
% 
% fitTol [numeric] (Optional): The value of the fitness change for the
%                              fitness to be considered to have changed
%                              between generations. [Default: 0.1].
% 
% fitTolGenCount [numeric] (Optional): The number of generations after
%                                      which the optimisation will stop if
%                                      the change of the best fitness value
%                                      has not exceeded the "fitTol" value.
%                                      [Default: 10].
% 
% fitVec [logical] (Optional): Whether the fitness function supports
%                              vectorised execution. [Default: false].
% 
% fitPar [logical] (Optional): Whether the fitness function is to be
%                              executed in parallel. If "fitVec" is set to
%                              true, this argument is ignored.
%                              [Default: false].
% 
% constrVec [logical] (Optional): Whether the constraint evaluation
%                                 function supports vectorised execution.
%                                 [Default: false].
% 
% constrPar [logical] (Optional): Whether the constraint evaluation
%                                 function supports parallel execution. If
%                                 "constrVec" is set to true, this argument
%                                 is ignored. [Default: false].
% 
% locSearchPar [logical] (Optional): Whether the local search will be
%                                    executed in parallel for each
%                                    individual. [Default: false].
% 
% plotMean [logical] (Optional): Whether to plot the mean fitness value for
%                                each generation. [Default: false].
% 
% plotMax [logical] (Optional): Whether to plot the maximum fitness value
%                               for each generation. [Default: false].
% 
% plotMin [logical] (Optional): Whether to plot the minimum fitness value
%                               for each generation. [Default: false].
% 
% --------------------------------------------------
% Output arguments
% 
% bestSol [numeric]: The individual with the best fitness function.
% 
% bestFitVal [numeric]: The fitness value of the fittest individual.
% 
% --------------------------------------------------
% Notes
% 
% Dependencies: - From the Memetic Algorithm "project":
%                   - calcFitness()
%                   - constrPop()
%                   - crossPop()
%                   - locSearch()
%                   - mutPop()
%                   - selNewGen()
%                   - selParents()
% 
% Integer vs float optimisation: There is no distinction in the type of the
% Discrete vs continuous         parameters in an individual's DNA. If
%                                integer optimisation is sought, the
%                                conversion of the values will have to be
%                                handled in the fitness function.
% 
% Individuals not meeting constraints: When an individual does not meet the
%                                      constraints it can either be
%                                      discarded or replaced. The initial
%                                      population will always result in
%                                      individuals that meet the
%                                      requirements and thus all
%                                      generations will have the same size.
%                                      Any alteration of an individual,
%                                      through mutation, crossover or local
%                                      search, can result in individuals
%                                      that do not meet the constraints. If
%                                      a replacement has to be found for
%                                      each one of them, computation times
%                                      may be significantly affected
%                                      negatively. On the other hand, if
%                                      individuals that do not meet the
%                                      constraints are discarded, the
%                                      remaining population may consist of
%                                      mainly the population of the last
%                                      generation and so the improvement
%                                      may be minor to negligible from one
%                                      generation to the next.
% 
% CAUTION: An extremely large lag can result if the new generation
%          selection method is "RankSpace" and the "newGenRankSpace" value
%          is large. This can result due to the fact that the fittest
%          individuals will have a very high probability of selection and
%          will be constantly selected and discarded due to duplication.
%          For practical reasons this can end up in an infinite loop that
%          will never allow for the optimisation to even begin.
%          On the contrary, if a very small number is chosen as
%          "newGenRankSpace" and the population is rather small, then the
%          last individual will end up with a probability of being selected
%          even higher than the fittest individual. This will result in an
%          individual with a rather constant low fitness value being
%          constantly present in the population. These issues should be
%          addressed in the future, as the rank space method seems to
%          provide very good results in initial experiments.
% 
% --------------------------------------------------
function [bestSol, bestFitVal] = memAlg(fitFunc, constrFunc, divFunc, ... % Function handles
                                                           nVars, ... % Number of variables per individual
                                                           popOpts, ... % Population options
                                                           constrOpts, ... % Contraint handling
                                                           xOverOpts, ... % Crossover options
                                                           mutOpts, ... % Mutation options
                                                           locSearchOpts, ... % Local search options options
                                                           newGenOpts, ... % New generation selection options
                                                           termOpts, ... % Termination options
                                                           execOpts, ... % Execution options
                                                           plotOpts) % Plotting options
    % ====================================================
    % Validate and set initial values of arguments
    % ====================================================
    arguments
        % Mandatory
        fitFunc (1, 1) function_handle {mustBeNonempty};
        constrFunc {mustBeFuncHandleOrEmpty(constrFunc)};
        divFunc {mustBeFuncHandleOrEmpty(divFunc)};
        nVars (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeReal, mustBeInteger, mustBeFinite, mustBeNonempty, mustBeNonNan, mustBePositive, mustBeNonempty};
        
        % Optional
        popOpts.varRange {mustBeNumeric, mustBeFinite, mustBeNonNan, mustHaveDims(popOpts.varRange, 2, nVars), mustBeNonempty, mustBeIncreasing(popOpts.varRange)} = ones(2, nVars) .* [-1; 1];
        popOpts.popSize (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeReal, mustBeFinite, mustBeInteger, mustBeNonNan, mustBePositive, mustBeNonempty} = 100;
        popOpts.initPop {mustBeNumeric, mustBeNonNan, mustHaveCols(popOpts.initPop, nVars)} = [];
        
        constrOpts.xOverConstr (1, 1) string {mustBeTextScalar, mustBeMember(constrOpts.xOverConstr, {'Discard', 'Replace'}), mustBeNonempty} = "Discard";
        constrOpts.mutPopConstr (1, 1) string {mustBeTextScalar, mustBeMember(constrOpts.mutPopConstr, {'Discard', 'Replace'}), mustBeNonempty} = "Discard";
        constrOpts.mutOffsprConstr (1, 1) string {mustBeTextScalar, mustBeMember(constrOpts.mutOffsprConstr, {'Discard', 'Replace'}), mustBeNonempty} = "Discard";
        constrOpts.locSearchConstr (1, 1) string {mustBeTextScalar, mustBeMember(constrOpts.locSearchConstr, {'Discard', 'Replace'}), mustBeNonempty} = "Discard";
        constrOpts.newGenConstr (1, 1) string {mustBeTextScalar, mustBeMember(constrOpts.newGenConstr, {'Discard', 'Replace'}), mustBeNonempty} = "Discard";

        xOverOpts.xOverPerc (1, 1) {mustBeNumeric, mustBeLessThanOrEqual(xOverOpts.xOverPerc, 100), mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 30;
        xOverOpts.xOverSel (1, 1) string {mustBeTextScalar, mustBeMember(xOverOpts.xOverSel, {'Random', 'Roulette', 'RankSpace', 'FitAndDiv'}), mustBeNonempty} = "Roulette";
        xOverOpts.xOverRankSpace (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 10;
        xOverOpts.nParents (1, 1) {mustBeNumeric, mustBeFinite, mustBeReal, mustBeInteger, mustBeGreaterThanOrEqual(xOverOpts.nParents, 2), mustBeNonempty} = 2;
        xOverOpts.xOverProb {mustBeNonnegative, mustBeReal, mustBeLessThanOrEqual(xOverOpts.xOverProb, 100), mustBeNonempty} = 100 * rand(2, nVars - 1);

        mutOpts.mutPopPerc (1, 1) {mustBeNumeric, mustBeLessThanOrEqual(mutOpts.mutPopPerc, 100), mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 30;
        mutOpts.mutPopSel (1, 1) string {mustBeTextScalar, mustBeMember(mutOpts.mutPopSel, {'Random', 'Roulette', 'RankSpace', 'FitAndDiv'}), mustBeNonempty} = "Roulette";
        mutOpts.mutPopRankSpace (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 10;
        mutOpts.mutPopProb {mustBeNumeric, mustBeReal, mustBeNonNan, mustBeFinite, mustBeNonempty, mustBeLessThanOrEqual(mutOpts.mutPopProb, 100)} = -1;
        mutOpts.mutPopVals {mustBeNumeric, mustBeFinite, mustBeNonNan, mustBeNonempty} = 1;

        mutOpts.mutOffsprPerc (1, 1) {mustBeNumeric, mustBeLessThanOrEqual(mutOpts.mutOffsprPerc, 100), mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 30;
        mutOpts.mutOffsprSel (1, 1) string {mustBeTextScalar, mustBeMember(mutOpts.mutOffsprSel, {'Random', 'Roulette', 'RankSpace', 'FitAndDiv'}), mustBeNonempty} = "Roulette";
        mutOpts.mutOffsprRankSpace (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 10;
        mutOpts.mutOffsprProb {mustBeNumeric, mustBeReal, mustBeNonNan, mustBeFinite, mustBeNonempty, mustBeLessThanOrEqual(mutOpts.mutOffsprProb, 100)} = -1;
        mutOpts.mutOffsprVals {mustBeNumeric, mustBeFinite, mustBeNonNan, mustBeNonempty} = 1;

        locSearchOpts.locSearchPerc (1, 1) {mustBeNumeric, mustBeLessThanOrEqual(locSearchOpts.locSearchPerc, 100), mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 5;
        locSearchOpts.locSearchSel (1, 1) string {mustBeTextScalar, mustBeMember(locSearchOpts.locSearchSel, {'Random', 'Roulette', 'RankSpace', 'FitAndDiv', 'Best'}), mustBeNonempty} = "Best";
        locSearchOpts.locSearchRankSpace (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 10;
        locSearchOpts.locSearchFreq (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonnegative, mustBeNonNan, mustBeNonempty, mustBeReal, mustBeInteger, mustBeFinite} = 10;
        locSearchOpts.locSearchMeth (1, 1) {mustBeTextScalar, mustBeMember(locSearchOpts.locSearchMeth, {'Full', 'Greedy'}), mustBeNonempty} = "Full";
        locSearchOpts.nNeighbourPts (1, 1) {mustBeScalarOrEmpty, mustBeNumeric, mustBeNonNan, mustBeReal, mustBeInteger, mustBeNonempty, mustBePositive, mustBeFinite} = 100;
        locSearchOpts.nIters (1, 1) {mustBeScalarOrEmpty, mustBeNumeric, mustBeFinite, mustBeNonNan, mustBeInteger, mustBeReal, mustBeNonempty, mustBePositive} = 100;
        locSearchOpts.stepSize {mustBeNumeric, mustBeNonNan, mustBeFinite, mustBeNonempty} = 0.1;

        newGenOpts.newGenSel (1, 1) string {mustBeTextScalar, mustBeMember(newGenOpts.newGenSel, {'Random', 'Roulette', 'RankSpace', 'FitAndDiv'}), mustBeNonempty} = "RankSpace";
        newGenOpts.newGenRankSpace (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 10;
        newGenOpts.elitePerc (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeLessThanOrEqual(newGenOpts.elitePerc, 100), mustBeReal, mustBeNonempty} = 5;
        newGenOpts.resetFreq (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeInteger, mustBeNonempty} = 0;
        newGenOpts.divWeight (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonnegative, mustBeNonNan, mustBeFinite, mustBeReal, mustBeNonempty} = 1;

        termOpts.maxGens (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBePositive, mustBeNonNan, mustBeReal, mustBeNonempty} = 200;
        termOpts.maxTime (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonNan, mustBePositive, mustBeReal, mustBeNonempty} = 600;
        termOpts.fitTol (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonNan , mustBeReal, mustBeNonempty} = 0.1;
        termOpts.fitTolGenCount (1, 1) {mustBeNumeric, mustBeScalarOrEmpty, mustBeNonNan, mustBeInteger, mustBeReal, mustBePositive, mustBeNonempty} = 10;

        execOpts.fitVec (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        execOpts.fitPar (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        execOpts.constrVec (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        execOpts.constrPar (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        execOpts.locSearchPar (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;

        plotOpts.plotMean (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        plotOpts.plotMax (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
        plotOpts.plotMin (1, 1) logical {mustBeNonNan, mustBeNonempty} = false;
    end

    
    % ====================================================
    % Generate the initial population
    % ====================================================
    if popOpts.popSize > size(popOpts.initPop, 1)
        popOpts.initPop = initPopulation(constrFunc, popOpts.initPop, popOpts.popSize, nVars, popOpts.varRange, constrOpts.newGenConstr, execOpts.constrVec, execOpts.constrPar);
    else
        popOpts.popSize = size(popOpts.initPop, 1);
    end
    
    % Set the new generation as the initial population
    newGen = popOpts.initPop;

    % Calculate the fitness of the population

    fitVals = calcFitness(fitFunc, newGen, execOpts.fitVec, execOpts.fitPar);

    % ====================================================
    % Initialise variables
    % ====================================================
    % Initialise local search flag
    locSearchFlag = locSearchOpts.locSearchFreq;
    
    % Initialise plotting feature
    if plotOpts.plotMean || plotOpts.plotMax || plotOpts.plotMin
        figure("Name", "Optimisation statistics", "WindowState", "maximized");
        legVals = [];

        if plotOpts.plotMean
            hMean = animatedline("Color", getColour(1), "LineStyle", "-.", "LineWidth", 6);
            legVals = cat(1, legVals, "Mean");
        end
        
        if plotOpts.plotMax
            hMax = animatedline("Color", getColour(2), "LineStyle", "-", "LineWidth", 6);
            legVals = cat(1, legVals, "Max");
        end
        
        if plotOpts.plotMin
            hMin = animatedline("Color", getColour(3), "LineStyle", ":", "LineWidth", 6);
            legVals = cat(1, legVals, "Min");
        end
        grid minor;

        legend(legVals, "FontSize", 32, "Location", "northoutside", "orientation", "horizontal", "Interpreter", "latex");
        
        ax = gca(); ax.FontSize = 28;
        xlabel("Generation", "FontSize", 32, "Interpreter", "latex");
        ylabel("Fitness value", "FontSize", 32, "Interpreter", "latex");

        % Housekeeping
        clear ax;
    end

    % Initialise termination conditions
    genCount = 0; % Number of generations
    
    bestFitVal = -Inf; % Last best fitness value
    fitGenCount = 0; % Count of generations without fitness difference above threshold

    tStart = tic(); % Timing

    
    % ====================================================
    % Let the world spin...
    % ====================================================
    while toc(tStart) < termOpts.maxTime && genCount < termOpts.maxGens && fitGenCount < termOpts.fitTolGenCount
        % ====================================================
        % Crossover operation
        % ====================================================
        % Define variable
        xOverOffsprings = [];

        % Perform cross-over
        while true
            % Pick parents    
            subjects = selParents(xOverOpts.xOverSel, fitVals, newGen, xOverOpts.xOverPerc, xOverOpts.nParents, divFunc, newGenOpts.divWeight);

            % Perform crossover
            xOverOffsprings = cat(1, xOverOffsprings, crossPop(subjects, xOverOpts.xOverProb));

            % Check for constraints
            if isempty(constrFunc)
                break;
            end
    
            % Constrain offsprings
            xOverOffsprings = constrPop(constrFunc, xOverOffsprings, execOpts.constrVec, execOpts.constrPar);
            xOverOffsprings = unique(xOverOffsprings, "rows", "stable"); % Make sure we didn't introduce duplicates

            % Check replace rule
            if ~strcmpi(constrOpts.xOverConstr, "Replace")
                break;
            end

            % If we have enough keep as many as we need
            if size(xOverOffsprings, 1) >= size(subjects, 1)
                xOverOffsprings = xOverOffsprings(1:size(subjects, 1), :);
                break;
            end
        end


        % ====================================================
        % Offspring mutation operation
        % ====================================================
        % Pick individuals to be mutated
        nMuts = ceil(mutOpts.mutOffsprPerc/100 * size(xOverOffsprings, 1)); % Number of individuals to be mutated

        % Define variable
        mutOffsprOffsprings = [];
        
        % Perform mutation
        while true
            % Pick individuals to mutate
            subjects = selNewGen(mutOpts.mutOffsprSel, fitVals, newGen, nMuts, 0, divFunc, newGenOpts.divWeight);
    
            % Perform mutation
            mutOffsprOffsprings = cat(1, mutOffsprOffsprings, mutPop(subjects, 100, mutOpts.mutOffsprProb, mutOpts.mutOffsprVals, false));
    
            % Check for constraints
            if isempty(constrFunc)
                break;
            end
    
            % Constrain offsprings
            mutOffsprOffsprings = constrPop(constrFunc, mutOffsprOffsprings, execOpts.constrVec, execOpts.constrPar);
            mutOffsprOffsprings = unique(mutOffsprOffsprings, "rows", "stable"); % Make sure we didn't introduce duplicates

            % Check replace rule
            if ~strcmpi(constrOpts.mutOffsprConstr, "Replace")
                break;
            end

            % If we have enough keep as many as we need
            if size(mutOffsprOffsprings, 1) >= nMuts
                mutOffsprOffsprings = mutOffsprOffsprings(1:nMuts, :);
                break;
            end
        end


        % ====================================================
        % Population mutation operation
        % ====================================================
        % Pick individuals to be mutated
        nMuts = ceil(mutOpts.mutPopPerc/100 * popOpts.popSize); % Number of individuals to be mutated

        % Define variable
        mutPopOffsprings = [];
        
        % Perform mutation
        while true
            % Pick individuals to mutate
            subjects = selNewGen(mutOpts.mutPopSel, fitVals, newGen, nMuts, 0, divFunc, newGenOpts.divWeight);
    
            % Perform mutation
            mutPopOffsprings = cat(1, mutPopOffsprings, mutPop(subjects, 100, mutOpts.mutPopProb, mutOpts.mutPopVals, false));
    
            % Check for constraints
            if isempty(constrFunc)
                break;
            end
    
            % Constrain offsprings
            mutPopOffsprings = constrPop(constrFunc, mutPopOffsprings, execOpts.constrVec, execOpts.constrPar);
            mutPopOffsprings = unique(mutPopOffsprings, "rows", "stable"); % Make sure we didn't introduce duplicates

            % Check replace rule
            if ~strcmpi(constrOpts.mutPopConstr, "Replace")
                break;
            end

            % If we have enough keep as many as we need
            if size(mutPopOffsprings, 1) >= nMuts
                mutPopOffsprings = mutPopOffsprings(1:nMuts, :);
                break;
            end
        end


        % ====================================================
        % Local search operation
        % ====================================================
        % Define variable
        optSubs = [];

        % Check if local search is "activated"
        if locSearchOpts.locSearchFreq ~= 0
            % Check if "it is time" to perform local search
            if locSearchFlag == 0
                nLocSrch = ceil((locSearchOpts.locSearchPerc/100) * popOpts.popSize); % Size of population to perform local search for

                % Perform the local search
                while true
                    % Get the individuals
                    if strcmpi(locSearchOpts.locSearchSel, "Best")
                        [~, subjects] = sort(fitVals);
                        locSrchIdx = subjects(1:nLocSrch); % Get the indices of the best individuals
                        subjects = newGen(locSrchIdx, :); % Get the best individuals
                        
                    else
                        [subjects, locSrchIdx] = selNewGen(locSearchOpts.locSearchSel, fitVals, newGen, nLocSrch, 0, divFunc, newGenOpts.divWeight);
                    end
    
                    % Perform local search
                    optSubs = cat(1, optSubs, locSearch(subjects, fitVals(locSrchIdx), locSearchOpts.nNeighbourPts, locSearchOpts.locSearchMeth, locSearchOpts.stepSize, locSearchOpts.nIters, fitFunc, execOpts.fitVec, execOpts.fitPar, execOpts.locSearchPar));
    
                    % Check for constraints
                    if isempty(constrFunc)
                        break;
                    end
            
                    % Constrain offsprings
                    optSubs = constrPop(constrFunc, optSubs, execOpts.constrVec, execOpts.constrPar);
                    optSubs = unique(optSubs, "rows", "stable"); % Make sure we didn't introduce duplicates

                    % Check replace rule
                    if ~strcmpi(constrOpts.locSearchConstr, "Replace")
                        break;
                    end
        
                    % If we have enough keep as many as we need
                    if size(optSubs, 1) >= nLocSrch
                        optSubs = optSubs(1:nLocSrch, :);
                        break;
                    end
                end

                % Reset the flag
                locSearchFlag = locSearchOpts.locSearchFreq - 1;
            else
                % Count down to local search
                locSearchFlag = locSearchFlag - 1;
            end
        end

        
        % ====================================================
        % Get the new generation
        % ====================================================
        % Calculate the fitness of the extended population
        xtPop = [newGen; xOverOffsprings; mutOffsprOffsprings; mutPopOffsprings; optSubs];
        xtPop = unique(xtPop, "rows", "stable");

        fitVals = calcFitness(fitFunc, xtPop, execOpts.fitVec, execOpts.fitPar);

        % Get the new generation
        [newGen, newGenIdx] = selNewGen(newGenOpts.newGenSel, fitVals, xtPop, popOpts.popSize, newGenOpts.elitePerc, divFunc, newGenOpts.divWeight);

        % Keep the fitness values of the new generation
        fitVals = fitVals(newGenIdx);

        % Update best values
        [maxFit, maxIdx] = max(fitVals);
        
        if bestFitVal < maxFit
            % Update max values
            bestFitVal = maxFit;
            bestSol = newGen(maxIdx, :);

            % Reset fitness tolerance countdown
            fitGenCount = 0;
        elseif abs(bestFitVal - maxFit) < termOpts.fitTol
            % Update fitness tolerance countdown
            fitGenCount = fitGenCount + 1;
        end

        % ====================================================
        % Update termination parameters
        % ====================================================
        % Increase generation count
        genCount = genCount + 1;
        

        % ====================================================
        % Plot if asked for
        % ====================================================
        if plotOpts.plotMean
            addpoints(hMean, genCount, mean(fitVals));
        end

        if plotOpts.plotMax
            addpoints(hMax, genCount, max(fitVals));
        end

        if plotOpts.plotMin
            addpoints(hMin, genCount, min(fitVals));
        end
        drawnow;


        % ====================================================
        % Reset
        % ====================================================
        if newGenOpts.resetFreq ~= 0 && mod(genCount, newGenOpts.resetFreq) == 0
            newGen = initPopulation(constrFunc, [], popOpts.popSize, nVars, popOpts.varRange, constrOpts.newGenConstr, execOpts.constrVec, execOpts.constrPar);
        end
    end
end





%% Utility functions
% Argument checking functions
function result = mustHaveDims(arg, rows, cols)
    result = isequal(size(arg), [rows, cols]);
end

function result = mustHaveCols(arg, nCols)
    result = size(arg, 2) == nCols;
end

function result = mustBeIncreasing(arg)
    result = isempty(find(diff(arg) <= 0, 1));
end

function result = mustBeFuncHandleOrEmpty(arg)
    if isa(arg, "function_handle")
        result = true;
    elseif isa(arg, "numeric")
        if isempty(arg)
            result = true;
        end
    else
        result = false;
    end
end


% Population initialisation function
function newPop = initPopulation(constrFunc, pop, popSize, nVars, varRange, treatConstr, vecExec, parExec)
    % Initialise variables
    nInd = popSize - size(pop, 1); % How many individuals we want
    
    % Create new population
    newPop = diff(varRange) .* rand(nInd, nVars) + varRange(1, :);
    newPop = cat(1, pop, newPop);

    % Constrain new population
    if ~isempty(constrFunc)
        newPop = constrPop(constrFunc, newPop, vecExec, parExec);
    end

    % Replace missing discarded individuals
    if strcmpi(treatConstr, "Replace")
        % Run while we have enough individuals
        while popSize > size(newPop, 1)
            newInd = diff(varRange) .* rand(popSize, nVars) + varRange(1, :);

            if ~isempty(constrFunc)
                newInd = constrPop(constrFunc, newInd, vecExec, parExec);
            end

            newPop = cat(1, newPop, newInd); % Put them all together
            newPop = unique(newPop, "rows", "stable"); % Get rid of duplicates
        end

        % Make sure we don't have more individuals than we need
        newPop = newPop(1:popSize, :); % Get the first popSize individuals
    end
end