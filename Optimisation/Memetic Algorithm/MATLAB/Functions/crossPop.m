%% Crossover operation for a genetic/memetic algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 05/12/2023 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Perform crossover in a genetic/memetic
%                algorithm
% --------------------------------------------------
% Input
% 
% family [numeric]: An array of dimensions NxMxK where N is the number of
%                   offsprings to be produced, M the variables in each
%                   individuals DNA and K the number of parents per
%                   offspring.
% 
% crossProb [numeric] (Optional): This is an (M - 1)xK matrix where M is
%                                 the number of variables per individual
%                                 and K is the number of parents per
%                                 offspring. Each row corresponds to the
%                                 probabilities of each position being
%                                 chosen as crossover point. For example,
%                                 with a 3 variable DNA, the probabilities
%                                 could be [40, 60], stating that there
%                                 is a probability of 40% the crossover
%                                 point being after the first element and
%                                 60% being after the second element. The
%                                 probabilities at each row must sum up to
%                                 100 with a tolerance of about 1000 * eps.
%                                 For an explanation of how crossover
%                                 points are chosen when multiple parents
%                                 are used see the notes below. This
%                                 argument can also be a column vector
%                                 with increasing integer values in the
%                                 range [1, M - 1]. These then correspond
%                                 to the crossover points for each parent
%                                 and are constant for all offsprings.
%                                 [Default: ones(M, K)/M].
% 
% 
% --------------------------------------------------
% Output
% 
% offsrping [numeric]: This variable holds the offsprings as derived after
%                      the crossover operation. It is a NxM matrix with N
%                      being the number of offsprings and M the number of
%                      variables per individual.
% 
% --------------------------------------------------
% Notes
% 
% Multiple parents: When multiple parents are used for each offspring the
%                   crossover points are chosen sequentially. When the
%                   first crossover point is chosen, all columns that
%                   correspond to probabilities before and up to this point
%                   are zeroed out so that the next crossover points (for
%                   the next parents) can only be after this position. A
%                   2x4 example (5 variables per individual with three
%                   parents each offspring) is:
%                   Probabilities: [20, 30, 30, 20;
%                                   10, 20, 50, 20]
%                   First crossover point: 2 (between the second and third
%                                          parameter) and the probabilties
%                                          for the second crossover point
%                                          now become
%                   Probabilities: [0, 0, 50/(50 + 20), 20/(50 + 20)]
%                   If the last available position is reached the process
%                   ends even if not all parents have contributed. In order
%                   to avoid this behaviour, the rows of the "crossProb"
%                   argument can have values at mutually exclusive columns.
%                   A 2x4 example is:
% 
%                   [40, 60,  0,  0;
%                     0,  0, 70, 30]
% 
%                   It would also make sense to have rows with non-zero
%                   elements partially overlaping over the columns such as,
%                   for the same 2x4 example get:
% 
%                   [30, 50, 20,  0;
%                     0, 10, 50, 40]
% 
%                   If there are overlaping crossover points with zero
%                   probability then these will be occupied by the next
%                   parent (or the last parent if they are in the last
%                   columns).
%                   Furthermore, the "crossProb" argument can be a column
%                   vector of increasing integer values. In this case, each
%                   element corresponds to the crossover point associated
%                   to each parent and is constant for all crossovers. The
%                   values must be increasing in the range [1, M - 1] where
%                   M is the number of variables per individual. For a 8
%                   variables DNA example with 3 parents per offspring
%                   would look like:
% 
%                   [2; 5; 7]
% 
%                   Important note: If the last position has a non-zero
%                   probability, the algorithm may end up performing no
%                   crossover. The offspring will be identical to its first
%                   parent. A solution to that is to make sure the
%                   probability for the last position is zero for the first
%                   parent. An nice meaningful solution is to provide
%                   either overlapping or non-overlapping probabilities
%                   like the examples above.
% 
% 
% TODO: Find a way to vectorise the crossover when the crossover positions
%       are not fixed/constant (depend on probabilties).
% 
% --------------------------------------------------
function offspring = crossPop(family, crossProb)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(family, "numeric", {'nonempty', 'finite', 'nonnan'}, mfilename, "The parents", 1);

    % Validate optional arguments
    if nargin > 1
        if iscolumn(crossProb)
            validateattributes(crossProb, "numeric", {'vector', 'increasing', 'integer', 'positive', 'nonnan', 'finite', 'real', 'nonempty', '<=', size(family, 2) - 1, 'numel', size(family, 3) - 1}, mfilename, "Crossover point probabilities", 2);
        else
            validateattributes(crossProb, "numeric", {'ncols', size(family, 2) - 1, 'nrows', size(family, 3), 'nonnegative', '<=', 100, 'nonnan', 'finite', 'real', 'nonempty'}, mfilename, "Crossover point probabilities", 2);

            % Make sure probabilites sum up to 1 with some tolerance
            if sum(abs(sum(crossProb, 2) - 100) > (1e3 * eps)) > 0
                error("crossPop(): The probabilities at each row of the crossover point probabilities matrix must sum up to 100 with a tolerance of 1000 * eps.");
            end

            % Convert to range [0, 1]
            crossProb = crossProb/100;
        end
    else
        % Initialise with uniform distribution for the crossover positions
        crossProb = ones(size(family, [1, 2]))/size(family, 2);
    end


    % ====================================================
    % Cross them parents over...!!!
    % ====================================================
    %% TODO: Find a way to perform the crossover in a vectorised fashion
    % Pre-allocate for speed
    offspring = zeros(size(family, [1, 2]));

    % Fixed crossover positions
    if iscolumn(crossProb)
        % Add the element to positions for convenience
        crossProb = [0; crossProb; size(family, 2)];
        % Go through the parents
        for parIdx = 1:size(family, 3)
            % Crossover indices
            xInd = crossProb(parIdx) + 1:crossProb(parIdx + 1);

            % Perform crossover
            offspring(:, xInd) = family(:, xInd, parIdx);
        end
    else
        % Go through the offsprings
        for offIdx = size(family, 1):-1:1
            xStart = 1; % Initialise crossover point

            % Go through the parents and perform crossover
            for parIdx = 1:size(family, 3)
                % Get the probabilities for this parent
                newProb = crossProb(parIdx, xStart:end); % Remove probabilities of the crossover points used prior to this parent
                newProb = newProb/sum(newProb); % Convert to probabilities (they must sum to 1)

                if isnan(newProb)
                    newProb = 1;
                end

                % Pick next crossover point
                if parIdx == size(family, 3)
                    xStart = xStart + 1;
                    xEnd = size(family, 2);
                else
                    xEnd = pickValProb(xStart + 1:size(family, 2), newProb, 1);
                end

                % Get the parent's genes
                offspring(offIdx, xStart:xEnd) = family(offIdx, xStart:xEnd, parIdx);

                % Check the last position of the crossed over variable
                if xEnd == size(family, 2)
                    % If the end of the variables is reached go the next offspring
                    break;
                else
                    % Update the starting position for the next parent's genes
                    xStart = xEnd;
                end
            end
        end
    end
end