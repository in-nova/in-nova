%% Selection of new generation in genetic algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 09/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: New generation selection process in
%                a genetic/memetic algorithm.
% --------------------------------------------------
% Input
% 
% selType [char/string]: Type of the selection mechanism. This can be one
%                        of
%                        - Roulette: This is the fitness proportionate
%                                      mechanism of selection where each
%                                      individual's probability of
%                                      survival/selection is proportionate
%                                      to its fitness value.
%                        - RankSpace: In this "mode", the fittest
%                                     individual has the highest
%                                     probability of being chosen, given by
%                                     a constant (typeParam value) and each
%                                     successive individual's probability
%                                     of survival is the probability of the
%                                     previous not being chosen times the
%                                     same constant. The given constant
%                                     must lie in the range (0, 100].
%                       - Random: The individuals are chosen randomly with
%                                 probabilities resulting from a uniform
%                                 distribution.
% 
% fitVals [function handle]: The fitness values of the population. This
%                            argument can be left empty but this will
%                            result in an error if "Roulette" is chosen as
%                            the selection mechanism.
% 
% pop [numeric]: The population from which to pick the individuals to
%                survive in the next generation. The population must be
%                sorted in decreasing fitness order for the "RankSpace"
%                mechanism of selection to work correctly, or if eliteness
%                is used.
% 
% genSize [numeric]: The size of the new generation.
% 
% elitePercent [numeric] (Optional): The number of the best individuals
%                                    that will pass directly to the next
%                                    generation as a percentage of the size
%                                    of the final population (given in
%                                    "genSize" parameter). Can be zero.
%                                    [Default: 0]
% 
% divFunc [function handle] (Optional): The function to calculate the
%                                       diversity of the individuals in the
%                                       population. The distance of each
%                                       individual from the origin in a
%                                       normalised fitness-diversity graph
%                                       will be used as the final fitness
%                                       value. [Default: empty].
% 
% divWeight [numeric] (Optional): The weight to be applied to the diversity
%                                 value of the individuals. This value is
%                                 used to balance the focus towards the
%                                 fitness function or diversity. If left
%                                 empty (or set equalt to 1) the scale of
%                                 the diversity will change to match that
%                                 of the fitness. This value has to be non
%                                 negative. [Default: 1].
%                                            
% --------------------------------------------------
% Output
% 
% newGen [numeric]: This is the population that survived this generation.
%                   The dimensions of the matrix are NxM where N is the
%                   number of individuals given by the "genSize" parameter
%                   and M the number of parameters of each individual.
% 
% newGenIdx [numeric] (Optional): The indices in the original population
%                                 parameter of the chosen individuals.
%
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [newGen, newGenIdx] = selNewGen(selType, fitVals, pop, genSize, elitePercent, divFunc, divWeight)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(4, 7);
    nargoutchk(0, 2);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(selType, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Selection type/mechanism", 1);
    validatestring(selType, ["Random", "Roulette", "RankSpace"], mfilename, "Selection type/mechanism", 1);

    if strcmpi(selType, "Roulette") && isempty(fitVals)
        error("selNewGen(): Fitness values must be provided if Roulette is selected as the selection mechanism.");
    else
        validateattributes(fitVals, "numeric", {'vector', 'numel', size(pop, 1), 'real', 'finite', 'nonnan'}, mfilename, "Fitness values", 2);
    end

    validateattributes(pop, "numeric", {'nonempty', 'finite', 'nonnan'}, mfilename, "Population", 3);
    validateattributes(genSize, "numeric", {'scalar', 'nonnan', 'nonempty', 'integer', 'real', 'positive', 'finite'}, mfilename, "New generation size", 4);

    if nargin > 4 && ~isempty(elitePercent)
        validateattributes(elitePercent, "numeric", {'scalar', 'nonnan', 'finite', 'real', 'nonnegative', '<=', 100}, mfilename, "Elite percentage of the new generation size", 5);
    else
        elitePercent = 0;
    end

    if nargin > 5 && ~isempty(divFunc)
        validateattributes(divFunc, {'function_handle'}, {}, mfilename, "Diversity function", 6);
    else
        divFunc = [];
    end

    if nargin > 6 && ~isempty(divWeight)
        validateattributes(divWeight, {'numeric'}, {'scalar', 'nonempty', 'real', 'finite', 'nonnegative', 'nonnan'}, mfilename, "Diversity weight", 7);
    else
        divWeight = 1;
    end
    

    % ====================================================
    % Make sure we do have to work
    % ====================================================
    if genSize >= size(pop, 1)
        newGen = pop(1:genSize, :);
        newGenIdx = 1:genSize;
        return;
    end

    % ====================================================
    % Process fitness
    % ====================================================
    % Sort if rank space method is used
    if strcmpi(selType, "RankSpace")
        [fitVals, fitValsIdx] = sort(fitVals, 1, "ascend");
        pop = pop(fitValsIdx, :);
    end

    % Make sure we only have non-negative values
    if sum(fitVals < 0) > 0 && strcmpi(selType, "Roulette")
        fitVals = fitVals - min(fitVals);
    end

    % ====================================================
    % Get the elites
    % ====================================================
    % Pre-allocate for speed
    newGen = zeros(genSize, size(pop, 2));
    newGenIdx = zeros(genSize, 1);
    
    % How many elites we need
    nElite = ceil((elitePercent/100) * genSize);

    % Get the elites
    if nElite > 0
        % Get the indices of the elites
        [~, newGenIdx(1:nElite)] = maxk(fitVals, nElite);

        newGen(1:nElite, :) = pop(newGenIdx(1:nElite), :); % Get the elites
        pop(newGenIdx(1:nElite), :) = []; % Remove elites from population
        fitVals(newGenIdx(1:nElite)) = []; % Remove the fitness values of the elites too
    end

    % ====================================================
    % Incorporate diversity
    % ====================================================
    if ~isempty(divFunc)
        % Calculate diversity of individuals
        diversity = divFunc(pop);
        
        % Scale diversity to match the scale of fitness
        diversity = diversity/max(diversity) * max(fitVals) * divWeight;
        
        % Calculate the distance on the fitness-diversity graph from the origin
        fitVals = vecnorm([fitVals(:), diversity(:)], 2, 2);
    end

    
    % ====================================================
    % Calculate the probabilities of the population
    % ====================================================
    if strcmpi(selType, "Random")
        % Generate and sort random value for each individual
        [~, genIdx] = sort(rand(size(pop, 1), 1), "descend");
        
        % Pick the individuals with the highest values
        newGen(nElite + 1:end, :) = pop(genIdx(1:genSize - nElite), :);

        % Pick up their indices
        newGenIdx(nElite + 1:end) = genIdx(1:genSize - nElite);

        % In this case the calculation of the new generation is done
        return;
    elseif strcmpi(selType, "Roulette")
        % Calculate relative fitness values
        fitVals = fitVals/sum(fitVals);
    elseif strcmpi(selType, "RankSpace")
        % Calculate the fitness values based on the position in the population
        fitVals = (1:numel(fitVals))/(sum(1:numel(fitVals)));
    end


    % ====================================================
    % Create new generation
    % ====================================================
    tmpIdx = pickValProb(1:size(pop, 1), fitVals, genSize - nElite);

    % Make sure we don't have duplicates
    tmpIdx = unique([newGenIdx(1:nElite).', tmpIdx], "stable"); % Add the elites

    while length(tmpIdx) < genSize
        % Run again
        tmpIdx = cat(2, tmpIdx, pickValProb(1:size(pop, 1), fitVals, 10 * genSize)); % Pick many individuals
        tmpIdx = unique(tmpIdx, "stable"); % Filter them for duplicates
    end

    % Get new generation (get only the genSize first indices - elites are included)
    if nElite ~= 0
        newGenIdx(nElite + 1:end) = tmpIdx(nElite + 1:genSize); % Get indices
        newGen(nElite + 1:end, :) = pop(newGenIdx(nElite + 1:end), :); % Get individuals

        if strcmpi(selType, "RankSpace")
            newGenIdx = fitValsIdx(newGenIdx); % Get the indices of the individuals (an fitness values) before the sorting
        end
    else
        newGenIdx(1:end) = tmpIdx(1:genSize); % Get indices
        newGen(1:end, :) = pop(newGenIdx(1:end), :); % Get individuals
    end
end