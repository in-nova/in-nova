%% Evaluate population against a fitness function
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 29/11/2023 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the fitness function of
%                a population.
% --------------------------------------------------
% Input
% 
% fitFunc [function handle]: The fitness function to be called for the
%                            evaluation of the population.
% 
% pop [numeric]: An NxM population matrix. N corresponds to the number of
%                the individuals in the population and M to the number of
%                variables in each individual.
% 
% vecExec [logical] (Optional): Whether the execution of the fitness
%                               function is vectorised. In this case, the
%                               fitness function is expected to provide the
%                               fitness of all individuals in an Nx1 vector
%                               through a single call. In this case, N is
%                               the size of the population.
%                               [Default: false].
% 
% parExec [logical] (Optional): Whether the execution is to be parallelised
%                               or not. [Default: false]
% --------------------------------------------------
% Output
% 
% fitness [numeric]: The fitness value for each individual in the
%                    population.
% 
% bestFit [numeric]: The individuals sorted in descending fitness order.
% 
% bestFitIdx [numeric]: The indices of the individuals after being sorted
%                       in descending fitness order.
% --------------------------------------------------
% Notes
% 
% - If the provided function can return the fitness of all the individuals
%   in a single call then "vecExec" should be set to true. This will
%   provide significantly shorter execution times.
% --------------------------------------------------
function [fitness, bestFit, bestFitIdx] = calcFitness(fitFunc, pop, vecExec, parExec)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 4);
    nargoutchk(0, 3);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(fitFunc, "function_handle", {}, mfilename, "Fitness function", 1);
    validateattributes(pop, "numeric", {'nonnan', 'finite'}, mfilename, "Population", 2);

    % Validate optional arguments
    if nargin > 2
        validateattributes(vecExec, "logical", {'nonnan', 'nonempty', 'scalar', 'finite', 'real'}, mfilename, "Parallel execution", 3);
    else
        vecExec = false;
    end

    if nargin > 3 && ~vecExec
        validateattributes(parExec, "logical", {'nonnan', 'nonempty', 'scalar', 'finite', 'real'}, mfilename, "Parallel execution", 4);
    else
        parExec = false;
    end

    % ====================================================
    % Calculate fitness
    % ====================================================
    % Calculate fitness
    if vecExec
        fitness = feval(fitFunc, pop);
    elseif parExec
        parfor idx = 1:size(pop, 1)
            fitness(idx) = feval(fitFunc, pop(idx, :));
        end
        fitness = fitness(:);
    else
        for idx = size(pop, 1):-1:1
            fitness(idx) = feval(fitFunc, pop(idx, :));
        end
        fitness = fitness(:);
    end

    % Sort the population according to their fitness values
    if nargout > 1
        [~, bestFitIdx] = sort(fitness, "descend"); % Get sorted indices
        bestFit = pop(bestFitIdx, :); % Sort the population
    else
        bestFit = [];
        bestFitIdx = [];
    end
end