%% Mutate population in a Memetic Algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 21/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Perform mutations in a Memetic
%                Algorithm
% --------------------------------------------------
% Input
% 
% pop [numeric]: An NxM population matrix. N corresponds to the number of
%                the individuals in the population and M to the number of
%                variables in each individual.
% 
% mutPerc [numeric] (Optional): Number of mutated individuals as a
%                               percentage of the size of the given
%                               population. This must be a number in the
%                               range [0, 100]. [Default: 20].
% 
% mutProb [numeric] (Optional): The probability of mutating each
%                               variable in an individual's DNA. This
%                               argument must be either a vector with a
%                               maximum number of elements equal to the
%                               variables in an individual, or a scalar. If
%                               a vector is passed, the elements must lie
%                               in the range [0, 100], denoting the
%                               probability of this variable to be mutated.
%                               When the number of elements is smaller than
%                               the variables in an individual's DNA, the
%                               last elements are filled with zeros.
%                               In case the argument is a scalar, it
%                               denotes the maximum number of variables
%                               that will be mutated in an individual. If
%                               the value is negative, exactly this number
%                               of variables will be mutated. The variables
%                               to be mutated are chosen at random from a
%                               uniform distribution. [Default: -1].
% 
% mutVals [numeric] (Optional): This argument must be either a vector with
%                               N elements or a 2xN matrix with N denoting
%                               the number of variables in an individual's
%                               DNA or a scalar. In the case the argument
%                               is a matrix, the two rows denote the
%                               minimum and maximum values to be added to
%                               the mutated variable (corresponding to each
%                               column). The vector is a special case where
%                               the one of the values is 0 (either the
%                               maximum or the minimum, which depends on
%                               the values of the elements of the vector).
%                               If a scalar is passed, the value added to
%                               each mutated variable will lie between 0
%                               and this value drawn from a uniform
%                               distribution. [Default: 1].
% 
% uniqueMuts [logical] (Optional): Whether the population picked for
%                                  mutation should be unique or duplicates
%                                  are allowed. [Default: false].
% 
% --------------------------------------------------
% Output
% 
% muts [numeric]: The mutated population.
% --------------------------------------------------
% Notes
% 
% - If probabilities of mutation are provided (with "mutProb" being a
%   vector), there is always a probability that no mutation will be
%   performed in case of values less than unity (since there's always a
%   probability for each variable to not be mutated).
% 
% - When a vector of probabilities is provided, the calculations are
%   vectorised and result in shorter execution times. Thus, it is preferred
%   to use this method whenever possible.
% 
% TODO: Find a more efficient way to perform the mutations (the bottleneck
%       seems to be at the calculation of indices). It would be good to
%       find a way to vectorise the calculations.
% --------------------------------------------------
function muts = mutPop(pop, mutPerc, mutProb, mutVals, uniqueMuts)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 5);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(pop, "numeric", {'nonempty', 'finite', 'nonnan'}, mfilename, "Population", 1);

    % Validate and initialise optional arguments
    if nargin > 1
        validateattributes(mutPerc, "numeric", {'scalar', 'nonnan', 'finite', 'nonnegative', '<=', 100}, mfilename, "Percentage of the population to be mutated", 2);

        if isempty(mutPerc)
            mutPerc = 20;
        elseif ~isscalar(mutPerc)
            mutPerc = mutPerc(1);
        end
    else
        mutPerc = 20;
    end

    if nargin > 2
        if isscalar(mutProb)
            validateattributes(mutProb, "numeric", {'real', 'nonnan', 'finite', 'scalar', '<=', size(pop, 2), '>=', -size(pop, 2), 'integer'}, mfilename, "Maximum number of mutated variables", 3);
        elseif isvector(mutProb)
            validateattributes(mutProb, "numeric", {'real', 'finite', 'nonnan', 'vector', 'nonnegative', '<=', 100, 'numel', size(pop, 2)}, mfilename, "Mutation probabilities of mutated variables", 3);
        elseif isempty(mutProb)
            mutProb = -1;
        else
            error("mutPop(): The mutation probabilities argument must be either a scalar or a vector.");
        end
    else
        mutProb = -1;
    end

    if nargin > 3
        if isscalar(mutVals)
            validateattributes(mutVals, "numeric", {'scalar', 'finite', 'nonnan'}, mfilename, "Value to be added to the mutated variables", 4);
        elseif isvector(mutVals)
            validateattributes(mutVals, "numeric", {'vector', 'finite', 'nonnan', 'numel', size(pop, 2)}, mfilename, "Values to be added to the mutated variables", 4);
        elseif ismatrix(mutVals)
            validateattributes(mutVals, "numeric", {'2d', 'nrows', 2, 'ncols', size(pop, 2), 'finite', 'nonnan'}, mfilename, "Value to be added to the mutated variables", 4);
        elseif isempty(mutVals)
            mutVals = 1;
        else
            error("mutPop(): The mutation value must be either a scalar, a vector, or a matrix.");
        end
    end

    if nargin > 4
        validateattributes(uniqueMuts, "logical", {'nonnan', 'finite', 'real', 'scalar'}, mfilename, "Uniqueness of population to be mutated", 5);

        if isempty(uniqueMuts)
            uniqueMuts = false;
        end
    else
        uniqueMuts = false;
    end


    % ====================================================
    % Pick the mutants
    % ====================================================
    nMuts = ceil((mutPerc/100) * size(pop, 1)); % Number of mutants

    % Ensure unique to-be-mutated population (if asked for)
    if uniqueMuts
        muts = uniqueRows(pop, nMuts);
    else
        muts = pop(randi(size(pop, 1), [nMuts, 1]), :); % The mutants themselves
    end


    % ====================================================
    % Calculate the values to be added to the mutated variables
    % ====================================================
    if isscalar(mutVals)
        mutVals = [zeros(1, size(pop, 2)); mutVals * ones(1, size(pop, 2))];
    elseif isvector(mutVals)
        mutVals = [zeros(1, size(pop, 2)); mutVals(:).'; zeros(size(pop, 2) - length(mutVals), 1)];
    end


    % ====================================================
    % Calculate probabilities if a vector is given for probabilities
    % ====================================================
    if ~isscalar(mutProb)
        mutProb = mutProb/100; % Convert to range [0, 1]
        mutProb = [mutProb(:).'; zeros(size(pop, 2) - length(mutProb), 1)];
    end


    % ====================================================
    % Perform mutations
    % ====================================================
    % Create probabilities for the variables to be mutated
    if isscalar(mutProb)
        parfor mutIdx = 1:nMuts
            if mutProb < 0
                % Make sure the number of mutated variables will be exactly that asked
                [~, varIdx] = maxk(rand(size(pop, 2), 1), abs(mutProb));
            else
                % Up to the asked number of variables will be mutated
                varIdx = unique(randi(size(pop, 2), [mutProb, 1]));
            end

            % Calculate index mask
            varIdx = ismember(1:size(pop, 2), varIdx);

            % Calculate values to be added to the variables
            tempMutVals = (mutVals(2, :) - mutVals(1, :)) .* rand(1, size(pop, 2)) + mutVals(1, :);
            tempMutVals = tempMutVals .* varIdx; % Mask the values

            % Thou shalt be named HULK...!!!
            muts(mutIdx, :) = muts(mutIdx, :) + tempMutVals;
        end
    else
        % Calculate index mask
        varIdx = rand(size(muts)) < mutProb;

        % Calculate values to be added to the variables
        tempMutVals = (mutVals(2, :) - mutVals(1, :)) .* rand(size(muts)) + mutVals(1, :);
        tempMutVals = tempMutVals .* varIdx; % Mask the values

        % Thou all shalt be named HULK...!!!
        muts = muts + tempMutVals;
    end
end