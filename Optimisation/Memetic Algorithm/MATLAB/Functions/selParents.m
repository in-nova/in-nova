%% Selection of parents in genetic algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 21/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Parent selection process in a genetic/memetic algorithm.
% --------------------------------------------------
% Input
% 
% selType [char/string]: Type of the selection mechanism. This can be one
%                        of
%                        - Roulette: This is the fitness proportionate
%                                      mechanism of selection where each
%                                      individual's probability of
%                                      survival/selection is proportionate
%                                      to its fitness value.
%                        - RankSpace: In this "mode", the fittest
%                                     individual has the highest
%                                     probability of being chosen, given by
%                                     a constant (typeParam value) and each
%                                     successive individual's probability
%                                     of survival is the probability of the
%                                     previous not being chosen times the
%                                     same constant. The given constant
%                                     must lie in the range (0, 100].
%                       - Random: The individuals are chosen randomly with
%                                 probabilities resulting from a uniform
%                                 distribution.
% 
% fitVals [function handle]: The fitness values of the population. This
%                            argument can be left empty but this will
%                            result in an error if any mechanism but the
%                            "Random" and "RankSpace" mechanisms are
%                            selected for the "selType".
% 
% pop [numeric]: The population from which the parents will be selected.
%                The population must be sorted in decreasing fitness order
%                for the "RankSpace" mechanism of selection to work
%                correctly.
% 
% offsprPerc [numeric]: Number of offsprings as percentage of the given
%                       population. This must be a number in the range
%                       [0, 100].
% 
% nParent [numeric] (Optional): How many parents will be calculated for
%                               each offspring. Must be a positive number
%                               greater or equal to 2. [Default: 2]
% 
% divFunc [function handle] (Optional): The function to calculate the
%                                       diversity of the individuals in the
%                                       population. The distance of each
%                                       individual from the origin in a
%                                       normalised fitness-diversity graph
%                                       will be used as the final fitness
%                                       value. [Default: empty].
% 
% divWeight [numeric] (Optional): The weight to be applied to the diversity
%                                 value of the individuals. This value is
%                                 used to balance the focus towards the
%                                 fitness function or diversity. If left
%                                 empty (or set equalt to 1) the scale of
%                                 the diversity will change to match that
%                                 of the fitness. This value has to be non
%                                 negative. [Default: 1].
%                                            
% --------------------------------------------------
% Output
% 
% parents [numeric]: An NxMxK matrix holding the parents. N corresponds to
%                    the number of offsprings, M to the number of variables
%                    per individual and K to the number of parents per
%                    offspring.
% 
% parentsVec [numeric]: An NKxM matrix where NK is the number of parents
%                       total number of parents corresponding to the
%                       parents per offspring times the number of
%                       offsprings. M is the number of variables per
%                       individual. The way parents are arranged is that
%                       for each offspring all its parents occupy
%                       successive positions. This is:
%                       [Par_1_Off_1_Var_1, Par_1_Off_1_Var_2, ...;
%                        Par_2_Off_1_Var_1, Par_2_Off_1_Var_2, ...;
%                                .        ,         .        , ...;
%                                .        ,         .        , ...;
%                                .        ,         .        , ...;
%                        Par_K_Off_1_Var_1, Par_2_Off_1_Var_2, ...;
%                        Par_1_Off_2_Var_1, Par_2_Off_1_Var_2, ...;
%                        Par_2_Off_2_Var_1, Par_2_Off_1_Var_2, ...;
%                                .        ,         .        , ...;
%                                .        ,         .        , ...;
%                                .        ,         .        , ...;
%                        Par_K_Off_N_Var_1, Par_2_Off_1_Var_2, ...]
% 
% --------------------------------------------------
% Notes
% 
% - The uniqueness of the parents for each offspring is not guaranteed.
%   Post-processing of the results may be needed to ensure that all parents
%   of an offspring are distinct. However, for "large enough" populations,
%   the probability of picking the same parent for an offspring is small,
%   for a small number of parents (hasn't been evaluated quantitatively).
% --------------------------------------------------
function [parents, parentsVec] = selParents(selType, fitVals, pop, offsprPerc, nParent, divFunc, divWeight)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(4, 7);
    nargoutchk(0, 2);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(selType, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Selection type/mechanism", 1);
    validatestring(selType, ["Random", "Roulette", "RankSpace"], mfilename, "Selection type/mechanism", 1);

    if strcmpi(selType, "Roulette") && isempty(fitVals)
        error("selNewGen(): Fitness values must be provided if Roulette is selected as the selection mechanism.");
    else
        validateattributes(fitVals, "numeric", {'vector', 'numel', size(pop, 1), 'real', 'finite', 'nonnan'}, mfilename, "Fitness values", 2);
    end

    validateattributes(pop, "numeric", {'nonempty', 'finite', 'nonnan'}, mfilename, "Population", 3);
    validateattributes(offsprPerc, "numeric", {'scalar', 'nonnan', 'nonempty', 'real', 'positive', 'finite', '>=' 0, '<=', 100}, mfilename, "Number of offsprings as a percentage of the population", 4);

    % Validate and initialise optional parameters
    if nargin > 4 && ~isempty(nParent)
        validateattributes(nParent, "numeric", {'nonnan', '>=', 2, 'finite', 'real', 'integer'}, mfilename, "Number of parents per offspring", 5);
    else
        nParent = 2;
    end

    if nargin > 5 && ~isempty(divFunc)
        validateattributes(divFunc, {'function_handle'}, {}, mfilename, "Diversity function", 6);
    else
        divFunc = [];
    end

    if nargin > 6 && ~isempty(divWeight)
        validateattributes(divWeight, {'numeric'}, {'scalar', 'nonempty', 'real', 'finite', 'nonnegative', 'nonnan'}, mfilename, "Diversity weight", 7);
    else
        divWeight = 1;
    end


    % ====================================================
    % Process fitness
    % ====================================================
    % Sort if rank space method is used
    if strcmpi(selType, "RankSpace")
        [fitVals, fitValsIdx] = sort(fitVals, 1, "ascend");
        pop = pop(fitValsIdx, :);
    end

    % Make sure we only have positive values
    if sum(fitVals < 0) > 0 && strcmpi(selType, "Roulette")
        fitVals = fitVals - min(fitVals);
    end

    % ====================================================
    % Incorporate diversity
    % ====================================================
    if ~isempty(divFunc)
        % Calculate diversity of individuals
        diversity = divFunc(pop);

        % Scale diversity to match the scale of fitness
        diversity = diversity/max(diversity) * max(fitVals) * divWeight;

        % Calculate the distance on the fitness-diversity graph from the origin
        fitVals = vecnorm([fitVals, divWeight * diversity(:)], 2, 2);
    end


    % ====================================================
    % Calculate probabilities
    % ====================================================
    % Get number of needed parents
    nOffspr = ceil((offsprPerc/100) * size(pop, 1));

    % Calculate 
    if strcmpi(selType, "Random")
        % Get some random parent indices
        idx = randi(size(pop, 1), nOffspr, nParent);

        % Put the parents into the array
        parfor ii = 1:nParent
            parents(:, :, ii) = pop(idx(:, ii), :);
        end

        if nargout > 1
            % Get the array into Parents x Offspring x Variables format
            parentsVec = permute(parents, [3, 1, 2]);
    
            % Reshape to the appropriate dimensions
            parentsVec = reshape(parentsVec, size(parents, 1) * size(parents, 3), size(parents, 2));
        end

        % We are done
        return;
    elseif strcmpi(selType, "Roulette")
        % Calculate relative fitness values
        fitVals = fitVals/sum(fitVals);
    elseif strcmpi(selType, "RankSpace")
        fitVals = (1:numel(fitVals))/(sum(1:numel(fitVals)));
    end


    % ====================================================
    % Choose parents
    % ====================================================
    % Do it for each offspring
    parfor offsprIdx = 1:nOffspr
        % Get parent indices based on their probability
        parentIdx = pickValProb(1:size(pop, 1), fitVals, nParent);

        % Put parents in the array
        parents(offsprIdx, :, :) = pop(parentIdx, :).';
    end


    % Generate parent vector
    if nargout > 1
        % Get the array into Parents x Offspring x Variables format
        parentsVec = permute(parents, [3, 1, 2]);

        % Reshape to the appropriate dimensions
        parentsVec = reshape(parentsVec, size(parents, 1) * size(parents, 3), size(parents, 2));
    else
        parentsVec = [];
    end
end