%% "Stochastic" local search for Memetic Algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 25/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: A "stochastic" (random) local search
%                to be used in the context of a
%                Memetic Algorithm.
% --------------------------------------------------
% Input
% 
% pop [numeric]: The population on which the local search will be applied.
% 
% fitVals [numeric]: The fitness values of each individual in the
%                    population to undergo local search optimisation.
% 
% nNeighbourPts [numeric]: Number of neighbouring points to investigate for
%                          a better value.
% 
% selMeth [char/string]: This is the method to be used to pick the
%                        neighbouring points. The available options are
%                        "Full", "Greedy". When "Full" is picked,
%                        "nNeighbourPts" are selected and the one that
%                        provides the highest improvement in the fitness
%                        value will be selected as the new individual,
%                        acting as the starting point for the new iteration
%                        (or final best if no iterations left). If "Greedy"
%                        is picked, when an individual that performs better
%                        than the current one is found the iteration is
%                        finished and this new value act as the starting
%                        point for the new iteration (or final best if no
%                        iterations left).
% 
% stepSize [numeric]: The maximum and minimum step size(s) in each
%                     direction/variable. This must be either a scalar, a
%                     two element vector, a vector with number of elements
%                     equal to the variables in each individual, or a 2xN
%                     matrix with N denoting the number of variables in
%                     each individual. When a scalar is provided, it
%                     denotes the fixed step size in all directions. A
%                     vector with two elements provides the minimum and
%                     maximum step sizes for all direction. A vector with
%                     N elements provides the fixed step for each
%                     direction. Finally, a 2xN matrix provides the minimum
%                     and maximum step for each direction.
% 
% nIters [numeric]: Number of iterations to be performed. The iterations
%                   will continue until "nIters" have been performed or the
%                   neighbouring individuals do not outperform the current
%                   one.
% 
% fitFunc [function_handle]: The function handle for the fitness
%                            calculation function.
% 
% vecExec [logical] (Optional): Whether the fitness function execution will
%                               be vectorised. [Default: false]
% 
% fitFuncParExec [logical] (Optional): Whether the fitness function will be
%                                      executed in parallel. The vectorised
%                                      flag takes precedence, so it
%                                      "vecExec" is set to true this
%                                      parameter is ignored.
%                                      [Default: false].
% 
% locOptParExec [logical] (Optional): Whether the local search will be
%                                     executed in parallel for each
%                                     individual. [Default: false].
% 
% --------------------------------------------------
% Output
% 
% optimPop [numeric]: The population after the local search has been
%                     performed.
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function optimPop = locSearch(pop, fitVals, nNeighbourPts, selMeth, stepSize, nIters, fitFunc, vecExec, fitFuncParExec, locOptParExec)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(7, 10);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(pop, "numeric", {'nonempty', 'nonnan', 'finite'}, mfilename, "Population", 1);
    validateattributes(fitVals, "numeric", {'vector', 'numel', size(pop, 1), 'real', 'finite', 'nonnan', 'nonempty'}, mfilename, "Fitness values", 2);
    validateattributes(nNeighbourPts, "numeric", {'scalar', 'nonempty', 'nonnan', 'finite', 'integer', 'positive'}, mfilename, "Number of points to test around an indiviudal", 3);

    validateattributes(selMeth, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Method to select points", 4);
    validatestring(selMeth, ["Full", "Greedy"], mfilename, "Method to select points", 4);

    if isscalar(stepSize)
        validateattributes(stepSize, "numeric", {'scalar', 'nonempty', 'nonnan', 'finite'}, mfilename, "Step size", 5);

        % Create step size matrix
        stepSize = [zeros(1, size(pop, 2)); ones(1, size(pop, 2)) * stepSize];
    elseif isvector(stepSize)
        if numel(stepSize) == 2
            validateattributes(stepSize, "numeric", {'vector', 'finite', 'nonempty', 'nonnan'}, mfilename, "Step size", 5);

            % Create step size matrix
            stepSize = stepSize(:) .* ones(2, size(pop, 2));
        elseif numel(stepSize) == size(pop, 2)
            validateattributes(stepSize, "numeric", {'vector', 'finite', 'nonnegative', 'nonempty', 'nonnan', 'real'}, mfilename, "Step size", 5);

            % Create step size matrix
            stepSize = [zeros(1, size(pop, 2)); stepSize(:).'];
        else
            error("locSearch(): Step size must be a vector with either two or as many as variables in an individual elements.")
        end
    elseif ismatrix(stepSize)
        validateattributes(stepSize, "numeric", {'2d', 'nrows', 2, 'ncols', size(pop, 2), 'finite', 'nonempty', 'nonnan'}, mfilename, "Step size", 5);
    else
        error("locSearcg(): Step size must be either a scalar, a two-element vector, a vector with number of elements equal to the number of paramters in each individual or a 2xN matrix where N is the number of variables.");
    end

    validateattributes(nIters, "numeric", {'scalar', 'finite', 'positive', 'nonempty', 'nonnan', 'real'}, mfilename, "Number of iterations", 6);
    validateattributes(fitFunc, "function_handle", {}, mfilename, "Fitness function", 7);

    % Validate optional parameters
    if nargin > 7
        validateattributes(vecExec, "logical", {'scalar', 'finite'}, mfilename, "Vectorised fitness function execution", 8);

        if isempty(vecExec)
            vecExec = false;
        end
    else
        vecExec = false;
    end

    if nargin > 8
        validateattributes(fitFuncParExec, "logical", {'scalar', 'finite'}, mfilename, "Parallelised fitness function execution", 9);

        if isempty(fitFuncParExec)
            fitFuncParExec = false;
        end
    else
        fitFuncParExec = false;
    end

    if nargin > 9
        validateattributes(locOptParExec, "logical", {'scalar', 'finite'}, mfilename, "Local search parallelised execution", 10);
        
        if isempty(locOptParExec)
            locOptParExec = false;
        end
    else
        locOptParExec = false;
    end


    % ====================================================
    % Perform the local search
    % ====================================================
    % Parameters
    rngFactor = stepSize(2, :) - stepSize(1, :); % Factor determining the range of the values
    sftFactor = stepSize(1, :); % Factor shifting the mean of the values
    nVars = size(pop, 2); % Number of variables
    
    % Pre-allocate
    optimPop = zeros(size(pop));

    % Go through the population
    parfor popIdx = 1:size(pop, 1)
        optimPop(popIdx, :) = pop(popIdx, :);

        % Loop conditions
        stopIter = false; iterIdx = 1;

        % Run for a predefined number of iterations
        while ~stopIter && iterIdx <= nIters
            if strcmpi(selMeth, "Full")
                % Add random steps
                locPop = rngFactor .* rand(nNeighbourPts, nVars) + sftFactor;
    
                % Calculate the positions to search
                locPop = optimPop(popIdx, :) + locPop;
    
                % Calculate fitness values
                [locFitVals, ~, locBestIdx] = calcFitness(fitFunc, locPop, vecExec, fitFuncParExec);
                
                % Test whether we did better than before or not
                if locFitVals(locBestIdx(1)) > fitVals(popIdx)
                    % Update individual
                    optimPop(popIdx, :) = locPop(locBestIdx(1), :);
                    fitVals(popIdx) = locFitVals(locBestIdx(1));
                else
                    % If we didn't do better stop iterations
                    stopIter = true;
                end

                % Increase iteration index
                iterIdx = iterIdx + 1;
            else
                for locIdx = 1:nNeighbourPts
                    % Add random steps
                    locPop = rngFactor .* rand(1, nVars) + sftFactor;
        
                    % Calculate the positions to search
                    locPop = optimPop(popIdx, :) + locPop;

                    % Get its fitness
                    locFitVal = calcFitness(fitFunc, locPop, vecExec, fitFuncParExec);

                    % Check how we did
                    if locFitVal > fitVals(popIdx)
                        % Update if we did better and go the next iteration
                        optimPop(popIdx, :) = locPop;
                        fitVals(popIdx) = locFitVal;

                        iterIdx = iterIdx + 1;
                        break;
                    end
                end
            end
        end
    end
end