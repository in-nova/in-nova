%% Impose constraints to a population in a Memetic Algorithm
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 29/11/2023 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Impose constraints to a population
%                in a Memetic Algorithm.
% --------------------------------------------------
% Input
% 
% constrFunc [function handle]: The fitness function to be called for the
%                               evaluation of the constraints. The function
%                               should take the population (or an
%                               individual) as input and return a logical
%                               value (true/false) for each one to state
%                               whether the constraints were met for this
%                               individual. The returned values of the
%                               function will be converted to logical (if
%                               this is not possible it will results in an
%                               error).
% 
% pop [numeric]: An NxM population matrix. N corresponds to the number of
%                the individuals in the population and M to the number of
%                variables in each individual.
% 
% 
% vecExec [logical] (Optional): Whether the execution of the evaluation
%                               function is vectorised. In this case, the
%                               function is expected to evaluate all
%                               individuals and provide the results in an
%                               Nx1 vector through a single call. In this
%                               case, N is the size of the population.
%                               [Default: false].
% 
% parExec [logical] (Optional): Whether the execution is to be parallelised
%                               or not. [Default: false]
% --------------------------------------------------
% Output
% 
% finalPop [numeric]: The constrained population.
% --------------------------------------------------
% Notes
% 
% - If the provided function can return the fitness of all the individuals
%   in a single call then "vecExec" should be set to true. This will
%   provide significantly shorter execution times.
% --------------------------------------------------
function finalPop = constrPop(constrFunc, pop, vecExec, parExec)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 4);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(constrFunc, "function_handle", {}, mfilename, "Evaluation function", 1);
    validateattributes(pop, "numeric", {'nonempty', 'nonnan', 'finite'}, mfilename, "Population", 2);

    % Validate optional arguments
    if nargin > 2
        validateattributes(vecExec, "logical", {'nonnan', 'finite', 'real', 'scalar'}, mfilename, "Vectorised execution", 3);
        
        if isempty(vecExec)
            vecExec = false;
        end
    else
        vecExec = false;
    end

    if nargin > 3
        validateattributes(parExec, "logical", {'nonnan', 'finite', 'real', 'scalar'}, mfilename, "Parallel execution", 4);
        
        if isempty(vecExec)
            parExec = false;
        end
    else
        parExec = false;
    end


    % ====================================================
    % Call the function to evaluate the constraints
    % ====================================================
    % Evaluate constraints
    if vecExec
        constrRes = feval(constrFunc,  pop);
    elseif parExec
        parfor idx = 1:size(pop, 1)
            constrRes(idx) = feval(constrFunc, pop(idx, :));
        end
    else
        for idx = size(pop, 1):-1:1
            constrRes(idx) = feval(constrFunc, pop(idx, :));
        end
    end

    % Keep only population that meets the requirements/constraints
    finalPop = pop(logical(constrRes), :);
end