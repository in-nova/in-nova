# Memetic Algorithm

Memetic Algorithms (MA), form an extension to the broader class of Evolutionary Algorithms (EA). A random search over an error space (or any other space the user will decide) is performed in some "guided" fashion. A bunch of "proposed" solutions are tested and evaluated based on their performance through a fitness function. Usually, the new solutions derive from those the "fittest" solutions, which are those that achieved the highest scores. Allowing some parts of the "bad" solutions to propagate to the new proposed solutions helps maintain diversity in the population.

What differentiates the MA from the EA is that a local search over the space is also performed to increase the performance of the solutions. The way, frequency and part of the population for which the search is performed constitute parameters that can be tuned.

More information on the implementation can be found in the [*Documentation*](https://gitlab.com/in-nova/in-nova/-/tree/main/Documentation/latex?ref_type=heads) folder.


## Dependencies
The implementation depends on functions from the [Utilities/Generic](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities/Generic?ref_type=heads). The necessary files are listed below:

- MATLAB
  - `pickValProb()`