# Active Noise Control

The field of *Active Noise Control* ANC) is a large, well established field. The current section holds only functions that are useful to the purposes of the IN-NOVA project and in no way is attempted to provide a complete library of ANC algorithms.

## Current structure of the section

The current content of the section is summarised below.

- Optimal tonal control
  - Frequency domain
    - Conventional ANC (no virtual sensing)
    - Remote Microphone Technique (observation filter)
    - FxLMS versions