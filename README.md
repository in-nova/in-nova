# IN-NOVA
This repository holds the open-access codebase used within the "Directional microphone arrays for remote microphone virtual sensing" sub-project, under the [IN-NOVA](https://in-nova-horizon.eu/) project . The codebase is divided into various sections, each relevant to a different topic/problem. The sections will be updated and more sections will be added on an as-needed basis, since the purpose of the project is not to create a software library to tackle the scientific problems.

The codebase under each section may or may not include code implementations in different languages but no plan exists to port each implementation in all available programming languages. More information about the codebase, the available implementations and topics/fields can be found in the [Documentation](https://gitlab.com/in-nova/in-nova/-/tree/main/Documentation/latex?ref_type=heads) of the project and its [Wiki](https://gitlab.com/in-nova/in-nova/-/wikis/home).


## Current structure

The current structure of the codebase is shown below. More information can be found in the [Wiki](https://gitlab.com/in-nova/in-nova/-/wikis/home) with separate pages for each category, and implementation.

- [Templates](https://gitlab.com/in-nova/in-nova/-/tree/main/Templates?ref_type=heads)
  - Function templates
- [Utilities](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities?ref_type=heads)
  - Generic
  - Geometries
- [Active Noise Control](https://gitlab.com/in-nova/in-nova/-/tree/main/Active%20Noise%20Control?ref_type=heads)
  - Optimal tonal control
    - Frequency domain
- [Virtual Sensing](https://gitlab.com/in-nova/in-nova/-/tree/main/Virtual%20Sensing?ref_type=heads)
  - Remote Microphone Technique
    - Frequency domain
    - Time domain
- [Optimisation](https://gitlab.com/in-nova/in-nova/-/tree/main/Optimisation/Memetic%20Algorithm?ref_type=heads)
  - Memetic Algorithm
- [Signal Processing](https://gitlab.com/in-nova/in-nova/-/tree/main/Signal%20Processing?ref_type=heads)
  - Array Processing
    - Frequency domain
    - Time domain
  - Generic
  - Measurements
- [Sound fields](https://gitlab.com/in-nova/in-nova/-/tree/main/Sound%20Fields?ref_type=heads)
  - Generation
    - Frequency domain
    - Time domain
    - Wave domain
  - Analysis
    - Wave domain Discrete Fourier Transforms


## Dependencies
Effort has been put towards minimising dependencies external to this repository. Even "inter-repository" dependencies (ex. a function under some field uses a general utility function) will be clearly stated in the relevant code documentation.


## Support
If you find any bugs, other issues and/or inconsistencies you can [e-mail](mailto::axilleaz@protonmail.com) Achilles Kappis at *<axilleaz@protonmail.com>* or *<A.Kappis@soton.ac.uk>*. More contacts will be added if/when new contributors join the project.


## Roadmap
There is no development roadmap for this project. The codebase is developed to cover the needs of the project as they show up, so the repository will be updated on an as-needed basis.

Although the repository is not considered to be *under construction* anymore, there are significant gaps in its structure. At the moment, the main focus is placed on updating code with new features and bug fixes and documenting ideas and useful features for the future (mainly as issues).

The port of the codebase to [Julia](https://julialang.org/) started a while ago, but it has currently been abandonded. The development requires considerable effort to provide a consistent and performant codebase. Instead of spending time to learn Julia to efficiently port the code, the effort will be put to improve the current codebase and possibly port it to [Python](https://www.python.org/), as it is one of the main languages used in computational sciences and signal processing. 


## Contributing
If you would like to contribute to the project you could do that in various ways, as listed below:

- Create issues for bugs, improvements and/or features.
- Fix a bug or develop an improvement and/or feature yourself. Clone the repository, create a new branch when done create a merge request.
- Report typos and/or inconsistencies in the documentation.
- Contribute to the documentation and/or the Wiki of the project.
- Create a CI configuration for automated testing, or develop unit tests.
- Port the codebase to other programming languages and/or environments.


#### Remarks

Since there is no CI set at the moment to perform automated testing, you are highly encouraged to test your contributions as much as possible and even contribute some unit tests if this aligns with your contributed content.

To try and maintain a coherent style in the codebase you are encouraged to use the template available in the [Templates](https://gitlab.com/in-nova/in-nova/-/tree/main/Templates?ref_type=heads) folder, although this is not necessary.  
Irrespective of style, it is highly advisable to name your variables in a meaningful manner and keep them as short as possible while still communicating their purpose. Comments are highly recommended in your code, especially where they can facilitate a better understanding of the code and the intent of the developer. Over-commented code is better than under-commented code.


## Contributors and acknowledgment

#### Contributors

The main contributor is Achilles Kappis.


#### Acknowledgments

Part of the `ptsOnSphere()` MATLAB/Octave function is an adaptation of a function provided by Prof. Jordan Cheer of the Institute of Sound & Vibration Research (ISVR), University of Southampton.


## License
The project is licensed under the ***MIT License***, which is a rather unrestrictive license, effectively passing the code to the public scope. For more information see the [MIT License](https://mit-license.org/), or the [LICENSE](https://gitlab.com/in-nova/in-nova/-/blob/main/LICENSE?ref_type=heads) file of the project.


## Versioning ##
The project uses [semantic versioning](https://semver.org/) and the current version is **v0.5.0**. The project hasn't reached v1.0.0 yet so changes that break backwards compatibility may (and have been) introduced in any version update. For information about the changes in each version consult the [CHANGELOG](https://gitlab.com/in-nova/in-nova/-/blob/main/CHANGELOG.md?ref_type=heads).


#### **Important**

If you are unable, or do not want to contribute under the current license please do **not** contribute to this project at all. All content within the project must be under the same license.
