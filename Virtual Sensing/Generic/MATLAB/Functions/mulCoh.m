%% Calculate the "multiple coherence"
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 17/08/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate "multiple coherence" between positions.
% --------------------------------------------------
% Input
% 
% Px [numeric]: The input matrix.
% 
% Py [numeric]: The output matrix. The number of rows must match the number
%               of columns of "Px".
% 
% Sqq [numeric] (Optional): The source power spectral density matrix. This
%                           must be a square matrix with each dimension
%                           matching the number of columns of "Px".
% 
% xRegFac [numeric] (Optional): A regularisation factor to be used in the
%                               inversion of the cross spectral density
%                               matrix of "Px". [Default: 0].
% 
% --------------------------------------------------
% Output
% 
% cohSq [numeric]: The multiple coherence squared.
% 
% coh [numeric]: The multiple coherence.
% 
% --------------------------------------------------
% Notes
% 
% - The implementation is based on the paper: "A study of coherence between
%   virtual signal and physical signals in remote acoustic sensing" by
%   P. Zhang, S. Wang, H. Duan, J. Tao, H. Zou and X. Qiu.
% --------------------------------------------------
function [cohSq, coh] = mulCoh(Px, Py, Sqq, xRegFac)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 4);
    nargoutchk(0, 2);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(Px, "numeric", {'nonempty', '2d'}, mfilename, "Input matrix", 1);
    validateattributes(Py, "numeric", {'nonempty', '2d', 'ncols', size(Px, 2)}, mfilename, "Output matrix", 2);

    % Validate optional arguments
    if nargin > 2 && ~isempty(Sqq)
        validateattributes(Sqq, "numeric", {'2d'}, mfilename, "Source strength(s)", 3);
        
        if isscalar(Sqq) || isvector(Sqq)
            Sqq = Sqq * eye(size(Px, 2));
        else
            validateattributes(Sqq, "numeric", {'2d', 'square', 'ncols', size(Px, 2)}, mfilename, "Source cross spectral density matrix", 3);
        end
    else
        Sqq = eye(size(Px, 2));
    end

    if nargin > 3 && ~isempty(xRegFac)
        validateattributes(xRegFac, "numeric", {'scalar', 'real', 'finite', 'nonnegative', 'nonnan'}, mfilename, "'Px' cross spectral density matrix regularisation value", 4);
    else
        xRegFac = 0;
    end


    % ====================================================
    % Validate input arguments
    % ====================================================
    % Calculate power spectral density matrices
    Sxx = Px * Sqq * Px';
    Syy = Py * Sqq * Py';
    Sxy = Px * Sqq * Py';

    % Calculate multiple coherence squared
    cohSq = diag((Sxy' * ((Sxx + xRegFac * eye(size(Sxx)))\Sxy))./Syy);
    cohSq = real(cohSq); % Make sure to get rid of residual imaginary parts

    if nargout > 1
        coh = sqrt(cohSq);
    end
end