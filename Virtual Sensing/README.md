# Virtual Sensing

The problem of *Virtual Sensing* is about estimating a sound field quantity at a position where sensors are not present. This is a generic problem and has been tackled in various ways. At the moment only the Remote Microphone Technique has been implemented in the project.

## Current Structure

The current structure of the section is shown below

- Generic
  - Multiple coherence
- Remote Microphone Technique
  - Observation filter calculation
    - Frequency domain (tonal)
    - Time domain (broadband and causal)


## Dependencies
There are no functional dependencies, however, the implementations are based on the geometries generated by functions in [Utilities/Geometries](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities/Geometries) and transfer functions calculated with functions from [Sound Fields](https://gitlab.com/in-nova/in-nova/-/tree/main/Sound%20Fields). The relevant files are listed below:

- MATLAB
  - Utilities
    - Geometries
      - `rcvGeo()`
      - `srcGeo()`
      - `virtMicGeo()`
  - Sound Fields
    - `ptSrcField()`
    - `ptSrcFieldTD()`
    - `planeWave()`

## Examples
A condensed example of a very simple estimation case is shown below and more examples can be found in the [*Documentation*](https://gitlab.com/in-nova/in-nova/-/tree/main/Documentation/latex?ref_type=heads), along with a complete description of all the functions.

```matlab
% Place primary sources, physical and virtual microphones
mPos = rcvGeo("UCA", 4, 0.5); % Omni mic positions
vPos = srcGeo("Diffuse", 0, 3, 10); % Primary source positions
ePos = virtMicGeo("Cube", [2, 2, 0], [11, 11, 0]); % Virtual/error mic positions on a grid

% Calculate transfer functions
Pm = ptSrcField(vPos, mPos, 7.5e2); % Primary-to-monitoring transfer function (f = 750 Hz)
Pe = ptSrcField(vPos, ePos, 7.5e2); % Primary-to-virtual transfer function (f = 750 Hz)

% Perform estimation and acquire the normalised square estimation error
Oopt = obsFilt(Pe, Pm, [], 1e2, 15); % Use regularisation factor 100 to calculate the observation filter and add noise with Signal-to-Noise Ratio of 15 dB.
[~, ~, ~, normSqrErr] = obsFileEst(Pm, Oopt, Pe); % Perform estimation
```

#### Acknowledgments

None.

## References

[1] A. Roure, A. Albarrazin, *"The remote microphone technique for active noise control"*, Inter-Noise and Noise-Con Congress and Conference Proceedings, Active99, Fort Lauderdale FL, pp. 1233-1244(12).\
[2] S. J. Elliott, J. Cheer, *"Modeling local active sound control with remote sensors in spatially random pressure fields"*, Journal of the Acoustical Society of America, 137, pp. 1936-1946, (2015).
