%% Perform pressure estimation with observation filters
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 19/12/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Perform sound pressure estimation with observation
%                filters.
% --------------------------------------------------
% Input
% 
% 
% Pm [numeric]: The transfer function from sources to the monitoring
%               microphones. The dimensions of the matrix must be KxM,
%               where K is the number of monitoring microphones and M is
%               the number of sources.
% 
% obsFilt [numeric]: The observation filters. This must be a matrix with
%                    dimensions NxΚ, where N is the number of virtual
%                    microphones and Κ is the number of monitoring
%                    (physical) microphones.
% 
% Pe [numeric] (Optional): The transfer function from sources to the
%                          virtual microphones. The dimensions of the
%                          matrix must be NxM, where N is the number of
%                          virtual microphones and M the number of sources.
%
% Svv [numeric] (Optional): The source cross spectral density matrix. This
%                           must be a square (MxM) symmetric matrix with
%                           the cross power spectral density of the
%                           sources. [Default: eye(M)]
% 
% --------------------------------------------------
% Output
% 
% est [numeric]: The estimated complex pressure(s) at the virtual
%                microphone position(s).
% 
% err [numeric]: If "Pe" is provided this is the error of the estimated
%                complex pressure(s), otherwise it is NaN.
% 
% errSqr [numeric]: If "Pe" is provided this is the sum of the squared
%                   errors of the estimated complex pressure(s), otherwise
%                   it is NaN.
% 
% normErrSqr [numeric]: If "Pe" is provided this is the sum of the squared
%                       errors of the estimated complex pressure(s)
%                       normalised to the squared pressure(s) of the
%                       virtual microphones. Otherwise it is NaN.
% 
% See [numeric]: If "Pe" is provided this is the normalisation factor
%                (cross spectral density of the error microphones) for
%                each virtual microphone position. Otherwise it is NaN.
%                "errSqr"./"See" gives "normErrSqr".
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [est, err, errSqr, normErrSqr, See] = obsFiltEst(Pm, O, Pe, Svv)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 4);
    nargoutchk(0, 5);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(Pm, "numeric", {'nonnan', 'nonempty', 'finite'}, mfilename, "Monitoring microphone pressure", 1);
    validateattributes(O, "numeric", {}, mfilename, "Observation filters", 2);


    % Validate optional arguments
    if nargin > 2 && ~isempty(Pe)
        validateattributes(Pe, "numeric", {'nonnan', 'nonempty', 'finite'}, mfilename, "Virtual microphone pressure", 1);
    else
        Pe = NaN;
    end

    if nargin > 3 && ~isempty(Svv)
        validateattributes(Svv, "numeric", {'2d', 'nonnan', 'finite'}, mfilename, "Source cross spectral density matrix", 3)

        % Check for correct dimensions
        if diff(size(Svv))
            error("The source power spectral density matrix must be a square matrix");
        elseif size(Svv, 1) ~= size(Pe, 1)
            error("The number of rows of the source power spectral density matrix must be equal to the number of sources");
        end
    else
        Svv = eye(size(Pe, 2));
    end


    % ====================================================
    % Perform estimation
    % ====================================================
    % Calculate pressure estimates
    est = O * Pm;

    % Estimation error
    if nargout > 1
        err = Pe - est;
    end

    % Sum of squared estimation errors
    if nargout > 2
        parfor eIdx = 1:size(Pe, 1)
            errSqr(eIdx, 1) = err(eIdx, :) * Svv * err(eIdx, :)';
        end
    end

    % Normalised squared errors
    if nargout > 3
        % Calculate power true spectral density at virtual microphones
        parfor eIdx = 1:length(errSqr)
            See(eIdx, 1) = Pe(eIdx, :) * Svv * Pe(eIdx, :)';
        end

        % Normalise the squared errors with the true power spectral density
        normErrSqr = errSqr./See;
    end
end