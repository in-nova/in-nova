%% Calculate optimal observation filters in the time domain
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 05/01/2025 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the optimal, in the least squares sense,
%                observation filters for the Remote Microphone Technique in
%                the time domain.
% --------------------------------------------------
% Input
% 
% e [numeric]: The measurement(s) at the virtual microphone position(s).
%              This must be an IxNxJ matrix with I the length of the
%              measurements in samples, N the number of virtual microphones 
%              and J the number of trials/sound field realisations.
% 
% m [numeric]: The measurement(s) at the monitoring microphone position(s).
%              This must be an KxMxJ matrix with K representing the length
%              of the measurements in samples, M the number of monitoring
%              microphones and J the number of trials/sound field
%              realisations. The number of trials must be the same as in
%              the argument "e".
% 
% beta [numeric] (Optional): The regularisation factor. This must be a real
%                            non-negative scalar. [Default: 0].
% 
% snrVal [numeric] (Optional): This is the Root-Mean-Square (RMS)
%                              Signal-to-Noise Ratio (SNR) in the
%                              monitoring microphone signals. This must be
%                              either a scalar indicating the common SNR
%                              value for all microphone signals, or a
%                              vector of dimensions Kx1, containing the SNR
%                              values for each microphone. The values must
%                              be real or, for the noiseless case Inf. See
%                              the Notes below for references on how this
%                              value is calculated. [Default: Inf].
% 
% filtLen [numeric] (Optional): The length of the observation filters in
%                               samples. This must be a positive scalar, at
%                               less than or equal to K, the number of
%                               samples in the monitoring microphone
%                               signals. [Default: size(m, 2)].
% 
% delay [numeric] (Optional): The delay to be added to the virtual
%                             microphone signals, effectively implementing
%                             the delayed Remote Microphone Technique. This
%                             can be either a scalar, in which case the
%                             same delay is applied to all virtual
%                             microphone signals, or a vector with each
%                             element holding the delay for each virtual
%                             microphone signal respectively. The delays
%                             must be real non-negative values less than
%                             or equal to I, the length of the virtual
%                             microphone signals. If "fs" is provided,
%                             these values correspond to seconds otherwise
%                             they are in samples and must be integer.
%                             [Default: 0].
% 
% fs [numeric] (Optional): The sampling frequency. If this value is given,
%                          the delay is treated as being in seconds,
%                          otherwise it is in samples. This argument must
%                          be a positive real integer.
% 
% --------------------------------------------------
% Output
% 
% O [numeric]: The observation filters. This is an NxfirLenxMxJ array.
% 
% Rme [numeric]: The cross-correlation matrices between the monitoring and
%                virtual microphone signals. This is an filtLenxMxNxJ
%                array.
% 
% Rmm [numeric]: The cross-correlation matrices of the monitoring
%                microphone signals. This is an filtLenxfiltLenxMxMxJ
%                array.
% 
% Ovec [numeric]: This is a matrix with the observation filters
%                 stacked/vectorised, so that they can be applied to a
%                 stacked/vectorised monitoring microphone measurements
%                 vector. The dimensions of this matrix are NxTotFiltLenxJ,
%                 where TotFiltLen is equal to M times filtLen. This way,
%                 stacking the monitoring microphone measured signals one
%                 can perform Ovec(:, :, jIdx) * dm (dm is the vectorised
%                 monitoring microphone signals vector) to calculate the
%                 estimated measurements at the N virtual microphone
%                 positions.
% 
% RmeMtx [numeric]: This is the matrix with the cross-correlation vectors
%                   between the monitoring and virtual microphones
%                   stacked/vectorised. The dimensions of the matrix are
%                   NxTotFiltLenxJ.
% 
% RmmMtx [numeric]: This is the matrix with the cross-correlation matrices
%                   of the monitoring microphone signals stacked together.
%                   The dimensions are TotFiltLenxTotFiltLenxJ.
% 
% condNum [numeric]: The condition number of the monitoring microphone
%                    cross-correlation matrix "RmmMtx". This is a vector of
%                    length J, with each element corresponding to the
%                    condition number of the matrix for each trial/sound
%                    field realisation.
%
% mMtx [numeric]: The monitoring microphone signals vectorised so that they
%                 can be used directly with either "Ovec" or "Oopt"
%                 arguments to perform the filtering/estimation.
% 
% Omean [numeric]: The observation filters calculated with the correlation
%                  matrices RmeMtx and RmmMtx averaged over the
%                  trials/sound field realisations (these are the
%                  "RmeMtxMean" and "RmmMtxMean" output arguments). This is
%                   an NxfirLenxM array.
% 
% RmeMean [numeric]: The cross-correlation matrices between the monitoring
%                    and virtual microphone signals averaged over the
%                    trials/sound field realisations. This is an
%                    NxMxfirLen array.
% 
% RmmMean [numeric]: The cross-correlation matrices of the monitoring
%                    microphone signals averaged over the trials/sound
%                    field realisations. This is an MxMxfirLenxfirLen
%                    array.
% 
% Oopt [numeric]: This is a matrix with the observation filters
%                 stacked/vectorised, so that they can be applied to a
%                 stacked/vectorised monitoring microphone measurements
%                 vector, calculated with the correlation matrices RmeMtx
%                 and RmmMtx averaged over all trials/sound field
%                 realisations (the "RmeMtxMean" and "RmmMtxMean" output
%                 arguments). The dimensions of this matrix are
%                 NxTotFiltLen.
% 
% RmeMtxMean [numeric]: This is the matrix with the cross-correlation
%                       vectors between the monitoring and virtual
%                       microphones stacked/vectorised, averaged over the
%                       trials/sound field realisations. The dimensions of
%                       the matrix are NxTotFiltLen.
% 
% RmmMtxMean [numeric]: This is the matrix with the cross-correlation
%                       matrices of the monitoring microphone signals
%                       stacked together and averaged over the trials/sound
%                       field realisations. The dimensions are
%                       TotFiltLenxTotFiltLen.
% 
% --------------------------------------------------
% Notes
% 
% - The calculations are based on the paper: "Modeling local active sound
%   control with remote sensors in spatially random pressure fields" by
%   Stephen J. Elliott and Jordan Cheer.
% 
% - The SNR at the monitoring microphones calculation is based on the
%   paper: "Combining the remote microphone technique with head-tracking
%   for local active sound control" by W. Jung, S. J. Elliott and J. Cheer.
% 
% --------------------------------------------------
function [O, Rme, Rmm, Ovec, RmeMtx, RmmMtx, condNum, mMtx, Omean, RmeMean, RmmMean, Oopt, RmeMtxMean, RmmMtxMean] = obsFiltTd(e, m, beta, snrVal, filtLen, delay, fs)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 7);
    nargoutchk(0, 14);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(e, "numeric", {'3d', 'real', 'nonnan', 'nonempty', 'finite'}, mfilename, "Virtual microphone measurements", 1);
    validateattributes(m, "numeric", {'3d', 'real', 'nonnan', 'nonempty', 'finite', 'size', [NaN, NaN, size(e, 3)]}, mfilename, "Monitoring microphone measurements", 2);

    % Validate optional arguments
    if nargin > 2 && ~isempty(beta)
        validateattributes(beta, "numeric", {'scalar', 'nonnegative', 'nonnan', 'nonempty', 'finite', 'real'}, mfilename, "Regularisation factor", 3);
    else
        beta = 0;
    end

    if nargin > 3 && ~isempty(snrVal)
        validateattributes(snrVal, "numeric", {'vector', 'real', 'nonnan', 'nonempty'}, mfilename, "Normalised Root-Mean-Square Singal-to-Noise Ratio of the monitoring microphones", 4);

        % Make sure snrVal has correct dimensions
        if ~isscalar(snrVal) && numel(snrVal) ~= size(m, 2)
            error("SNR must be either a scalar or its length must match the number of monitoring microphones.");
        end

        if sum(snrVal == -Inf) > 0
            error("SNR value cannot be equal to -Inf");
        end
    else
        snrVal = Inf;
    end

    if nargin > 4 && ~isempty(filtLen)
        validateattributes(filtLen, "numeric", {'scalar', 'positive', 'nonnan', 'real', 'nonempty', 'finite', '<=', size(m, 1)}, mfilename, "Length of observation filters", 5);
    else
        filtLen = size(m, 2);
    end

    if nargin > 5 && ~isempty(delay)
        validateattributes(delay, "numeric", {'vector', 'nonnegative', 'nonnan', 'finite', 'real', 'nonempty', '<=', size(e, 1)}, mfilename, "Delay to be added to virtual microphone signals to implement the delayed Remote Microphone Technique", 6);

        if isscalar(delay)
            delay = delay * ones(size(e, 2), 1);
        elseif length(delay) ~= size(e, 2)
            error("The 'delay' argument must be either a scalar, or a vector with number of elements equal to the number of virtual microphone signals.");
        end
    else
        delay = zeros(size(e, 2), 1);
    end

    if nargin > 6 && ~isempty(fs)
        validateattributes(fs, "numeric", {'scalar', 'integer', 'positive', 'nonnan', 'nonempty', 'finite', 'real'}, mfilename, "The sampling frequency", 7);
    else
        fs = [];
    end
    

    % ====================================================
    % Pre-process data
    % ====================================================
    % Delay the virtual microphone signals
    if logical(delay)
        if isempty(fs)
            for jIdx = size(e, 3):-1:1
                e(:, :, jIdx) = delayseq(e(:, :, jIdx), delay);
            end
        else
            for jIdx = size(e, 3):-1:1
                e(:, :, jIdx) = delayseq(e(:, :, jIdx), delay, fs);
            end
        end
    end

    % ====================================================
    % Calculate cross- and auto-correlation matrices
    % ====================================================
    % Go through the trials/sound field realisations
    for jIdx = size(m, 3):-1:1
        % Go through the monitoring microphones
        for mIdx = size(m, 2):-1:1
            tmp = m(:, mIdx, jIdx);

            % Calculate the cross-correlations between virtual and monitoring microphones
            parfor eIdx = 1:size(e, 2)
                corr = xcorr(tmp, e(:, eIdx, jIdx), filtLen);

                Rme(:, mIdx, eIdx, jIdx) = corr(filtLen + 1:-1:2);
            end

            % Go through the monitoring microphones to calculate the monitoring microphone correlation matrices
            for mmIdx = mIdx:-1:1
                % Auto-correlation matrices are Toeplitz symmetric
                if mIdx == mmIdx
                    corr = xcorr(m(:, mmIdx, jIdx), m(:, mmIdx, jIdx), filtLen);

                    Rmm(:, :, mIdx, mmIdx, jIdx) = toeplitz(corr(filtLen + 1:-1:2));
                else
                    corr = xcorr(m(:, mIdx, jIdx), m(:, mmIdx, jIdx), filtLen);

                    % Cross-correlation matrices
                    for iIdx = filtLen-1:-1:0
                        Rmm(:, iIdx + 1, mIdx, mmIdx, jIdx) = corr(iIdx + (filtLen + 1:-1:2));
                    end
                    Rmm(:, :, mmIdx, mIdx, jIdx) = squeeze(Rmm(:, :, mIdx, mmIdx, jIdx)).';
                end
            end
        end
    end

    % ====================================================
    % Post-process cross- and auto-correlation matrices
    % ====================================================
    % "Reshape" the data
    RmeMtx = reshape(permute(Rme, [2, 1, 3, 4]), prod(size(Rme, [1, 2])), size(Rme, 3), size(Rme, 4));
    RmmMtx = reshape(permute(Rmm, [3, 1, 4, 2, 5]), prod(size(Rmm, [1, 3])), prod(size(Rmm, [2, 4])), size(Rmm, 5));

    % ====================================================
    % Calculate observation filters
    % ====================================================
    % Regularisation
    regMtx = beta * eye(size(RmmMtx, 1));

    for jIdx = size(RmmMtx, 3):-1:1
        if ~isinf(snrVal)
            snrMtx = 10^(-snrVal/10); % Convert SNR to linear value
            snrMtx = (snrMtx^2) * norm(RmmMtx(:, :, jIdx), 'fro')/(size(m, 2)^2); % Calculate the appropriate SNR amplitude values
        else
            snrMtx = 0;
        end
        snrMtx = snrMtx * eye(size(RmmMtx, 1)); % Make the matrix
        denomMtx = RmmMtx(:, :, jIdx) + regMtx + snrMtx;

        Ovec(:, :, jIdx) = RmeMtx(:, :, jIdx).'/denomMtx;

        % Condition number of RmmMtx for each trial/sound field realisation
        if nargout > 6
            condNum(jIdx) = cond(denomMtx);
        end
    end

    % "Split" observation filter vector to observation filters per monitoring and virtual microphone
    O = permute(reshape(Ovec, size(Ovec, 1), size(m, 2), filtLen, size(Ovec, 3)), [1, 3, 2, 4]);

    % ====================================================
    % Provide additional output arguments
    % ====================================================
    % The monitoring microphone signals vectorised per trial/sound field realisation
    if nargout > 7
        mMtx = reshape(m, prod(size(m, [1, 2])), size(m, 3));
    end

    % Observation filter calculated with the mean Rme and Rmm over the trials/sound field realisation
    if nargout > 8
        % Average the RmeMtx and RmmMtx matrices
        RmeMtxMean = mean(RmeMtx, 3);
        RmmMtxMean = mean(RmmMtx, 3);

        % Calculate the observation filter
        Oopt = RmeMtxMean.'/(RmmMtxMean + beta * eye(size(RmmMtxMean)));

        % Reshape
        Omean = permute(reshape(Oopt, size(Oopt, 1), size(m, 2), filtLen), [1, 3, 2]);
    end
    
    % Mean cross-correlations between monitoring and virtual microphones over trials/sound field realisations
    if nargout > 9
        RmeMean = mean(Rme, 4);
    end

    % Mean cross-correlations of monitoring microphones over trials/sound field realisations
    if nargout > 10
        RmmMean = mean(Rmm, 5);
    end
end