\contentsline {lstlisting}{\numberline {2.1}{\ignorespaces \texttt {isEven} MATLAB\textsuperscript {\texttrademark } function header.}}{4}{lstlisting.2.1}%
\contentsline {lstlisting}{\numberline {2.2}{\ignorespaces \texttt {isEven} fist example. Finding out whether the \textit {Nyquist} frequency is present in a single-sided spectrum}}{5}{lstlisting.2.2}%
\contentsline {lstlisting}{\numberline {2.3}{\ignorespaces \texttt {isEven} second example. Pick the largest even value from a two-dimensional matrix and use it to seed a random generator.}}{5}{lstlisting.2.3}%
\contentsline {lstlisting}{\numberline {2.4}{\ignorespaces \texttt {isOdd} MATLAB\textsuperscript {\texttrademark } function header.}}{6}{lstlisting.2.4}%
\contentsline {lstlisting}{\numberline {2.5}{\ignorespaces Example of \texttt {isOdd} function. Pick only odd-numbered samples to undersample a signal vector}}{6}{lstlisting.2.5}%
\contentsline {lstlisting}{\numberline {2.6}{\ignorespaces \texttt {intersectThree} MATLAB\textsuperscript {\texttrademark } function header.}}{7}{lstlisting.2.6}%
\contentsline {lstlisting}{\numberline {2.7}{\ignorespaces Example with \texttt {intersectThree} MATLAB\textsuperscript {\texttrademark } function. Find common values among three vectors holding uniform angle intervales generated with different angular steps.}}{8}{lstlisting.2.7}%
\contentsline {lstlisting}{\numberline {2.8}{\ignorespaces \texttt {randUniqRows} MATLAB\textsuperscript {\texttrademark } function header.}}{8}{lstlisting.2.8}%
\contentsline {lstlisting}{\numberline {2.9}{\ignorespaces Example with \texttt {randUniqRows} MATLAB\textsuperscript {\texttrademark } function. Pick at random some time-domain recordings that have different RMS values (based on a specified tolerance).}}{9}{lstlisting.2.9}%
\contentsline {lstlisting}{\numberline {3.1}{\ignorespaces \texttt {obsFilt} MATLAB\textsuperscript {\texttrademark } function header.}}{23}{lstlisting.3.1}%
