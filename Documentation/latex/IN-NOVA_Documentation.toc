\contentsline {chapter}{Listings}{ii}{dummy.1}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Codebase structure}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Licence}{2}{section.1.2}%
\contentsline {chapter}{\numberline {2}Utilities}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}MATLAB\textsuperscript {\texttrademark }/GNU Octave}{3}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}\texttt {isEven}}{4}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}\texttt {isOdd}}{5}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}\texttt {intersectThree}}{7}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}\texttt {randUniqRows}}{8}{subsection.2.1.4}%
\contentsline {chapter}{\numberline {3}Active Noise Control}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}Active Control}{10}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Tonal control}{12}{subsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.1.1}Tonal control formulation}{12}{subsubsection.3.1.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Optimal filter estimation}{14}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Filtered-x LMS}{14}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}Virtual sensing}{15}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Remote Microphone Technique}{16}{subsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.1.1}Problem formulation}{17}{subsubsection.3.2.1.1}%
\contentsline {subsubsection}{\numberline {3.2.1.2}MATLAB\textsuperscript {\texttrademark } implementation}{23}{subsubsection.3.2.1.2}%
\contentsline {chapter}{\numberline {4}Optimisation}{25}{chapter.4}%
