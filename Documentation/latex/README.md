# IN-NOVA codebase documentation
The documentation is developed in *LaTeX* format. It can easily be imported to [*Overleaf*](https://www.overleaf.com/home-2) to work on new additions and corrections. However, the repository contains all needed files to work on the documentation in any environment.

## Dependencies

The documentation requires certain packages to be compiled. These are:

- natbib
- bibentry
- tikz
- attrib
- xcolor
- defaultsans
- mathpazo
- setspace
- geometry
- fancyhdr
- amsmath
- amsfonts
- amssymb
- amscd
- amsthm
- xspace
- caption
- graphicx
- epstopdf
- subcaption
- booktabs
- rotating
- listings
- hyperref
- titling
- tocbibind
- longtable
- xifthen

Most, if not all packages usually come with the default distribution of *LaTeX* development environments. If this is not the case, the user/developer must handle the acquisition of the missing packages as they are not provided in this repository.

## Contributing
If you would like to contribute to the project you could do that in various ways, as listed below:

- Report typos and/or inconsistencies.
- Contribute to the documentation and/or the Wiki of the project.
- Port the documentation to other document writting environments such as MS-Word™ or Author™.

#### Remarks

To try and maintain a coherent style in the documentation you are encouraged to use the already existing files, or copy and adapt them. Although this is not necessary, it will greatly increase the readability of the documentation and allow for consistent formatting.

Irrespective of style, it is highly advised to follow the format of the current documentation as shown below. If you would like to suggest a change in the structure of the documentation you can raise an issue, develop the changes and then push a merge request.

- Topic/Project
    - Sub-project_1
        - Task_1/problem_1
            - Introduction - problem formulation
            - Development-environment_1/programming-language_1
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_2/programming-language_2
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_3/programming-language_3
                - Interface
                - Examples
                - Additional information and remarks
        - Task_2/problem_2
            - Introduction - problem formulation
            - Development-environment_1/programming-language_1
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_2/programming-language_2
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_3/programming-language_3
                - Interface
                - Examples
                - Additional information and remarks
    - Sub-project_2
        - Task_1/problem_1
            - Introduction - problem formulation
            - Development-environment_1/programming-language_1
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_2/programming-language_2
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_3/programming-language_3
                - Interface
                - Examples
                - Additional information and remarks
        - Task_2/problem_2
            - Introduction - problem formulation
            - Development-environment_1/programming-language_1
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_2/programming-language_2
                - Interface
                - Examples
                - Additional information and remarks
            - Development-environment_3/programming-language_3
                - Interface
                - Examples
                - Additional information and remarks