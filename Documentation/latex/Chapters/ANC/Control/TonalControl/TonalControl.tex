%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Tonal control subsection %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tonal control} \label{sec:TonalControl}

% ==========================================
% Introduction
% ==========================================
The ANC problem is formulated in the frequency domain for narrowband disturbances considered to be realisations of \textit{Wide Sense Stationary} (WSS) processes and all signals will be described by their \textit{Cross Spectral Densities} (CSD) at a single frequency. The generic formulation presented here enables the evaluation of measurement and control setups with an arbitrary number of reference, monitor and error sensors and primary and secondary sources.


% ==========================================
% Problem formulation
% ==========================================
\subsubsection{Tonal control formulation} \label{sec:TonalANCFormulation}

Referencing \refFig{fig:ANCBlock}, primary (disturbance) sources with strengths $\V{v}$ generate the fields $\V{x}$ and $\V{d}$ at the reference and error sensor positions through the transfer functions $\M{P}_{\unit{x}}$ and $\M{P}_{\unit{e}}$. The signal vectors are given by

\begin{subequations}
	\begin{align}
		\V{x} = \M{P}_{\unit{x}} \V{v} \label{eq:ReferenceSignal}\\
		\V{d}_{\unit{e}} = \M{P}_{\unit{e}} \V{v} \label{eq:ErrorSignal}
	\end{align}
\end{subequations}


The field $\V{x}$ sensed at the reference sensors is used to drive the secondary source signals $\V{u} = \left[ u_{\unit{1}}, u_{\unit{2}}, \ldots, u_{N_{\unit{u}}} \right]^{T}$ of $N_{\unit{u}}$ secondary sources after being filtered by the filter matrix $\M{W}$. The control signals $\V{u}$ propagate to the monitoring and reference microphone positions through the transfer functions $\M{G}_{\unit{m}}$ and $\M{G}_{\unit{s}}$ respectively. $\hat{\M{G}}_{\unit{s}}$ denotes the estimate of the feedback path which is subtracted from $\V{s} = \V{x} + \M{G}_{\unit{s}} \V{u}$ to recover $\V{x}$. Expanding \refEq{eq:ANCErr}, the signals of $N_{\unit{e}}$ error microphones are thus given by

\begin{equation} \label{eq:TonalANCErr}
	\V{e} = \V{d}_{\unit{e}} + \V{y} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \V{u} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \M{W} \hat{\V{s}}.
\end{equation}

If $\hat{\M{G}}_{\unit{s}} = \M{G}_{\unit{s}}$ then the reference signal is perfectly recovered and the system is purely feed-forward. The error is then given by

\begin{equation} \label{eq:TonalANCErrFF}
	\V{e} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \M{W} \hat{\V{s}} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \M{W} \left[ \M{P}_{\unit{x}} \V{v} + \left( \M{G}_{\unit{s}} - \hat{\M{G}}_{\unit{s}} \V{u} \right) \right] = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \M{W} \M{P}_{\unit{x}} \V{v}.
\end{equation}

The optimal, in the least squares sense, filter $\M{W}_{\unit{opt}}$ is calculated by minimising the function

\begin{equation} \label{eq:LSANCMinFunc}
	\begin{split}
		J_{2} & = \unit{E} \left[ \V{e}^{\unit{H}} \V{e} \right] = \tr \left\{ \unit{E} \left[ \V{e} \V{e}^{\unit{H}} \right] \right\} \\
		&  = \tr \left\{ \unit{E} \left[ \left( \V{d}_{\unit{e}} + \M{G}_{\unit{e}} \M{W} \V{x} \right) \left( \V{d}_{\unit{e}} + \M{G}_{\unit{e}} \M{W} \V{x} \right)^{\unit{H}} \right] \right\} \\
		& = \tr \left\{ \M{S}_{\unit{ee}} + \M{S}_{\unit{xe}} \M{W}^{\unit{H}} \M{G}_{\unit{e}}^{\unit{H}} + \M{G}_{\unit{e}} \M{W} \M{S}_{\unit{xe}}^{\unit{H}} + \M{G}_{\unit{e}} \M{W} \M{S}_{\unit{xx}} \M{W}^{\unit{H}} \M{G}_{\unit{e}}^{\unit{H}} \right\},
	\end{split}
\end{equation}

where $\unit{E} \left[ \, \cdot \, \right]$ denotes expectation, $\left[ \, \cdot \, \right]^{\unit{H}}$ Hermitian conjugation and the cross spectral density matrices\footnote{Sometimes, when the expectation of a signal vector with its Hermitian is taken, the matrix is termed the power spectral density matrix but the power spectral densities of the signals are the diagonal terms and the off-diagonal terms correspond to the cross spectral densities of the sources, thus the latter is a more generic term, in the opinion of the author, and is used in this work to describe all matrices of this form.} are given by

\begin{subequations}
	\begin{align}
		\M{S}_{\unit{ee}} & = \unit{E} \left[ \V{d}_{\unit{e}} \V{d}_{\unit{e}}^{\unit{H}} \right] = \unit{E} \left[ \M{P}_{\unit{e}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{e}}^{\unit{H}} \right] = \M{P}_{\unit{e}} \M{S}_{\unit{vv}} \M{P}_{\unit{e}}^{\unit{H}}\\
		\M{S}_{\unit{xx}} & = \unit{E} \left[ \V{x} \V{x}^{\unit{H}} \right] = \unit{E} \left[ \M{P}_{\unit{x}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{x}}^{\unit{H}} \right] = \M{P}_{\unit{x}} \M{S}_{\unit{vv}} \M{P}_{\unit{x}}^{\unit{H}}\\
		\M{S}_{\unit{xe}} & = \unit{E} \left[ \V{d}_{\unit{e}} \V{x}^{\unit{H}} \right] = \unit{E} \left[ \M{P}_{\unit{e}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{x}}^{\unit{H}} \right] = \M{P}_{\unit{e}} \M{S}_{\unit{vv}} \M{P}_{\unit{x}}^{\unit{H}}
	\end{align}
\end{subequations}

In the equations above $\M{S}_{\unit{vv}} = \unit{E} \left[ \V{v} \V{v}^{\unit{H}} \right]$ is the cross spectral density of the source signals. After the optimal filter $\M{W}_{\unit{opt}}$ is calculated, \refEq{eq:TonalANCErrFF} can be used to calculate the error at an arbitrary position due to the primary and secondary disturbances like

\begin{equation} \label{eq:TonalANCErrFFOpt}
	\V{e} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \M{W}_{\unit{opt}} \M{P}_{\unit{x}} \V{v}.
\end{equation}







% ==========================================
% Optimal filter estimation
% ==========================================
\subsection{Optimal filter estimation} \label{sec:ANCOptFilt}

The optimal filter $\M{W}_{\unit{opt}}$ is caclulated by minimising \refEq{eq:LSANCMinFunc}. The derivative of the function $J_{2}$ with respect to $\M{W}$ must be set to zero and then solved for $\M{W}$. The derivative is calculated like

\begin{equation} \label{eq:LSANCDerivative}
	\begin{split}
		\frac{\partial J_{2}}{\partial \M{W}} & = \frac{\partial }{\partial \M{W}} \tr \left\{ \M{S}_{\unit{ee}} + \M{S}_{\unit{xe}} \M{W}^{\unit{H}} \M{G}_{\unit{e}}^{\unit{H}} + \M{G}_{\unit{e}} \M{W} \M{S}_{\unit{xe}}^{\unit{H}} + \M{G}_{m} \M{W} \M{S}_{\unit{xx}} \M{W}^{\unit{H}} \M{G}_{\unit{e}}^{\unit{H}} \right\} \\
		& = 2 \M{G}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} + 2\M{G}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \M{W} \M{S}_{\unit{xx}}
	\end{split}
\end{equation}

Now, setting \refEq{eq:LSANCDerivative} to zero and solving for the filter $\M{W}$ we get

\begin{equation} \label{eq:LSANCSol}
	\begin{split}
		\frac{\partial J_{2}}{\partial \M{W}} & = \0 \implies
		2 \M{G}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} + 2\M{G}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \M{W} \M{S}_{\unit{xx}} = \0 \implies \M{W}_{\unit{opt}} = \left( \M{G}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \M{G}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} \M{S}_{\unit{xx}}^{-1}
	\end{split}
\end{equation}

where $\0$ denotes the zero vector. A regularisation factor $\beta_{\unit{\tiny{G}}} \I$ can be used to make sure the inversion $\left( \M{G}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1}$ is not ill-posed. The symbol $\I$ denotes the identity matrix of appropriate dimensions. The inversion of the reference signal cross spectral density matrix can also be ill-conditioned if, for example, the reference sensors are located in very close proximity to each other. A different regularisation factor $\beta_{\unit{x}} \I$ could be used in the inversion of this matrix too. The result with both regularisation factors included would be

\begin{equation} \label{eq:LSANCRegSol}
	\M{W}_{\unit{opt}} = \left( \M{G}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} + \beta_{\unit{\tiny{G}}} \I \right)^{-1} \M{G}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} \left( \M{S}_{\unit{xx}} + \beta_{\unit{x}} \I \right)^{-1}
\end{equation}

\refEqCap{eq:LSANCRegSol} is the optimal unconstrained solution of the feed-forward active control system for tonal disturbances. It can be used to estimate the theoretically optimal performance of such a system and is often the first step in the design or investigation of an active control system.





% ==========================================
% Fx-LMS optimal filter
% ==========================================
\subsection{Filtered-x LMS} \label{sec:ANCFxLMS}

In many practical applications, the filter matrix $\M{M}$ is calculated with the \textit{Filtered-x Lease Mean Squares} (FxLMS) algorithm. This is an adaptive algorithm and the update equation to calculate the optimal secondary signals is

\begin{equation} \label{eq:FxLMS}
	\begin{split}
		\V{u} \left( n + 1 \right) & = \V{u} \left( n \right) + \alpha \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{e} \left( n \right) \\
		& = \V{u} \left( n \right) + \alpha \left[ \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}} \left( n \right) + \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \V{u} \left( n \right) \right].
	\end{split}
\end{equation}

The term $\alpha$ is the learning/convergence/step coefficient and $n$ denotes the current time sample/instance. When the solution has converged, the term in the square brackets is equal to zero and the secondary signals after convergence are

\begin{equation} \label{eq:FxLMSOptU}
	\hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}} + \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \V{u}_{\infty} = \0 \implies \V{u}_{\infty} = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}},
\end{equation}

where the time dependence has been dropped for notational convenience. Expressing the secondary signals after convergence as $\V{u}_{\infty} = \M{W}_{\unit{opt}} \V{x}$, multiplying both sides of \refEq{eq:FxLMSOptU} by $\V{x}^{\unit{H}}$, taking expectation and solving for $\M{W}$ we get

\begin{equation} \label{eq:FxLMSSol}
	\begin{split}
		\V{u}_{\infty} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}} \\
		 \M{W}_{\unit{opt}} \V{x} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}}  \\
		\unit{E} \left[ \M{W}_{\unit{opt}} \V{x} \V{x}^{\unit{H}} \right] & = - \unit{E} \left[ \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{d}_{\unit{e}} \V{x}^{\unit{H}} \right]  \\
		\M{W}_{\unit{opt}} \M{S}_{\unit{xx}} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}}  \\
		\M{W}_{\unit{opt}} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} \M{S}_{\unit{xx}}^{-1}.
	\end{split}
\end{equation}

It can be seen that the optimal filter given by \refEq{eq:FxLMSSol} depends also on the estimated plant response. Like in the optimal control, regularisation can be applied to get

\begin{equation} \label{eq:FxLMSSolRegul}
	\M{W}_{\unit{opt}} = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{e}} + \beta_{\unit{\tiny{G}}} \I \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{S}_{\unit{xe}} \left( \M{S}_{\unit{xx}} + \beta_{\unit{x}} \I \right)^{-1}.
\end{equation}