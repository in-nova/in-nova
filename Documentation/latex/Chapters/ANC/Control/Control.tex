%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Active Noise Control section %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Active Control} \label{sec:ActiveControl}

One of the biggest parts of the IN-NOVA project is the active control of noise and vibration. This section holds the implementations of the control algorithms used in the project. A high-level description of the problem will be given below to facilitate understanding of the formulations presented in the sections describing the implemented methods.

\subsubsection*{Descrpition of the ANC problem}

A generic ANC system aims to reduce noise or vibration by using secondary sources. The disturbance to be controlled is measured with sensors termed \textit{reference} sensors. Based on the signals measured by the reference sensors, a controller is used to generate signals to drive the secondary sources to minimise the disturbance. Under certain circumstances, the reference signals may not correspond exactly to the field that has to be minimised. \textit{Error} sensors are used to measure the minimised signals and can be fed back to the controller to form a feedback loop.

The two major categories the ANC systems are divided into are the \textit{feedforward} and \textit{feedback}. In the former case, the disturbance to be controlled is measured in advance and based on measurements or modelling of the system, the appropriate signals are generated to minimise it at the location of interest. In feedback systems, the quantity to be minimised is the signal at the error microphones which are fed back to the controller to generate the signals that will drive the secondary sources. In certain cases, feedback and feedforward topologies can be combined to form \textit{hybrid} ANC systems in an attempt to combine the positives of both.

The reference and error sensors can measure vibrations or sound and the two can measure different quantities. The formulations usually consider linear systems described by transfer functions in the transfer domain, or impulse responses in the time domain. A block diagram of a generic ANC system is shown in \refFig{fig:ANCBlock}. All lowercase bold letters denote vector quantities and the uppercase bold letters denote matrices. 

\input{\CurrentFilePath/ANCBlock}

The primary source signals $\V{v}$ propagate through the transfer functions $\M{P}_{\unit{x}}$ and $\M{P}_{\unit{e}}$ to reach the reference and error sensors respectively. The term $\M{W}$ denotes the filter or controller that generates the secondary signals $\V{u}$. The secondary signals propagate through the transfer functions $\M{G}_{\unit{s}}$ and $\M{G}_{\unit{e}}$ to reach the reference and error sensors too. These transfer functions are usually termed \textit{"Plant responses"} in the ANC literature. The signal $\V{e}$ is the sum of the primary disturbance and the secondary disturbances as recorded at the error sensors given by

\begin{equation} \label{eq:ANCErr}
	\V{e} = \V{d}_{\unit{e}} + \V{y} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \V{u}
\end{equation}

The secondary source signals $\V{u}$ can contaminate the reference signals and make the system unstable. A technique termed \textit{Internal Model Control} (IMC) can be used where the feedback transfer function is either measured or estimated and subtracted by the reference signals. The matrix $\M{\hat{G}}_{s}$ denotes the estimate of the feedback transfer path and is subtracted by the reference signal.

In the block diagram of \refFig{fig:ANCBlock} removing the reference microphones results in a feedback topology. To acquire a pure feedforward system, one has to discard the feedback path from the error microphones to the filter block and acquire a perfect estimate of the feedback transfer function. In practice, a perfect estimate is very hard to acquire, if even possible.

\input{\CurrentFilePath/TonalControl/TonalControl}