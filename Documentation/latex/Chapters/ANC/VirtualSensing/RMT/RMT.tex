%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Remote microphone technique subsection %%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Remote Microphone Technique} \label{sec:RMT}

The remote microphone technique is a virtual sensing method used in the active noise control context. The method is divided into two stages, the \textit{identification} and the \textit{control}. At the identification stage, the complete control system is augmented with error microphones at the positions where control is needed, which are termed \textit{virtual microphones}. The transfer function between the microphones of the control system, termed \textit{monitoring microphones} and the virtual microphones is measured/estimated. This transfer function is called the \textit{observation filter}.

During the control phase, the observation filter is used to project the sound field measured by the monitoring microphones to the positions of the virtual microphones and control it with the secondary sources. In order for control to be implemented, the plant responses from the secondary sources to the virtual microphones have to be measured/estimated during the identification stage.

\subsubsection{Problem formulation} \label{sec:RMTFormulation}

The formulation described here will follow that of \cite{ModelingLocalANC}, where the disturbances are regarded as realisations of WSS processes and are characterised by their cross spectral densities. \refFigCap{fig:RMTBlock} shows the block diagram of a purely feed-forward active control system with the part related to the remote microphone technique being marked with the red dashed line. The generation of the disturbances is similar to that presented in \refSec{sec:TonalANCFormulation}. The difference here is the separation of the error microphones, which in the RMT case correspond to the virtual microphones, from the monitoring microphones which are the physical microphones used for the estimation of the sound field at the virtual positions. Referencing the block diagram of \refFig{fig:RMTBlock} and following the derivation of \refSec{sec:TonalANCFormulation}, the primary disturbances at the reference, monitoring and virtual sensors are given by

\begin{subequations}
	\begin{align}
		\V{x} & = \M{P}_{\!\unit{x}} \V{v} \label{eq:PrimaryRefSigVec}\\
		\V{d}_{\unit{m}} & = \M{P}_{\!\unit{m}} \V{v} \label{eq:PrimaryMonSigVec}\\
		\V{d}_{\unit{e}} & = \M{P}_{\!\unit{e}} \V{v}. \label{eq:PrimaryErrSigVec}
	\end{align}
\end{subequations}

\input{\CurrentFilePath/RMTBlock}

Like in the "conventional" ANC, the reference signals are filtered by the filter matrix $\M{W}$ and drive the secondary sources with signals $\V{u}$ that contribute to the sound field in the monitor and error microphone positions giving

\begin{subequations}
	\begin{align}
		\V{m} = \M{P}_{\unit{m}} \V{v} + \M{G}_{\unit{m}} \V{u} \label{eq:TotMonSigs}\\
		\V{e} = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \V{u} . \label{eq:TotErrSigs}
	\end{align}
\end{subequations}

The true plant responses, $\M{G}_{\unit{m}}$ and $\M{G}_{\unit{e}}$ are often unavailable. In real applications, their estimates $\hat{\M{G}}_{\unit{m}}$ and $\hat{\M{G}}_{\unit{e}}$ are calculated during the identification stage and are used instead \cite{RobustPerformance}. The disturbance due to the primary sources at the location of the monitoring microphones is then estimated as

\begin{equation} \label{eq:dmEst}
	\hat{\V{d}}_{\unit{m}} = \V{m} - \hat{\M{G}}_{\unit{m}} \V{u} = \V{d}_{\unit{m}} + \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \V{u} .
\end{equation}

If the plant response estimate $\hat{\M{G}}_{\unit{m}}$, is a perfect representation of the true response $\M{G}_{\unit{m}}$ the estimated primary source field at the monitoring positions is equal to its true value. Applying the observation $\hat{\M{O}}$ filter to the estimated primary disturbance at the monitoring microphones provides the estimate of the primary field at the virtual position. The estimated field is given by

\begin{equation} \label{eq:deEst}
	\hat{\M{d}}_{\unit{e}} = \hat{\M{O}} \hat{\V{d}}_{\unit{m}} = \hat{\M{O}} \V{d}_{\unit{m}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \V{u}.
\end{equation}

If the plant responses to the monitoring microphones are perfectly estimated, the estimate of the primary field at the virtual microphone is dependent on the observation filter $\hat{\M{O}}$. During control, it is the estimated disturbance at the remote location that will be minimised. The estimated residual signal is given by

\begin{equation} \label{eq:errEst}
	\begin{split}
		\hat{\V{e}} & = \hat{\V{d}}_{\unit{e}} + \hat{\M{G}}_{\unit{e}} \V{u} \\
		& = \hat{\M{O}} \V{d}_{\unit{m}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \V{u} + \hat{\M{G}}_{\unit{e}} \V{u} \\
		& = \hat{\M{O}} \V{d}_{\unit{m}} + \left[ \hat{\M{G}}_{\unit{e}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \right] \V{u}.
	\end{split}
\end{equation}

The estimated plant response $\hat{\M{G}}_{e}$ has been used since the true value is not available for the estimation. The difference between the true and estimated error at the virtual microphone positions is given by

\begin{equation} \label{eq:errDiff}
	\begin{split}
		\V{e} - \hat{\V{e}} & = \V{d}_{\unit{e}} + \M{G}_{\unit{e}} \V{u} - \hat{\V{d}}_{\unit{e}} - \hat{\M{G}}_{\unit{e}} \V{u} \\
		& = \M{P}_{\unit{e}} \V{v} + \M{G}_{\unit{e}} \V{u} - \hat{\M{O}} \M{P}_{\unit{m}} \V{v} - \left[ \hat{\M{G}}_{\unit{e}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \right] \V{u} \\
		& = \left( \M{P}_{\unit{e}} - \hat{\M{O}} \M{P}_{m} \right) \V{v} + \left[ \M{G}_{\unit{e}} - \hat{\M{G}}_{\unit{e}} - \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \right] \V{u}\\
		& = \left\{ \left( \M{P}_{\unit{e}} - \hat{\M{O}} \M{P}_{m} \right) + \left[ \M{G}_{\unit{e}} - \hat{\M{G}}_{\unit{e}} - \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \right] \M{W} \M{P}_{\unit{x}} \right\} \V{v},
	\end{split}
\end{equation}

where \textit{equations} \eqref{eq:PrimaryMonSigVec} and \eqref{eq:PrimaryErrSigVec} have been used to express $\V{d}_{\unit{m}}$ and $\V{d}_{\unit{e}}$.


\subsubsection*{Observation filters}

If the plant response estimates to the monitoring and virtual microphones are perfect, the effect of the secondary sources on the error difference is zero, denoting perfect control of the estimated primary field. In this case, the error residual is entirely dependent on the ability to estimate the primary disturbance at the error microphones with the observation filter $\hat{\M{O}}$. The same is true in the absence of control, in which case the task is just to estimate the sound field at the virtual microphones. The estimation error in this case is given by 

\begin{equation} \label{eq:sndFieldEstErr}
	\V{d}_{\unit{e}} - \hat{\V{d}}_{\unit{e}} =  \V{d}_{\unit{e}} - \hat{\M{O}} \V{d}_{\unit{m}} .
\end{equation}

The calculation of the observation filter can be formulated as an optimisation problem with the least mean square estimate being given by minimising the cost function \cite{DSPforANC}

\begin{equation} \label{eq:LSObsFiltCostFunction}
	\begin{split}
		J_{\unit{2}} & =  \tr \left\{ \unit{E} \! \left[ \left( \V{d}_{\unit{e}} - \M{O} \V{d}_{\unit{m}} \right) \left( \V{d}_{\unit{e}} - \M{O} \V{d}_{\unit{m}} \right)^{\unit{H}} + \beta_{\unit{\tiny{O}}} \M{O} \M{O}^{\unit{H}} \right] \right\} \\
		& = \tr \left\{ \unit{E} \! \left[ \V{d}_{\unit{e}} \V{d}_{\unit{e}}^{\unit{H}} - \V{d}_{\unit{e}} \V{d}_{\unit{m}}^{\unit{H}} \M{O}^{\unit{H}} - \M{O} \V{d}_{\unit{m}} \V{d}_{\unit{e}}^{\unit{H}} + \M{O} \V{d}_{\unit{m}} \V{d}_{\unit{m}}^{\unit{H}} \M{O}^{\unit{H}} + \beta_{\unit{\tiny{O}}} \M{O} \M{O}^{\unit{H}} \right] \right\} \\
		& = \tr \left\{ \M{S}_{\unit{ee}} - \M{S}_{\unit{me}} \M{O}^{\unit{H}} - \M{O} \M{S}_{\unit{me}}^{\unit{H}} + \M{O} \M{S}_{\unit{mm}} \M{O}^{\unit{H}} + \beta_{\unit{\tiny{O}}} \M{O} \M{O}^{\unit{H}} \right\}.
	\end{split}
\end{equation}

The cross spectral density matrices are given by

\begin{subequations}
	\begin{align}
		\M{S}_{\unit{mm}} & = \unit{E} \! \left[ \V{d}_{\unit{m}} \V{d}_{\unit{m}}^{\unit{H}} \right] = \unit{E} \! \left[ \M{P}_{\unit{m}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{m}}^{\unit{H}} \right] = \M{P}_{\unit{m}} \M{S}_{\unit{vv}} \M{P}_{\unit{m}}^{\unit{H}} \label{eq:Smm}\\
		\M{S}_{\unit{ee}} & = \unit{E} \! \left[ \V{d}_{\unit{e}} \V{d}_{\unit{e}}^{\unit{H}} \right] = \unit{E} \! \left[ \M{P}_{\unit{e}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{e}}^{\unit{H}} \right] = \M{P}_{\unit{e}} \M{S}_{\unit{vv}} \M{P}_{\unit{e}}^{\unit{H}} \label{eq:See}\\
		\M{S}_{\unit{me}} & = \unit{E} \! \left[ \V{d}_{\unit{e}} \V{d}_{\unit{m}}^{\unit{H}} \right] = \unit{E} \! \left[ \M{P}_{\unit{e}} \V{v} \V{v}^{\unit{H}} \M{P}_{\unit{m}}^{\unit{H}} \right] = \M{P}_{\unit{e}} \M{S}_{\unit{vv}} \M{P}_{\unit{m}}^{\unit{H}} \label{eq:Sme},
	\end{align}
	\label{eq:ObsFiltCSD}
\end{subequations}

where like in \refSec{sec:TonalANCFormulation}, $\M{S}_{\unit{vv}} = \unit{E} \!\left[ \V{v} \V{v}^{\unit{H}} \right]$ is the cross spectral density matrix of the source signals. The term $\beta_{\unit{\tiny{O}}} \M{O} \M{O}^{\unit{H}}$ is a regularisation matrix with $\beta_{\unit{\tiny{O}}}$ being a scalar regularisation factor \cite{DSPforANC}. The regularisation term applies a penalty to the solution of \refEq{eq:LSObsFiltCostFunction} reducing the maximum value of the observation filter. For uncorrelated primary sources with common source strength $v$ the spectral density matrix of the source signals is $\M{S}_{\unit{vv}} = v \I$. \textit{Equations} \eqref{eq:ObsFiltCSD} can be used to model correlated sources by introducing values off the main diagonal of the $\M{S}_{\unit{vv}}$ matrix. Following a similar approach to that used in \refSec{sec:TonalANCFormulation} to derive the optimal filter, the solution to \refEq{eq:LSObsFiltCostFunction} can be shown to be \cite{DSPforANC, ModelingLocalANC}

\begin{equation} \label{eq:LSObsFiltSol}
	\hat{\M{O}}_{\unit{opt}} = \M{S}_{\unit{me}} \left( \M{S}_{\unit{mm}} + \beta_{\unit{\tiny{O}}} \I \right)^{-1} = \M{P}_{\unit{e}} \M{S}_{\unit{vv}} \M{P}_{\unit{m}}^{\unit{H}} \left( \M{P}_{\unit{m}} \M{S}_{\unit{vv}} \M{P}_{\unit{m}}^{\unit{H}} + \beta_{\unit{\tiny{O}}} \I \right)^{-1}.
\end{equation}

Where \textit{equations} \eqref{eq:ObsFiltCSD} were used to expand \refEq{eq:LSObsFiltSol}. The optimal observation filter can be used in \refEq{eq:errDiff} to calculate the reduction at the virtual microphone, assuming the secondary signals $\V{u}$, or the filters $\M{W}$ are known.



\subsubsection*{Control}

The optimal control filter $\M{W}_{\unit{opt}}$ can be calculated following the same procedure as that followed in \refSec{sec:TonalANCFormulation} or above to calculate the optimal observation filter. Starting from \refEq{eq:errEst} we write

\begin{equation} \label{eq:errEst}
	\begin{split}
		\hat{\V{e}} & = \hat{\V{d}}_{\unit{e}} + \hat{\M{G}}_{\unit{e}} \V{u} \\
		& = \hat{\M{O}} \V{d}_{\unit{m}} + \left[ \hat{\M{G}}_{\unit{e}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right) \right] \M{W} \V{x} \\
		& = \hat{\M{O}} \V{d}_{\unit{m}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \V{x},
	\end{split}
\end{equation}

where $\M{G}_{\unit{\tiny{RM}}}$, which is the effective plant response between the secondary sources and the virtual microphones, is given by

\begin{equation} \label{eq:GRM}
	\M{G}_{\unit{\tiny{RM}}} = \hat{\M{G}}_{\unit{e}} + \hat{\M{O}} \left( \M{G}_{\unit{m}} - \hat{\M{G}}_{\unit{m}} \right).
\end{equation}

Using again a least squares approach, we formulate the cost function for the calculation of the optimal control filter as

\begin{equation} \label{eq:LSANCRMTFunc}
	\begin{split}
		J_{2} & = \tr \left\{ \unit{E} \left[ \hat{\V{e}} \hat{\V{e}}^{\unit{H}} \right] \right\} \\
		& = \tr \left\{ \unit{E} \left[ \left( \hat{\M{O}} \V{d}_{\unit{m}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \V{x} \right) \left( \hat{\M{O}} \V{d}_{\unit{m}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \V{x} \right)^{\unit{H}} \right] \right\} = \\
		& = \tr \left\{ \hat{\M{O}} \M{S}_{\unit{mm}} \hat{\M{O}}^{\unit{H}} + \hat{\M{O}} \M{S}_{\unit{xm}} \M{W}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xm}}^{\unit{H}} \hat{\M{O}}^{\unit{H}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xx}} \M{W}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \right\}
	\end{split}
\end{equation}

The derivative of \refEq{eq:LSANCRMTFunc} with respect to $\M{W}$ is

\begin{equation} \label{eq:LSANCRMTDerivative}
	\begin{split}
		\frac{\partial J_{2}}{\partial \M{W}} & = \frac{\partial }{\partial \M{W}} \tr \left\{ \hat{\M{O}} \M{S}_{\unit{m m}} \hat{\M{O}}^{\unit{H}} + \hat{\M{O}} \M{S}_{\unit{xm}} \M{W}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xm}}^{\unit{H}} \hat{\M{O}}^{\unit{H}} + \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xx}} \M{W}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \right\} \\
		& = 2 \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \hat{\M{O}} \M{S}_{\unit{xm}} + 2 \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xx}}.
	\end{split}
\end{equation}

So, by setting the derivative to zero and solving for $\M{W}$ we get

\begin{equation} \label{eq:LSANCRMTSol}
	\begin{split}
		\frac{\partial J_{2}}{\partial \M{W}} & = \0 \implies 2 \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \hat{\M{O}} \M{S}_{\unit{xm}} + 2 \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \M{W} \M{S}_{\unit{xx}}^{\unit{H}} = \0 \implies \\
		\implies \M{W}_{\unit{opt}} & = \left( \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \hat{\M{O}} \M{S}_{\unit{xm}} \M{S}_{\unit{xx}}^{-1}
	\end{split}
\end{equation}

Similar to the ANC with no virtual sensing, regularisation can be used for the inversion of both $\left( \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)$ and $\M{S}_{\unit{xx}}$ to get

\begin{equation} \label{eq:ANCRMTLeastSquaresRegSolution}
	\M{W}_{\unit{opt}} = \left( \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} + \beta_{\unit{\tiny{G}}} \I \right)^{-1} \M{G}_{\unit{\tiny{RM}}}^{\unit{H}} \hat{\M{O}} \M{S}_{\unit{xm}} \left( \M{S}_{\unit{xx}} + \beta_{\unit{x}} \I \right)^{-1}
\end{equation}


\subsubsection*{FxLMS solution}

As mentioned in \refSec{sec:ANCFxLMS}, in most practical applications the implementation of the ANC system is based on the FxLMS algorithm. Following the same reasoning, starting from \refEq{eq:FxLMS} and plugging in \refEq{eq:errEst} for the error, we can write for the RMT system

\begin{equation} \label{eq:FxLMSRMT}
	\begin{split}
		\V{u} \left( n + 1 \right) & = \V{u} \left( n \right) + \alpha \hat{\M{G}}_{\unit{e}}^{\unit{H}} \V{e} \left( n \right) \\
		& = \V{u} \left( n \right) + \alpha \left[ \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}} \left( n \right) + \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \V{u} \left( n \right) \right].
	\end{split}
\end{equation}

After convergence, the term in the square brackets equals zero and from this, the optimal secondary signals are given as

\begin{equation} \label{eq:FxLMSRMTOptU}
	\hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}} \left( n \right) + \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \V{u}_{\infty} = \0 \implies \V{u}_{\infty} = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}},
\end{equation}

where again, the dependence on time has been dropped for notational convenience. Expressing the secondary signals after convergence as $\V{u}_{\infty} = \M{W}_{\unit{opt}} \V{x}$, multiplying both sides of \refEq{eq:FxLMSRMTOptU} by $\V{x}^{\unit{H}}$, taking expectation and solving for $\M{W}$ we get

\begin{equation} \label{eq:FxLMSSol}
	\begin{split}
		\V{u}_{\infty} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}} \\
		\M{W}_{\unit{opt}} \V{x} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}} \\
		\unit{E} \left[ \M{W}_{\unit{opt}} \V{x} \V{x}^{\unit{H}} \right] & = - \unit{E} \left[ \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \V{d}_{\unit{m}} \V{x}^{\unit{H}} \right] \\
		\M{W}_{\unit{opt}} \M{S}_{\unit{xx}} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \M{S}_{\unit{xm}} \\
		\M{W}_{\unit{opt}} & = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \M{S}_{\unit{xm}} \M{S}_{\unit{xx}}^{-1}.
	\end{split}
\end{equation}

Regularisation can be used to get

\begin{equation} \label{eq:FxLMSSolRegul}
	\M{W}_{\unit{opt}} = - \left( \hat{\M{G}}_{\unit{e}}^{\unit{H}} \M{G}_{\unit{\tiny{RM}}} + \beta_{\unit{\tiny{G}}} \I \right)^{-1} \hat{\M{G}}_{\unit{e}}^{\unit{H}} \hat{\M{O}}_{\unit{opt}} \M{S}_{\unit{xm}} \left( \M{S}_{\unit{xx}} + \beta_{\unit{x}} \I \right)^{-1}.
\end{equation}


\subsubsection{MATLAB\textsuperscript{\texttrademark} implementation}

\subsubsection*{Observation filters}

The calculation of the observation filters is performed in the frequency domain for tonal excitations and is based on the formulation provided in \refSec{sec:RMTFormulation} above. The calculation is performed with a single function, \texttt{obsFilt} and returns the optimal filters as well as other parameters that may be useful for simulations and development.

\subsubsection*{Interface}

The function header for the calculation of the optimal observation filters is:

\begin{lstlisting}[caption = \texttt{obsFilt} MATLAB\textsuperscript{\texttrademark} function header., style = matlab]
function [oOpt, Sme, Smm, est, errSqr, normErrSqr, See, condNum] = obsFilt(Pe, Pm, srcCsd, regFacs)
\end{lstlisting}

The arguments of the function are:

\begin{itemize}
	\item \textbf{Input}
	\begin{itemize}
		\item \texttt{Pe}: The transfer function from sources to the virtual microphones. The dimensions of the matrix must be $N \times M$, where $N$ is the number of virtual microphones and $M$ the number of sources.
		\item \texttt{Pm}: The transfer function from sources to the monitoring microphones. The dimensions of the matrix must be $K \times M$, where $K$ is the number of monitoring microphones and $M$ is the number of sources.
		\item \texttt{srcCsd} (optional): The source cross spectral density matrix. This must be a square ($M \times M$) symmetric matrix with the cross power spectral density of the sources. \\Default value: \texttt{eye(M)}.
		\item \texttt{regFacs} (optional): The regularisation factors used for the inversion of the cross-spectra of the monitoring microphones. Can be a vector with number of elements equal to $K$ (number of monitoring microphones) or a scalar which will result in the same regularisation factor for all microphones. \\Default value: $0$.
	\end{itemize}
	\item \textbf{Output}
	\begin{itemize}
		\item \texttt{oOpt}: The optimal observation filters for each virtual microphone position. The dimensions of the matrix are $N \times M$, where $N$ is the number of virtual positions and $M$ the number of monitoring microphones.
		\item \texttt{Sme}: The cross-spectra between monitoring and virtual microphones. The matrix has dimensions $N \times K$ where $N$ is the number of virtual microphones and $M$ the number of monitoring microphones.
		\item \texttt{Smm}: The cross-spectra between monitoring microphones. The matrix has dimensions $K \times K$, where $K$ is the number of monitoring microphones.
		\item \texttt{est}: The estimated complex pressure(s) at the virtual microphone position(s).
		\item \texttt{err}: The error of the estimated complex pressure(s).
		\item \texttt{errSqr}: The sum of the squared errors of the estimated complex pressure(s).
		\item \texttt{normErrSqr}: The sum of the squared errors of the estimated complex pressure(s) normalised to the squared pressure(s) of the virtual microphones.
		\item \texttt{See}: The normalisation factor (the cross spectral density of the error microphones) for each virtual microphone position. \texttt{errSqr./See} gives the \texttt{normErrSqr} values.
		\item \texttt{condNum}: The inversion condition number of the regularised monitoring microphone cross spectrum matrix.
	\end{itemize}
\end{itemize}


\subsubsection*{Examples}