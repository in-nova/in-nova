%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% isEven function %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\texttt{isEven}} \label{sec:isEven}

The \texttt{isEven} function returns a \texttt{logical} value declaring whether its input is an even number or not. It supports arbitrarily sized arrays and performs the check element-wise. The returned value has the same dimensions as the input with each element in the output declaring whether each element in the input is an even number. Currently, only real numbers are supported and \texttt{Inf} and \texttt{NaN} values always return \texttt{false}.

The function's header is:

\begin{lstlisting}[caption = \texttt{isEven} MATLAB\textsuperscript{\texttrademark} function header., style = matlab]
result = isEven(num)
\end{lstlisting}

The arguments of the function are:

\begin{itemize}
	\item \textbf{Input}
	\begin{itemize}
		\item \texttt{num}: This argument must be \texttt{numeric}\footnote{The function \texttt{isnumeric()} must return \texttt{true} when \texttt{num} is passed as its argument.} and real but can have arbitrary dimensions. The check is performed element-wise.
	\end{itemize}
	\item \textbf{Output}
	\begin{itemize}
		\item \texttt{result}: The result of the checks. The type of this argument is \texttt{logical} and has the same dimensions of \texttt{num}.
	\end{itemize}
\end{itemize}

Although it is very easy to perform checks in your code to decide whether values are even or odd, it is good practice to use functions to perform even easy tasks. Functions can be maintained better than snippets of code sparsely spread in your codebase. Furthermore, functions can be enriched with features to accommodate needs that may arise in the future.

\subsubsection*{Dependencies}

The latest MATLAB\textsuperscript{\texttrademark} built-in function used requires MATLAB\textsuperscript{\texttrademark} R2011b, which is the earliest version the function can be used with, but there are no other dependencies.



\subsubsection*{Examples}

The first example shown below uses the function to decide whether the right-sided spectrum contains the \textit{Nyquist} frequency. If the length of the double-sided spectrum is even it is assumed that the \textit{Nyquist} frequency is included in the positive frequencies. This assumption could hold, considering that the \textit{DC} and \textit{Nyquist} frequencies are not mirrored in the spectrum. Removing the $0 \, \unit{Hz}$ from the frequency vector, its length is an odd number suggesting that one of the bins cannot be mirrored, which must be the \textit{Nyquist} frequency.

The check is performed to generate the double-sided spectrum from the single-sided one. Knowing whether the \textit{Nyquist} frequency is included gives information on the bins that have to be mirrored. The variable \texttt{oneSideSpec} is assumed to be a vector of complex numbers (although the type of the numbers does not matter).

\begin{lstlisting}[caption = \texttt{isEven} fist example. Finding out whether the \textit{Nyquist} frequency is present in a single-sided spectrum, style = matlab]
% Find out whether Nyquist is included
if isEven(length(oneSideSpec))
	% Nyquist is included, don't mirror it
	dblSideSpec = [oneSideSpec, conj(flip(oneSideSpec(2:end - 1))];
else
	% Nyquist not included, mirror everything but DC
	dblSideSpec = [oneSideSpec, conj(flip(oneSideSpec(2:end))];
end
\end{lstlisting}


The second example illustrates the use of the function with multi-dimensional arrays. From a two-dimensional numerical array (matrix), the even values are chosen and their largest number is used to seed a random number generator. This example does not provide a solution to a specific problem but effectively illustrates the use of the function where it is applied element-wise. The variable \texttt{matVar} is assumed to be the matrix that holds numerical values.

\begin{lstlisting}[caption = \texttt{isEven} second example. Pick the largest even value from a two-dimensional matrix and use it to seed a random generator., style = matlab]
rng(max(matVal(isEven(matVal)), [], "all"));
\end{lstlisting}