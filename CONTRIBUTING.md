## Contribute
If you would like to contribute to the project you could do that in various ways. Some examples are listed below:

- Create issues for bugs, improvements and/or features.
- Fix a bug or develop an improvement and/or feature yourself. Clone the repository, create a new branch and when done create a merge request.
- Report typos and/or inconsistencies in the documentation.
- Contribute to the documentation and/or the Wiki of the project.
- Create a CI configuration for automated testing, or develop unit tests.
- Port the codebase to other programming languages and/or environments.


## Remarks

Since there is no CI set at the moment to perform automated testing, you are highly encouraged to test your contributions as much as possible and even contribute some unit tests if this aligns with your contributed content.

To try and maintain a coherent style in the codebase you are encouraged to use the template available in the [Templates]() folder, although this is not necessary.  
Irrespective of style, it is highly advisable to name your variables in a meaningful manner and keep them as short as possible while still communicating their purpose. Comments are highly recommended in your code, especially where they can facilitate a better understanding of the code and the intent of the developer. Over-commented code is better than under-commented code.

## ***IMPORTANT***
The project is licensed under the [**MIT License**](https://mit-license.org/). If you are unable or unwilling to contribute code under this license do **not** add any code to the repository. The content of this project must remain under the same license.

