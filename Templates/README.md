# Templates

This section holds templates for functions and other code structures that have been implemented in the codebase.

It is considered good practice to use these templates as a starting point for your implementations to maintain a consistent style throughout the project.

Updates and changes to the templates are welcome when justified to fulfill a purpose that will make the code easier to maintain, read and even more efficient.