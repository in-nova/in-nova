%% Descriptive title of the function
% --------------------------------------------------
% Author:
% e-mail:
%
% Date: --/--/---- (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Briefly describe the functionality
%                of the function here.
% --------------------------------------------------
% Input
% 
% inParam_1_Name [paramType]: Parameter description goes here. Don't
%.                            be afraid to put as much information is
%                             needed to make sure the user can use the
%                             function properly.
% 
% inParam_2_Name [paramType]: The same goes here too. Provide enough
%                             information to the used and make reference
%                             to other input or output arguments. Usually
%                             they are placed in quotes like: This
%                             parameter must have the same number of rows
%                             as "inputParam_1_Name".
%                                
% 
% inParam_3_Name [paramType] (Optional): This is an optional parameter. You
%                                        have to explicitly declare it here
%                                        so that users know they can omit it.
%                                        Furthermore, it's good practice to
%                                        put optional parameters after the
%                                        mandatory ones, although this is not
%                                        a necessity.
%                                            
% --------------------------------------------------
% Output
% 
% outParam_1_Name [paramType]: The same "rules" apply to the output variables.
%                              Again, don't be "afraid" to provide as much
%                              information is needed for the user to be able to
%                              use correctly the returned values.
% 
% outParam_2_Name [paramType]: Here I provide information on the way data are
%                              structured in this returned array. This parameter
%                              has dimensions N*KxM where the N corresponds to
%                              number of offsprings, K is the number of parents
%                              of each offspring and M the number of variables
%                              for each individual. The data are arranged as:
%                              [Par_1_Off_1_Var_1, Par_1_Off_1_Var_2, ...;
%                               Par_2_Off_1_Var_1, Par_2_Off_1_Var_2, ...;
%                                       .        ,         .        , ...;
%                                       .        ,         .        , ...;
%                                       .        ,         .        , ...;
%                               Par_K_Off_1_Var_1, Par_2_Off_1_Var_2, ...;
%                               Par_1_Off_2_Var_1, Par_2_Off_1_Var_2, ...;
%                               Par_2_Off_2_Var_1, Par_2_Off_1_Var_2, ...;
%                                       .        ,         .        , ...;
%                                       .        ,         .        , ...;
%                                       .        ,         .        , ...;
%                               Par_K_Off_N_Var_1, Par_2_Off_1_Var_2, ...]
% 
% --------------------------------------------------
% Notes
% 
% Dependencies: - State the dependencies here so that the user can get them
%                 and use the function. In this example the function requires
%                 the "pickValProb" function from the MATLAB Utilities project.
%
% - If "inParam_3_Name" is used, the values of "inParam_2_Name" will be normalised
%   to sum up to unity (this is a note example).
% --------------------------------------------------
function [outParam_1_Name, outParam_2_Name] = funcName(inParam_1_Name, inParam_2_Name, inParam_3_Name)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(1, 2);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(inParam_1_Name, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Sting variable example", 1);
    validatestring(inParam_1_Name, ["IN", "NOVA", "Project", "Rulez"], mfilename, "String variable example", 1);

    if strcmpi(selTypeinParam_1_Name, "Rulez")
        if ~isnumeric(inParam_2_Name)
            error("funcName(): inParam_2_Name must be a positive value corresponding the awesomeness of the IN-NOVA project when inParam_1_Name is Rulez.");
        else
            if inParam_2_Name <= 0
                error("funcName(): You must be mistaken somehow... IN-NOVA is more than 0 awesome. Please reconsider and call the function again!");
            end
        end
    elseif strcmpi(inParam_1_Name, "Project") && ~isnumeric(inParam_2_Name)
        error("funcName(): When you say Project you must also provide the project's ID.");
    end

    validateattributes(inParam_2_Name, "numeric", {'nonempty', 'finite', 'nonnan', 'positive', 'real', 'integer'}, mfilename, "inParam_2_Name", 2);
    
    % Validate and initialise optional parameters
    if nargin > 2
        validateattributes(inParam_3_Name, "numeric", {'nonnan', '>=', 2, 'finite', 'real', 'integer'}, mfilename, "inParam_3_Name", 3);
    else
        inParam_3_Name = 1.25e2;
    end


    % ====================================================
    % Calculate needed variables - Pre-processing
    % ====================================================

    
    % ====================================================
    % Do your work
    % ====================================================

    % ====================================================
    % Post-processing
    % ====================================================
    
    % ====================================================
    % Return values
    % ====================================================
    outParam_1_Name = "The best output value EVER...!!!";
    
    if nargout > 1
        outParam_2_Name = inParam_3_Name - 13;
    else
        outParam_2_Name = [];
    end
end
