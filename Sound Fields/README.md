# Soundfields

This section is about code related to the generation of sound fields (or wavefields in general). In this same section, sound field analysis or reconstruction algorithms could be found with the exception of the *Array Processing* field which is accommodated under the *Signal Processing* section.

## Current structure

The current structure of the section is shown below.

- Generation
  - Point source
    - Frequency domain
    - Time domain
    - Wave domain (spherical harmonics)
  - Plane wave
    - Frequency domain
    - Wave domain (spherical harmonics)
- Wave domain
  - Spherical harmonics
  - Circular harmonics
- Analysis
  - Discrete Spherical Fourier Transform (DSFT)
    - Forward
      - Direct
      - Least-Squares
    - Inverse
  - Discrete Circular Fourier Transform (DCFT)
    - Forward
      - Direct
      - Least-Squares
    - Inverse
- Extrapolation
  - Spherical harmonics

## Dependencies
Some implementations use dependencies from [Utilities/Generic](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities/Generic?ref_type=heads). The necessary files, as well as the relevant functions are listed below:

- MATLAB
  - `ptSrcField()` requires `twoPtDist()` from [Utilities/Generic](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities/Generic?ref_type=heads).
  - `planeWaveSH()` requires `sphHarm()` and `idsft()` from [Sound Fields](https://gitlab.com/in-nova/in-nova/-/tree/main/Sound%20Fields) (this folder).
  - `ptSrcFieldTD()` requires `twoPtDist()` from from [Utilities/Generic](https://gitlab.com/in-nova/in-nova/-/tree/main/Utilities/Generic?ref_type=heads).
