%% Perform a Discrete Circrular Fourier Transform
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 03/06/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate a Discrete Circular Fourier Transform.
% --------------------------------------------------
% Input
% 
% sigMtx [numeric]: The measured signal(s). If this is a matrix, the
%                   tranform is performed for each column and the results
%                   lie in each column of the output argument
%                   "circFtCoeffs".
% 
% circHarms [numeric]: These are the circular harmonics corresponding to
%                      the spatial signal points (angles/directions).
% 
% method [char/string] (Optional): This parameter sets the method of
%                                  calculation. The available values are
%                                  "Direct" and "Least
%                                  Squares"/"Least-Squares"/"LS" and they
%                                  are NOT case-sensitive.
%                                  [Default: "Direct"].
% 
% --------------------------------------------------
% Output
% 
% circFtCoeffs [numeric]: The Discrete Circular Fourier Transform
%                         coefficients corresponding to the signal vector
%                         as calculated by the Circular Harmonics expansion
%                         provided in "circHarms" argument.
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function circFtCoeffs = dcft(sig, circHarms, method)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(sig, "numeric", {'2d', 'finite', 'nonnan', 'nonempty'}, mfilename, "Wave-field samples", 1);
    validateattributes(circHarms, "numeric", {'2d', 'finite', 'nonnan', 'nonempty', 'nrows', size(sig, 1)}, mfilename, "Circular harmonics for the directions of the spatial wave-field samples", 2);

    % Validate optional arguments
    if nargin > 2 && ~isempty(method)
        validateattributes(method, {'char', 'string'}, {'scalartext'}, mfilename, "The method to be used for the calculation of the transform", 3);
        validatestring(method, ["Direct", "LS", "Least Squares", "Least-Squares"], mfilename, "The method to be used for the calculation of the transform", 3);
    else
        method = "Direct";
    end

    % =============================================
    % Calculate the coefficients
    % =============================================
    if strcmpi(method, "Direct")
        circFtCoeffs = circHarms' * sig;
    else
        circFtCoeffs = pinv(circHarms) * sig;
    end
end