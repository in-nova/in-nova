%% Get the circular harmonics up to a given order
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 29/09/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the circular harmonics of given directions up to
%                a given order. Both complex and real harmonics are
%                provided.
% --------------------------------------------------
% Input
% 
% dir [numeric]: The directions for which the spherical harmonics will be
%                calculated. This  must be an 2xM matrix, where M is the
%                number of directions and 2 stands for the azimuth and
%                inclination. The angles must be given in radians. An
%                example of 2 directions would look like:
%                [az1, az2; incl1, incl2].
% 
% N [numeric]: The highest order of the spherical harmonics to be
%              calculated.
% 
% kr [numeric] (Optional): This is a scalar value corresponding to the
%                          normalised wavenumber of interest. If this
%                          argument is not provided or left empty, the
%                          resulting circular harmonics will NOT be scaled
%                          with the corresponding Bessel functions. The
%                          Fourier coefficients can be scaled in
%                          post-processing by multiplying each with
%                          besselj(n, kr * sin(theta)), where "theta"
%                          corresponds to the inclination value for each
%                          direction in "dirs".
% 
% 
% --------------------------------------------------
% Output
% 
% circHarms [numeric]: The circular harmonics up to order "N" for the
%                      directions "dir". This is an MxK matrix with M
%                      denoting the directions and K the circular
%                      harmonics. The orders are arranged from smallest to
%                      highest. An example of a row (one direction) for up
%                      to order 4 is:
%                      [F-4, F-3, F-2, F-1, F0, F1, F2, F3, F4].
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function circHarms = circHarm(dirs, N, kr)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(dirs, "numeric", {'2d', 'nrows', 2, 'finite', 'nonnan', 'nonempty', 'real'}, mfilename, "Directions", 1);
    validateattributes(N, "numeric", {'scalar', 'finite', 'nonnegative', 'nonnan', 'nonempty', 'real'}, mfilename, "Highest circular harmonics order", 2);

    % Validate optional arguments
    if nargin > 2 && ~isempty(kr)
        validateattributes(kr, "numeric", {'scalar', 'real', 'nonnan', 'nonempty', 'finite', 'nonnegative'}, mfilename, "Normalised wavelength (kr)", 3);
    else
        kr = [];
    end

    % =============================================
    % Calculate the spherical harmonics
    % =============================================
    % Calculate parameters
    n = -N:N; % Orders

    % Circular harmonics
    circHarms = 1i.^(n) .* exp(-1i * n .* dirs(1, :).');

    % "Scale"
    if ~isempty(kr)
        circHarms = circHarms .* besselj(n .* ones(size(dirs, 2), 1), kr * sin(dirs(2, :).') .* ones(1, length(n)));
    end
end