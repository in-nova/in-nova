%% Perform an Inverse Discrete Circular Fourier Transform
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 01/06/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate an Inverser Discrete Circular Fourier Transform.
% --------------------------------------------------
% Input
% 
% circFtCoeffs [numeric]: This is a vector with the Discrete Circular
%                         Fourier Transform coefficients of the spatially
%                         sampled wave-field. This must be an NxM matrix
%                         with N denoting the number of Spherical Fourier
%                         coefficients. The transform is calculated for
%                         each column Mi.
% 
% circHarms [numeric]: The circular harmonics corresponing to the Discrete
%                      Circular Fourier Transform coefficients in
%                      "circFtCoeffs" argument. This must be a matrix of
%                      dimensions KxN where K is the number of spatial
%                      sampling points and N the number of circular
%                      harmonics in the expansion.
% 
% --------------------------------------------------
% Output
% 
% waveFieldSamps [numeric]: The wave field samples for each column of the
%                           input argument "circFtCoeffs". This is a KxM
%                           matrix with K the number of spatial samples and
%                           M the number of columns in "circFtCoeffs"
%                           argument (possibly representing different
%                           circular functions).
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function waveFieldSamps = idcft(circFtCoeffs, circHarms)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(circFtCoeffs, "numeric", {'2d', 'finite', 'nonnan', 'nonempty'}, mfilename, "Discrete Circular Fourier Transform coefficients", 1);
    validateattributes(circHarms, "numeric", {'2d', 'finite', 'nonnan', 'nonempty', 'ncols', size(circFtCoeffs, 1)}, mfilename, "Circular harmonics corresponding to the Discrete Circular Fourier Transform coefficients", 2);


    % =============================================
    % Calculate the sampled signals
    % =============================================
    waveFieldSamps = circHarms * circFtCoeffs;
end