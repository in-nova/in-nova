%% Extrapolate the sound field in the Spherical Harmonics domain
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 05/01/2025 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Extrapolate sound field pressure field coefficients in the
%                Spherical Harmonics domain.
% --------------------------------------------------
% Input
% 
% shCoeffs [numeric]: The spherical harmonics pressure coefficients at the
%                     measurement position. This must be an NxMxK matrix
%                     with N being the number of coefficients per position,
%                     M the number of positions/measurements and K the
%                     number of frequencies of interest.
% 
% r [numeric]: This is a vector with at least two elements that correspond
%              to the radial distance component of the spherical
%              coordinates of each position the sound field needs to be
%              extrapolated to. The first element corresponds to the radial
%              distance (radius) of the measurement array.
% 
% k [numeric]: The wavenumbers of the frequencies of interest. This must be
%              a vector with number of elements equal to K, the frequencies
%              of interest.
% 
% w [numeric] (Optional): Pressure coefficient weights to be applied before
%                         the extrapolation process. This can be used to
%                         effectively suppress or use only specific modes
%                         for the extrapolation process.
%                         [Default: ones(size(shCoeffs, 1), 1)].
% 
% dir [numeric] (Optional): This signifies the "direction" of the sound
%                           field. I.e. whether it is an outgoing or
%                           ingoing sound field. In practice, an outgoing
%                           sound field is one where the radial distance
%                           components of the sources are smaller than the
%                           corresponding components of the measurement and
%                           extrapolated positions. In the opposite case,
%                           where the radial distance components are
%                           greater than those of the measurement and
%                           extrapolated positions, the field is an ingoing
%                           field. The possible values of the argument are
%                           "In", "Ingoing", "Converging", "Out",
%                           "Outgoing", "Diverging" and are NOT
%                           case-sensitive. [Default: "In"].
% 
% --------------------------------------------------
% Output
% 
% extrCoeffs [numeric]: This is an NxMxJxK matrix with the extrapolated
%                       coefficients, where J is the number of
%                       extrapolation positions and equals length(r) - 1.
%                       The coefficients may be Inf if the Bessel (or
%                       Hankel) functions at the measured radius exhibit a
%                       zero (or infinity for Hankel functions).
% 
% extrCoeffsReduced [numeric]: This argument holds the same values as
%                              "extrCoeffs" but with the singular
%                              dimensions being reduced. For example, if
%                              extrapolation at a single position is needed
%                              the array has dimensions NxMxK. If
%                              extrapolating at a single frequency, the
%                              dimensions are NxMxJ and if extrapolating at
%                              a single position and frequency the result
%                              is of dimensions NxM. This is equivalent to
%                              the command squeeze(extrCoeffs).
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [extrCoeffs, extrCoeffsReduced] = extrpFieldSh(shCoeffs, r, k, w, dir)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(3, 5);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(shCoeffs, "numeric", {'2d', 'nonempty', 'nonnan', 'finite'}, mfilename, "The spherical harmonic pressure coefficients at the measurement positions", 1);
    validateattributes(r, "numeric", {'vector', 'nonempty', 'nonnan', 'finite', 'nonnegative'}, mfilename, "The radial distance components of all positions", 2);

    % Check if we have AT LEAST two distances
    if numel(r) < 2
        error("You must provide at least two distances, with the first being the radius of the measurement array");
    end

    validateattributes(k, "numeric", {'vector', 'nonempty', 'nonnan', 'finite', 'nonnegative', 'real'}, mfilename, "The wavenumbers of the frequencies of interest", 3);

    % Validate optional arguments
    if nargin > 3 && ~isempty(w)
        validateattributes(w, "numeric", {'vector', 'nonempty', 'nonnan', 'numel', size(shCoeffs, 1), 'real', 'finite'}, mfilename, "Coefficient weights", 4);
    else
        w = ones(size(shCoeffs, 1), 1);
    end

    if nargin > 4 && ~isempty(dir)
        validateattributes(dir, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "The 'direction' of the sound field", 5);
        validatestring(dir, ["In", "Ingoing", "Converging", "Out", "Outgoing", "Diverging"], mfilename, "The 'direction' of the sound field", 5);

        if sum(strcmpi(dir, ["In", "Ingoing", "Converging"])) > 0
            dir = "In";
        end
    else
        dir = "In";
    end

    % =============================================
    % Calculate parameters
    % =============================================
    N = sqrt(size(shCoeffs, 1)) - 1; % Highest spherical harmonics order
    nDeg = 1:2:2 * N + 1; % Number of degrees/modes per order


    % =============================================
    % Extrapolate coefficients
    % =============================================
    % Create some matrices to ease calculations
    if strcmpi(dir, "in")
        Jref = besselj(repmat(0:N, length(k), 1), repmat(r(1) * k(:), 1, N + 1));
    else
        Jref = besselh(repmat(0:N, length(k), 1), repmat(r(1) * k(:), 1, N + 1));
    end

    % Go through the frequencies
    for fIdx = size(shCoeffs, 3):-1:1
        % Go through the measurements/positions
        for rIdx = length(r) - 1:-1:1
            tmpIdx = rIdx + 1;
            if strcmpi(dir, "in")
                J = besselj(0:N, repmat(k(fIdx) * r(tmpIdx), 1, N + 1));
            else
                J = besselh(0:N, repmat(k(fIdx) * r(tmpIdx), 1, N + 1));
            end

            % Replicate and weight J coefficients
            J = w(:).' .* repelem(J./Jref(fIdx, :), nDeg);
            J = diag(J);

            % Calculate extrapolated pressure coefficients
            extrCoeffs(:, :, rIdx, fIdx) = J * shCoeffs(:, :, fIdx);
        end
    end

    if nargout > 1
        extrCoeffsReduced = squeeze(extrCoeffs);
    end
end