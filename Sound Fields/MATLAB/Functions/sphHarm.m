%% Get the spherical harmonics up to a given order
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 29/09/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the spherical harmonics of given directions up
%                to a given order. Both complex and real harmonics are
%                provided.
% --------------------------------------------------
% Input
% 
% dir [numeric]: The directions for which the spherical harmonics will be
%                calculated. This  must be an 2xM matrix, where M is the
%                number of directions and 2 stands for the azimuth and
%                elevation. The angles must be given in radians. An
%                example of 2 directions would look like:
%                [az1, az2; el1, el2].
% 
% N [numeric]: The highest order of the spherical harmonics to be
%              calculated.
% 
% 
% --------------------------------------------------
% Output
% 
% cShpHarm [numeric]: The complex spherical harmonics up to order "N" for
%                     the directions "dir". This is an MxK matrix with M
%                     denoting the directions and K the spherical
%                     harmonics. The spherical harmonics (along each row)
%                     are layed with the largest negative degrees given
%                     first moving to higher degrees for each order.
%                     The orders are arranged from smallest to highest. An
%                     example of a row (one direction) for up to order 2
%                     is (first number represents order and second degree):
%                     [Y00, Y1-1, Y10, Y11, Y2-2, Y2-1, Y20, Y21, Y22].
% 
% 
% rShpHarm [numeric]: The real spherical harmonics up to order "N" for
%                     the directions "dir". This is an MxK matrix with M
%                     denoting the directions and K the spherical
%                     harmonics. The spherical harmonics (along each row)
%                     are layed with the largest negative degrees given
%                     first moving to higher degrees for each order.
%                     The orders are arranged from smallest to highest. An
%                     example of a row (one direction) for up to order 2
%                     is (first number represents order and second degree):
%                     [Y00, Y1-1, Y10, Y11, Y2-2, Y2-1, Y20, Y21, Y22].
% 
% cShpHarmMtx [numeric]: The complex spherical harmonics up to order "N"
%                        for the directions "dir" in a matrix format.
%                        This is an NxJxM matrix with N being the highest
%                        order of the spherical harmonics (actually the
%                        value of the input argument "N"). J is twice the
%                        highest order N (two accommodate the degrees of
%                        the highest order) and K is the number of
%                        directions (equal to the number of rows of the
%                        input argument "dirs"). For all orders other than
%                        the highest, the "non-existent" degrees are given
%                        as NaN. An example of a matrix for one direction
%                        with spherical harmonics up to order 2 is:
%                        [NaN,  NaN,  Y00, NaN, NaN; ...
%                         NaN,  Y1-1, Y10, Y11, NaN; ...
%                         Y2-2, Y2-1, Y20, Y21, Y22]
% 
% rShpHarmMtx [numeric]: The real spherical harmonics up to order "N"
%                        for the directions "dir" in a matrix format.
%                        This is an NxJxM matrix with N being the highest
%                        order of the spherical harmonics (actually the
%                        value of the input argument "N"). J is twice the
%                        highest order N (two accommodate the degrees of
%                        the highest order) and K is the number of
%                        directions (equal to the number of rows of the
%                        input argument "dirs"). For all orders other than
%                        the highest, the "non-existent" degrees are given
%                        as NaN. An example of a matrix for one direction
%                        with spherical harmonics up to order 2 is:
%                        [NaN,  NaN,  Y00, NaN, NaN; ...
%                         NaN,  Y1-1, Y10, Y11, NaN; ...
%                         Y2-2, Y2-1, Y20, Y21, Y22]
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [cSphHarm, rSphHarm, cSphHarmMtx, rSphHarmMtx] = sphHarm(dirs, N)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 4);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(dirs, "numeric", {'2d', 'nrows', 2, 'finite', 'nonnan', 'nonempty', 'real'}, mfilename, "Directions", 1);
    validateattributes(N, "numeric", {'scalar', 'finite', 'nonnegative', 'nonnan', 'nonempty', 'real'}, mfilename, "Highest spherical harmonics order", 2);


    % =============================================
    % Calculate the spherical harmonics
    % =============================================
    % Complex spherical harmonics
    for n = N:-1:0
        % Set the degrees
        m = 0:n;

        % Normalisation factors
        normFac = sqrt((2 * n + 1) * factorial(n - m)./(4 * pi * factorial(n + m)));

        % Legendre function
        Lnm = legendre(n, cos(dirs(2, :))).';

        % Exponential function
        Nnm = exp(1i * m .* dirs(1, :).');
        
        % Spherical harmonics
        tmpHarm = normFac .* Lnm .* Nnm;
        
        % Calculate negative degrees and put them in the matrix
        cSphHarm(:, n^2 + n + (-n:n) + 1) = [((-1).^m(end:-1:2)) .* conj(tmpHarm(:, end:-1:2)), tmpHarm];
    end


    % Real spherical harmonics
    if nargout > 1
        % Calculate real harmonics from complex
        for n = N:-1:0
            % Degrees (excluding for 0)
            m = 1:n;
            
            % Index of needed complex spherical harmonics
            idx = n^2 + n + m + 1;

            % Normalisation factors
            normFac = sqrt(2) * ((-1).^m);

            % Get the real spherical harmonics
            rSphHarm(:, n^2 + n + (-n:n) + 1) = [normFac(end:-1:1) .* imag(cSphHarm(:, idx(end:-1:1))), ...
                                                 cSphHarm(:, n^2 + n + 1), ...
                                                 normFac .* real(cSphHarm(:, idx))];
        end
    end

    % Complex spherical harmonics matrix
    if nargout > 2
        % Pre-allocate
        cSphHarmMtx = NaN * (1 + 1i) * zeros(N, length(-N:N), size(dirs, 2));

        % Go through the orders and degrees
        for n = N:-1:0
            for m = n:-1:-n
                cSphHarmMtx(n + 1, m + N + 1, :) = cSphHarm(:, n^2 + n + m + 1);
            end
        end
    end

    % Real spherical harmonics matrix
    if nargout > 3
        % Pre-allocate
        rSphHarmMtx = NaN * zeros(N, length(-N:N), size(dirs, 2));

        % Go through the orders and degrees
        for n = N:-1:0
            for m = n:-1:-n
                rSphHarmMtx(n + 1, m + N + 1, :) = rSphHarm(:, n^2 + n + m + 1);
            end
        end
    end
end