%% Perform an Inverse Discrete Spherical Fourier Transform
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 01/06/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate an Inverser Discrete Spherical Fourier
%                Transform.
% --------------------------------------------------
% Input
% 
% sphFtCoeffs [numeric]: This is a vector with the Discrete Spherical
%                        Fourier Transform coefficients of the spatially
%                        sampled wave-field. This must be an NxM matrix
%                        with N denoting the number of Spherical Fourier
%                        coefficients. The transform is calculated for each
%                        column Mi.
% 
% sphHarms [numeric]: The spherical harmonics corresponing to the Discrete
%                     Spherical Fourier Transform coefficients in
%                     "sphFtCoeffs" argument. This must be a matrix of
%                     dimensions KxN where K is the number of spatial
%                     sampling points and N the number of spherical
%                     harmonics in the expansion.
% 
% --------------------------------------------------
% Output
% 
% waveFieldSamps [numeric]: The wave field samples for each column of the
%                           input argument "sphFtCoeffs". This is a KxM
%                           matrix with K the number of spatial samples and
%                           M the number of columns in "sphFtCoeffs"
%                           argument (possibly representing different
%                           spherical functions).
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function waveFieldSamps = idsft(sphFtCoeffs, sphHarms)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(sphFtCoeffs, "numeric", {'2d', 'finite', 'nonnan', 'nonempty'}, mfilename, "Discrete Spherical Fourier Transform coefficients", 1);
    validateattributes(sphHarms, "numeric", {'2d', 'finite', 'nonnan', 'nonempty', 'ncols', size(sphFtCoeffs, 1)}, mfilename, "Spherical harmonics corresponding to the Discrete Spherical Fourier Transform coefficients", 2);


    % =============================================
    % Calculate the sampled signals
    % =============================================
    waveFieldSamps = sphHarms * sphFtCoeffs;
end