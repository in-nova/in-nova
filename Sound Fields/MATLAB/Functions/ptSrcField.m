%% Calculate wavefield generated by point sources
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 19/12/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the wavefield generated by point sources at
%                specified locations.
% --------------------------------------------------
% Input
% 
% sPos [numeric]: The position of the sources in Cartesian coordinates. The
%                 variable should be a matrix with dimensions 3xN where N
%                 is the number of sources and the rows represent the x, y
%                 and z coordinates respectively.
% 
% rPos [numeric]: The position of the receivers in Cartesian coordinates.
%                 The variable should be a matrix with dimensions 3xM where
%                 M is the number of receivers and the rows represent the
%                 x, y and z coordinates respectively.
% 
% freqs [numeric]: The frequencies for which to calculate the wave field.
%                  It must be a scalar or vector.
% 
% Q [numeric] (Optional): The source strength of the point sources. This
%                         variable must be either a scalar corresponding to
%                         all sources having the same strength, or a matrix
%                         with dimensions NxK, where K is the number of
%                         frequencies of interest. The strengths can be
%                         complex numbers. [Default: 1].
% 
% c [numeric] (Optional): The speed of sound in m/s. [Default: 343].
% 
% rho [numeric (Optional): The density of the medium in kg/(m^3).
%                          [Default: 1.204].
% 
% --------------------------------------------------
% Output
% 
% waveField [numeric]: The complex pressures at the position of the
%                      receivers. The variable is a matrix of dimensions
%                      MxNxF where M is the number of receivers, N the
%                      number of sources and F the number of frequencies.
% 
% --------------------------------------------------
% Notes
% 
% Dependencies: - twoPtDist(): To calculate the distances between sources
%                              and receivers.
% --------------------------------------------------
function waveField = ptSrcField(sPos, rPos, f, Q, c, rho)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(3, 6);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(sPos, "numeric", {'2d', 'nrows', 3, 'finite', 'nonnan', 'nonempty', 'real'}, mfilename, "Source positions", 1);
    validateattributes(rPos, "numeric", {'2d', 'nrows', 3, 'finite', 'nonnan', 'nonempty', 'real'}, mfilename, "Receiver positions", 2);
    validateattributes(f, "numeric", {'vector', 'finite', 'nonnan', 'nonempty', 'real'}, mfilename, "Frequencies", 3);

    % Validate optional arguments (and set their values)
    if nargin > 5 && ~isempty(rho)
        validateattributes(rho, "numeric", {'scalar', 'finite', 'positive', 'nonnan', 'nonempty', 'real'}, mfilename, "Medium density", 6);
    else
        rho = 1.204;
    end

    if nargin > 4 && ~isempty(c)
        validateattributes(c, "numeric", {'scalar', 'finite', 'positive', 'nonnan', 'nonempty', 'real'}, mfilename, "Speed of propagation", 5);
    else
        c = 343;
    end

    if nargin > 3 && ~isempty(Q)
        validateattributes(Q, "numeric", {'2d', 'finite', 'nonempty', 'nonnan'}, mfilename, "Source strength(s)", 4);

        % Make sure Q has the correct dimensions
        if ~isscalar(Q) && sum((size(Q) ~= [size(sPos, 2), length(f)])) > 0
            error("Source strength(s) must be either a scalar or its length must match the number of sources.");
        end
    else
        Q = 1;
    end


    % ====================================================
    % "Condition" arguments
    % ====================================================
    % Make sure frequencies is a column vector
    f = f(:);


    % =============================================
    %  Calculate variables
    %  ============================================
    s2rDist = twoPtDist(sPos, rPos).';

    % Create a vector with appropriate dimensions for the source strength(s)
    if isscalar(Q)
        Q = ones(size(sPos, 2), length(f)) * Q;
    end
    

    % =============================================
    %  Calculate variables
    %  ============================================
    % Go through the frequencies
    parfor fIdx = 1:numel(f)
        % Calculate parameters
        w = 2 * pi * f(fIdx); % Radial frequency
        k = w/c; % Wavenumber

        % Calculate complex pressure
        waveField(:, :, fIdx) = (1j * w * rho * Q(:, fIdx).')./(4 * pi * s2rDist) .* exp(-1j * k * s2rDist);
    end
end