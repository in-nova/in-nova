# Utilities

This section holds generic, utility functions that do not fall under any other category and can be used in various cases. There are currently two sub-categories, the "Generic" and the "Geometries". The first contains generic functions that can be used in various cases. The latter contains code to generate microphone and source geometries. Although it has been created to be used in the *Active Noise Control* problem to generate arrangements of sources and receivers it can be used for any other purpose.

## Dependencies

There are depenencies between the different sub-categories of the utility functions. The known dependencies are summarised below and more information can be found in the respective [Wiki](https://gitlab.com/in-nova/in-nova/-/wikis/home) pages of each function or in the [Documentation](https://gitlab.com/in-nova/in-nova/-/tree/main/Documentation/latex?ref_type=heads).

- MATLAB
  - Generic
    - `isOdd()`
      - `isEven()`
  - Geometries
    - `rcvGeo()`
      - `rotMat3d()`
    - `srcGeo()`
      - `rotMat3d()`
      - `ptsOnSphere()`
    - `virtMicGeo()`
      - `rotMat3d()`

#### Acknowledgments

- Part of the function `ptsOnSphere.m` has been adapted from a MATLAB® function provided by Prof. Jordan Cheer of ISVR - University of Southampton.