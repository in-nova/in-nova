%% Determine whether a number is even or not
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 04/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Function to check if a real number is even.
% --------------------------------------------------
% Input arguments
%
% num [numeric]: The number to check.
%
% --------------------------------------------------
% Output arguments
% 
% result [logical]: True if the number is even, False else.
%
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function result = isEven(num)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 1);
    nargoutchk(0, 1);

    % ====================================================
    % Validate arguments
    % ====================================================
    % Mandatory arguments
    validateattributes(num, "numeric", {'real', 'nonempty'}, mfilename, "Number to test.", 1);
    
    % ====================================================
    % Perform the check
    % ====================================================
    result = mod(num, 2) == 0;
end