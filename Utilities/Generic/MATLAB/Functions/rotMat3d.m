%% Calculate a 3D rotation matrix
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 14/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate a 3D rotation matrix.
% --------------------------------------------------
% Input
% 
% xAng [numeric]: Rotation angle around the x-axis.
% 
% yAng [numeric] (Optional): Rotation angle around the y-axis.
%                            [Default: 0].
% 
% zAng [numeric] (Optional): Rotation angle around the z-axis.
%                            [Default: 0].
% 
% rotOrd [char/string] (Optional): The order the rotations will be applied.
%                                  This can be some permutation of the
%                                  string "xyz" and is not case-sensistive.
%                                  The order corresponds to multiplication
%                                  on the left (i.e. R * v, where "R" is
%                                  the  rotation matrix and "v" a vector to
%                                  be rotated). [Default: "xyz"].
% 
% deg [logical] (Optional): This value declares if the provided angles are
%                           in radians or degrees. If it is true, the
%                           values are treated as degrees.
%                           [Default: false].
% --------------------------------------------------
% Output
% 
% rotMat [numeric]: A 3x3 matrix which applies the rotation.
% 
% xRotMat[numeric]: A 3x3 matrix which applies the rotation along the
%                   x-axis.
% 
% yRotMat[numeric]: A 3x3 matrix which applies the rotation along the
%                   y-axis.
% 
% zRotMat[numeric]: A 3x3 matrix which applies the rotation along the
%                   z-axis.
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [rotMat, xRotMat, yRotMat, zRotMat] = rotMat3d(xAng, yAng, zAng, rotOrd, deg)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 5);
    nargoutchk(0, 4);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Mandatory arguments
    validateattributes(xAng, "numeric", {'scalar', 'nonnan', 'finite', 'real'}, mfilename, 'Roll angle', 1);

    % Optional arguments
    if nargin > 1 && ~isempty(yAng)
        validateattributes(yAng, "numeric", {'scalar', 'nonnan', 'finite', 'real'}, mfilename, 'Pitch angle', 2);
    else
        yAng = 0;
    end

    if nargin > 2 && ~isempty(zAng)
        validateattributes(zAng, "numeric", {'scalar', 'nonnan', 'finite', 'real'}, mfilename, 'Yaw angle', 2);
    else
        zAng = 0;
    end

    if nargin > 4 && ~isempty(rotOrd)
        validateattributes(rotOrd, {'char', 'string'}, {'scalartext'}, mfilename, "The sequence of the rotations", 4);
        validatestring(rotOrd, join(perms(["x", "y", "z"]), ""), mfilename, "The sequence of the rotations", 4);
    else
        rotOrd = "xyz";
    end

    if nargin > 4 && ~isempty(deg)
        validateattributes(deg, "logical", {'scalar', 'nonempty', 'binary', 'real', 'finite', 'nonnan'}, mfilename, "Flag declaring whether the provided angle values are in degrees", 5);
    else
        deg = false;
    end


    % ====================================================
    % Pre-process angle values
    % ====================================================
    if deg
        xAng = deg2rad(xAng);
        yAng = deg2rad(yAng);
        zAng = deg2rad(zAng);
    end


    % ====================================================
    % Create respective matrices
    % ====================================================
    % Rolling rotation matrix
    xRotMat = [1, 0, 0;...
               0, cos(xAng), -sin(xAng);...
               0, sin(xAng), cos(xAng)];

    % Pitch rotation matrix
    yRotMat = [cos(yAng), 0, sin(yAng);...
                0, 1, 0;...
                -sin(yAng), 0, cos(yAng)];

    % Yaw rotation matrix
    zRotMat = [cos(zAng), -sin(zAng), 0;...
              sin(zAng), cos(zAng), 0;...
              0, 0, 1];

    % Create rotation matrix based on the sequency provided
    rotMat = cat(3, xRotMat, yRotMat, zRotMat);

    rotOrd = split(rotOrd, ""); rotOrd(rotOrd == "") = [];
    [~, idx] = sort(rotOrd); [~, idx] = sort(idx, "descend");

    rotMat = rotMat(:, :, idx(1)) * rotMat(:, :, idx(2)) * rotMat(:, :, idx(3));
end
