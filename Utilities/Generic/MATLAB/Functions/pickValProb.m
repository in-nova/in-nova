%% Pick a value based on its probability
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 19/08/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Pick numbers from an array based on
%                their probabilities
% --------------------------------------------------
% Input
% 
% vals [numeric]: This should be a vector holding the values from which to
%                 pick.
% 
% probs [numeric]: These are the probabilities of each element being
%                  picked. It must be a vector with number of elements
%                  equal to "vals". Its elements must lie in the range
%                  [0, 1] and their sum must equal 1 (with a tolerance of
%                  1000 epsilon).
% 
% nVals [numeric] (Optional): The number of values to return. [Default: 1]
% 
% --------------------------------------------------
% Output
% 
% result [numeric]: The picked value(s).
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function result = pickValProb(vals, probs, nVals)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(vals, "numeric", {'vector', 'finite', 'nonempty'}, mfilename, "Values from which to pick.", 1);
    validateattributes(probs, "numeric", {'numel', numel(vals), 'real', 'vector', 'nonempty', 'nonnan', 'finite', '<=', 1, '>=', 0}, mfilename, "Probability of each number to be picked.", 2);

    % Check that probabilities sum to 1 with a tolerance of 10 * eps
    if abs(sum(probs) - 1) > 1e3 * eps
        error("pickValProb(): Sum of probabilities must be one (with a tolerance of about 1000 * eps)");
    end


    % Validate optional arguments
    if nargin > 2
        validateattributes(nVals, "numeric", {'scalar', 'finite', 'positive', 'real', 'nonempty', 'nonnan', 'integer'}, mfilename, "Number of values to return.", 3);
    else
        nVals = 1;
    end


    % ====================================================
    % Pick values
    % ====================================================
    % Calculate cumulative probability
    P = cumsum(probs(:)/sum(probs));

    % Generate random values
    randIdx = rand(nVals, 1) * ones(1, length(vals));

    % Get the correct elements
    randIdx = sum(P(:).' < randIdx, 2) + 1;
    result = vals(randIdx);
end