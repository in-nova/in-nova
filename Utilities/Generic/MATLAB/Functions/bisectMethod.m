%% Bisection method for a single argument scalar function
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 10/05/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Approximate a root of a single argument scalar valued
%                function with the bisection method.
% --------------------------------------------------
% Input
% 
% xLow [numeric]: The low end of the function domain to be searched. This
%                 must be a real scalar.
% 
% xHigh [numeric]: The high end of the function domain to be searched. This
%                  must be a real scalar.
% 
% func [function_handle]: The function being evaluated. 
% 
% yLow [numeric] (Optional): The value of the function at "xLow". If this
%                            is not provided, the value is calculated with
%                            "f". [Default: []].
% 
% nIters [numeric] (Optional): The maximum number of iterations to be
%                              performed. [Default: 10].
% 
% tol [numeric] (Optional): The tolerance in the value of the argument.
%                           [Default: 1e-3].
% 
% --------------------------------------------------
% Output
% 
% xZero [numeric]: The argument closest to the root as found by the
%                  algorithm.
% 
% yZero [numeric]: The value of the function at "xZero".
% 
% nIter [numeric]: The number of iterations performed.
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [xZero, yZero, nIter] = bisectMethod(xLow, xHigh, func, yLow, nIters, tol)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(3, 6);
    nargoutchk(0, 3);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(xLow, "numeric", {'scalar', 'real', 'nonnan', 'nonempty', 'finite'}, mfilename, "The low end of the function domain to be searched", 1);
    validateattributes(xHigh, "numeric", {'scalar', 'real', 'nonnan', 'nonempty', 'finite'}, mfilename, "The high end of the function domain to be searched", 2);
    validateattributes(func, "function_handle", {'nonempty'}, mfilename, "A 'function_handle' to the function to be evaluated", 3);

    % Validate optional arguments
    if nargin > 3 && ~isempty(yLow)
        validateattributes(yLow, "numeric", {'scalar', 'nonempty', 'nonnan', 'real', 'finite'}, mfilename, "The value of the function at the low end of the domain to be searched", 4);
    else
        yLow = func(xLow);
    end

    if nargin > 4 && ~isempty(nIters)
        validateattributes(nIters, "numeric", {'scalar', 'nonempty', 'nonnan', 'positive', 'real', 'finite', 'integer'}, mfilename, "Number of maximum iterations", 5);
    else
        nIters = 10;
    end

    if nargin > 5 && ~isempty(tol)
        validateattributes(tol, "numeric", {'scalar', 'nonempty', 'nonnan', 'finite', 'real'}, mfilename, "The tolerance of the difference in the argument", 6);
    else
        tol = 1e-3;
    end


    % ====================================================
    % Calculate central and edge frequencies
    % ====================================================
    % Start iterating
    for nIter = 1:nIters
        % Calculate parameters
        midPt = (xLow + xHigh)/2; % Mid-point
        yMidPt = func(midPt); % Value of function at mid-point
    
        % Check for conditions
        if abs(yMidPt) == 0 || ((xHigh - xLow)/2) < tol
            break;
        end
    
        % Check which point we should discard
        if sign(yMidPt) == sign(yLow)
            xLow = midPt;
            yLow = yMidPt;
        else
            xHigh = midPt;
        end
    end
    
    % Return the values we found
    xZero = midPt; % Argument
    yZero = yMidPt; % Function value
end