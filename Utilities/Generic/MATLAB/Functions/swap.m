%% Function to swap to variables
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 22/05/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Swap to variables. The variables can be anything, even
%                empty and they don't have to have matched types or sizes.
% --------------------------------------------------
% Input
% 
% x [anything]: The first variable to swap.
% 
% y [anything]: The second variable to swap.
% 
% --------------------------------------------------
% Output
% 
% xSwap [anything]: This variable now holds the content of the argument
%                   "y".
% 
% ySwap [anything]: This variable now holds the content of the argument
%                   "x".
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function [xSwap, ySwap] = swap(x, y)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 2);

    % ====================================================
    % Swap variables
    % ====================================================
    xSwap = y;
    
    if nargout > 1
        ySwap = x;
    end
end