%% Find the intersection of three vectors
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 03/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Get the intersection (common values) of three vectors. Can
%                provide the indices of the values as an additional output.
% --------------------------------------------------
% Input
% 
% vecOne [any]: The first vector.
% 
% vecTwo [any]: The second vector.
% 
% vecThree [any] (Optional): The third vector. [Defaul: []].
% --------------------------------------------------
% Output
% 
% commonVals [any]: The values that are common in all three vectors. The
%                   values are sorted in non-decreasing order and, if
%                   repeated, only the first occurence is "registered".
% 
% idxOne [numeric]: The indices corresponding to the common values in the
%                   first vector.
% 
% idxTwo [numeric]: The indices corresponding to the common values in the
%                   second vector.
% 
% idxThree [numeric]: The indices corresponding to the common values in the
%                     third vector.
% --------------------------------------------------
% Notes
% 
% All inputs must be vectors. Matrices are not supported.
% --------------------------------------------------
function [commonVals, idxOne, idxTwo, idxThree] = intersectThree(vecOne, vecTwo, vecThree)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(0, 4);

    % ====================================================
    % Validate input arguments
    % ====================================================
    validateattributes(vecOne, {'numeric'}, {'vector'}, mfilename, '1st vector', 1);
    validateattributes(vecTwo, {'numeric'}, {'vector'}, mfilename, '2nd vector', 2);

    if nargin > 2
        validateattributes(vecThree, {'numeric'}, {'vector'}, mfilename, '3rd vector', 3);
    else
        vecThree = [];
    end

    % ====================================================
    % Calculate intersection
    % ====================================================
    [commonVals, idxOne, idxTwo] = intersect(vecOne, vecTwo); % First two vectors
    [commonVals, idxTemp, idxThree] = intersect(commonVals, vecThree); % The result with the third vector
    
    % Get the indices of vecOne and vecTwo that correspond to values
    % present in vecThree
    if nargout > 1
        idxOne = idxOne(idxTemp);
    end
    
    if nargout > 2
        idxTwo = idxTwo(idxTemp);
    end
end