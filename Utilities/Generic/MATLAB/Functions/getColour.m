%% Get MATLAB's colours in RGB array
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 10/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Get MATLAB's colours in RGB array
% --------------------------------------------------
% Input
% 
% colour [numeric/string]: The colour to be returned. Numerical values
%                          correspond to "blue", "orange", "yellow",
%                          "purple", "green", "mustard", "grey", and a
%                          "kind of burgundy" from 1 to 8. Values other
%                          than those will result in an error. Decimal
%                          values will be rounded to the nearest integer.
%                          The string values accepted are those
%                          corresponding to the numerical values and are
%                          not case sensitive.
% 
% --------------------------------------------------
% Output
% 
% rgb [numeric]: The Red-Green-Blue (RGB) values of the chosen colour in a
%                column vector.
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function rgb = getColour(colour)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 1);
    nargoutchk(0, 1);

    % If no output arguments bail (no need to calculate)
    if nargout == 0
        return;
    end

    % ====================================================
    % Validate input arguments
    % ====================================================
    if ~isStringScalar(colour)&& ~(isnumeric(colour) && isscalar(colour))
        error("Colour must be a scalar or string.")
    end




    % Return the appropriate colour
    if(colour == 1 || strcmpi(colour, "blue"))
        rgb = [0, 0.45, 0.74];
    elseif (colour == 2 || strcmpi(colour, "orange"))
        rgb = [0.85, 0.33, 0.1];
    elseif (colour == 3 || strcmpi(colour, "yellow"))
        rgb = [0.93, 0.69, 0.13];
    elseif (colour == 4 || strcmpi(colour, "purple"))
        rgb = [0.49,0.18,0.56];
    elseif (colour == 5 || strcmpi(colour, "green"))
        rgb = parula;
        rgb = rgb(end/2, :);
    elseif (colour == 6 || strcmpi(colour, "mustard"))
        rgb = parula;
        rgb = rgb(3 * end/4, :);
    elseif (colour == 7 || strcmpi(colour, "grey"))
        rgb = [0.5, 0.5, 0.5];
    elseif (colour == 8) || strcmpi(colour, "burgundy")
        rgb = [0.5, 0.1, 0.2];
    else
        error("Colour must be either a number from 1 to 4 or one of: 'blue', 'orange', 'yellow', 'purple'")
    end