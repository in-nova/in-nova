%% Calculate equispaced positions on a sphere surface
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 30/09/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate positions of uniformly distributed source on a
%                sphere.
% --------------------------------------------------
% Input arguments
%
% rad [numeric]: The radius of the sphere.
%
% param [numeric]: This is the parameter used for the generation of the
%                  positions on the sphere and depends on the value of the
%                  "method" argument. If "method" is "Grid" this argument
%                  corresponds to the angular distance in degrees, between
%                  positions on the sphere surface. If "method" is
%                  "Fibonacci" this argument corresponds to the number of
%                  points (almost, see next argument).
% 
% method [string/char] (Optional): The method to be used to generate the
%                                  points. This argument can be either
%                                  "Grid" or "Fibonacci" and is not case
%                                  sensitive. The "Grid" method generates
%                                  poits of approximately equal angular
%                                  distance, while the "Fibonacci" method
%                                  calculates points on spirals with
%                                  specific features (see "Notes" for more
%                                  information"). The "Fibonacci" method
%                                  always returns an odd number of sources.
%                                  [Default: Grid].
%
% --------------------------------------------------
% Output arguments
% 
% pts [numeric]: The 3D vectors of the positions. The dimensions of the
%                matrix is 3xN where N is the number of sources and the
%                rows represent the coordinates of the vectors [x,y,z].
%
% --------------------------------------------------
% Acknowledgements
% 
% - The "Grid" method is an adaptation of a function provided by Prof.
%   Jordan Cheer of the Institute of Sound and Vibration Research (ISVR),
%   University of Southampton.
% 
% - The "Fibonacci" method is taken from "Measurement of Areas on a Sphere
%   Using Fibonacci and Latitude-Longitude Lattices" by Alvaro Gonzalez.
% 
% --------------------------------------------------
function pts = ptsOnSphere(rad, param, method)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 3);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Method is needed to evaluate the parameter argument
    if nargin > 2 && ~isempty(method)
        validateattributes(method, {'char', 'string'}, "scalartext", mfilename, "Method of point calculation", 3);
        validatestring(method, ["Grid", "Fibonacci"], mfilename, "Method of point calculation", 3);
    else
        method = "Grid";
    end

    % Validate mandatory arguments
    validateattributes(rad, "numeric", {'scalar', 'finite', 'nonempty', 'nonnan', 'real'}, mfilename, "The radius of the sphere.", 1);

    if strcmpi(method, "Grid")
        validateattributes(param, "numeric", {'scalar', 'finite', 'nonempty', 'nonnan', 'positive', 'real'}, mfilename, "The inter-element angular distance", 2);
    else
        validateattributes(param, "numeric", {'scalar', 'finite', 'nonempty', 'nonnan', 'positive', 'real', 'integer'}, mfilename, "The number of sources minus one", 2);
    end


    % ====================================================
    % Generate positions
    % ====================================================
    if strcmpi(method, "Grid")
        % Define sensors in the horizontal plane
        theta = deg2rad(0:param:360 - param).'; % Angles
        
        % Angles between the horizontal planes
        phi = deg2rad(-90 + param:param:90 - param);
        
        % Calculate the number of sources on each horizontal plane
        nHorSrc = (floor(length(theta) * cos(phi)));
        totSrc = sum(nHorSrc);
        param = 2 * pi./nHorSrc; % Actual delta theta for the generated points
        
        % ====================================================
        % Calculate positions
        % ====================================================
        % Pre-allocate
        azim = zeros(1, totSrc);
        elev = zeros(1, totSrc);
        
        % Go through the angles
        cnt = 1;
        for idx = 1:length(phi)
            azim(1, cnt:cnt + nHorSrc(idx) - 1) = (0:param(idx):2 * pi - param(idx));
            elev(1, cnt:cnt + nHorSrc(idx) - 1) = repmat(phi(idx), 1, nHorSrc(idx));
            cnt = cnt + nHorSrc(idx);
        end
        
        % Convert to Cartesian
        [x, y, z] = sph2cart(azim, elev, repmat(rad, 1, totSrc));
        pts = [x; y; z];
        
        % Add two sources on the "poles"
        pts = cat(2, [0; 0; rad], pts, [0; 0; -rad]);
    else
        % Define some parameters
        gRatio = 1.61803398875; % Golden ratio
        param = floor(param/2); 

        % Pre-allocate
        pts = zeros(2, param);
        cnt = 1;
    
        % Calculate points
        for idx = -param:param
            elev = asin((2 * idx)/(2 * param + 1)) * 180/pi;
            azim = mod(idx, gRatio) * 360/gRatio;
            
            azim = azim + (360 * azim < -180) - (360 * azim > 180);
    
            pts(:, cnt) = [azim; elev];
            cnt = cnt + 1;
        end

        [ptsX, ptsY, ptsZ] = sph2cart(deg2rad(pts(1, :)), deg2rad(pts(2, :)), rad);
        pts = [ptsX; ptsY; ptsZ];
    end
end
