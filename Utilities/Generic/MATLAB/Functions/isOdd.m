%% Determine whether a number is even or not
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 04/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Function to check if a real number is odd.
% --------------------------------------------------
% Input arguments
%
% num [numeric]: The number to check.
%
% --------------------------------------------------
% Output arguments
% 
% result [logical]: true if the number is odd, false else.
%
% --------------------------------------------------
% Notes
% 
% Dependencies: - isEven() from the IN-NOVA MATLAB® Utilities.
% --------------------------------------------------
function result = isOdd(num)
    % ====================================================
    % Perform the check
    % ====================================================
    result = ~isEven(num);
end