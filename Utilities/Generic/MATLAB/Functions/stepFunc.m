%% Numerical (simple) Heaviside function
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 03/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: A Heaviside (step) function
% --------------------------------------------------
% Input
% 
% x [numeric]: The argument of the function. Multi-dimensional arrays are
%              supported.
% 
% zeroHalf [logical] (Optional): The way x = 0 is treated. If this argument
%                                is true, the returned value is 0.5 when
%                                x = 0, otherwise it is 1.
%                                [Default: false].
% 
% --------------------------------------------------
% Output
% result [numeric]: Either 0 or 1 based on whether the argument of the
%                   function is greater or equal to zero.
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function result = stepFunc(x, zeroHalf)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate arguments
    % ====================================================
    % Mandatory arguments
    validateattributes(x, "numeric", {'real', 'nonempty', 'nonnan', 'finite'}, mfilename, 'Function argument.', 1);

    % Optional arguments
    if nargin > 1
        validateattributes(zeroHalf, "logical", {'scalar', 'nonempty', 'nonnan'}, mfilename, "Treatment of x = 0 case.", 2);
    else
        zeroHalf = false;
    end

    
    % ====================================================
    % Calculate result
    % ====================================================
    if zeroHalf
        result = heaviside(x);
    else
        result = x >= 0;
    end
end