%% Pick a number of unique rows from a matrix at random
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 06/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Pick a specified number of unique rows from a matrix at
%                random.
% --------------------------------------------------
% Input arguments
%
% mat [numeric]: The matrix from which rows will be picked. Only 2D
%                matrices are supported.
%
% nRows [numeric]: The number of unique rows to pick from the matrix.
%
% --------------------------------------------------
% Output arguments
% 
% uniqueRows [numeric]: The unique rows picked from the matrix
%
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function uniqueRows = randUniqRows(mat, nRows)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate arguments
    % ====================================================
    % Mandatory arguments
    validateattributes(mat, "numeric", {'nonempty', '2d'}, mfilename, "Matrix from which to pick rows", 1);
    validateattributes(nRows, "numeric", {'scalar', 'nonempty', 'integer', 'positive', 'finite', 'real', 'nonnan'}, mfilename, "Number of unique rows to pick", 2);

    % ====================================================
    % Start working
    % ====================================================
    % Get the unique rows of the matrix
    uniqueRows = unique(mat, "rows", "stable");
    
    % Check number of items asked for against available items
    if nRows >= size(uniqueRows, 1)
        return;
    else
        uniqueIdx = [];
        while numel(uniqueIdx) < nRows
            uniqueIdx = cat(1, uniqueIdx, randi(size(uniqueRows, 1), nRows, 1));
            uniqueIdx = unique(uniqueIdx, "stable");
        end
        uniqueRows = uniqueRows(uniqueIdx(1:nRows), :);
    end
end