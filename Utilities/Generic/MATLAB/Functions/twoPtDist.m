%% Calculate the distance between two points in space
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 29/09/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate distance between positions in 3D.
% --------------------------------------------------
% Input arguments
%
% srcPos [numeric]: The 3D position vectors of the starting points. The
%                   columns represent points and the rows their Cartesian
%                   coordinates [x,y,z] resulting in a 3xN matrix with N
%                   being the number of points.
%
% rcvPos [numeric]: The 3D position vectors of the end points. The columns
%                   represent points and the rows their Cartesian
%                   coordinates [x,y,z] resulting in an 3xM matrix with M
%                   being the number of points.
%
% --------------------------------------------------
% Output arguments
% 
% dist [numeric]: The distance between each start and end point. The
%                 dimensions of the argument is NxM with each row
%                 corresponding to the distance between the nth start point
%                 and each of the M end points.
%
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function dist = twoPtDist(srcPos, rcvPos)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 2);
    nargoutchk(0, 1);

    % ====================================================
    % Validate arguments
    % ====================================================
    % Mandatory arguments
    validateattributes(srcPos, "numeric", {'nrows', 3, 'nonempty', 'nonnan', 'finite', 'real'}, mfilename, "Start points Cartesian coordinates", 1);
    validateattributes(rcvPos, "numeric", {'nrows', 3, 'nonempty', 'nonnan', 'finite', 'real'}, mfilename, "End points Cartesian coordinates", 2);

    % ====================================================
    % Start working
    % ====================================================
    % Go through the sources
    for idx = size(srcPos, 2):-1:1
        dist(idx, :) = vecnorm(rcvPos - srcPos(:, idx), 2, 1);
    end
end