% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 03/02/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Function to synthesise double-sided spectrum
%                from their positive (first in MatLab) half.
% --------------------------------------------------
% Input arguments
%
% mag [numeric]: The magnitude of the frequency response. It must
%                correspond to only the positive frequencies.
%
% phase [numeric]: The phase of the frequency response. It must correspond
%                  to the positive frequencies only.
% 
% nyquistFreq [logical] (Optional): Whether the Nyquist frequency is
%                                   included in the given data ("mag" and
%                                   "phase" arguments). [Default: false].
%
% --------------------------------------------------
% Output arguments
% 
% FR [numeric]: The double-sided spectrum in Cartesian coordinates
%               (z = x + jy).
%
% --------------------------------------------------
function FR = dblSideSpec(mag, phase, nyquistFreq)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(2, 5);
    nargoutchk(0, 1);
    
    % ====================================================
    % Validate input arguments
    % ====================================================
    validateattributes(mag, {'numeric'}, {'nonnegative', 'real', 'nonnan', 'finite', 'nonempty', 'vector'}, mfilename, 'Magnitude', 1);
    validateattributes(phase, {'numeric'}, {'real', 'nonnan', 'finite', 'nonempty', 'vector', 'numel', length(mag)}, mfilename, 'Phase', 2);
    
    if nargin > 2
        validateattributes(nyquistFreq, {'logical'}, {'scalar', 'nonnan', 'nonempty'}, mfilename, 'Nyquist Frequency', 3);
    else
        nyquistFreq = false;
    end
    
    % ====================================================
    % Mirror the frequency response
    % ====================================================
    % Put the frequency response in Cartesian coordinates
    [x, y] = pol2cart(phase, mag);
    FR = x(:) + 1i * y(:);
    
    if nyquistFreq
        FR = [FR; flip(conj(FR(2:end - 1)))];
    else
        FR = [FR; flip(conj(FR(2:end)))];
    end
end