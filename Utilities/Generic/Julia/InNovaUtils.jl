"""
This is the module with the Julia implementations of the generic utilities of the IN-NOVA project.
--------------------------------------------------
Author: Achilles Kappis
e-mail: axilleaz@protonmail.com

Date: 19/08/2024 (DD/MM/YYYY)

Copyright: MIT
--------------------------------------------------

# Functions included
- stepFunc
- rotMat3d
- pickValProb
- twoPtDist
--------------------------------------------------
"""
module Utils

# Add packages that are needed
using LinearAlgebra # Native linear algebra


"""
Numerical (simple) Heaviside function
--------------------------------------------------
Author: Achilles Kappis
e-mail: axilleaz@protonmail.com

Date: 19/08/2024 (DD/MM/YYYY)

Copyright: MIT
--------------------------------------------------
Functionality: A Heaviside numerical (step) function
--------------------------------------------------
# Input

x::Real: The argument of the function.

zeroHalf::Bool (Optional): The way x = 0 is treated. If this argument is true, the returned value is 0.5 when x = 0, otherwise it is 1. [Default: false].

--------------------------------------------------
# Output

result::Float64/Bool: The returned value can either be a boolean or a Float64 with value 0.5 if the "zeroHalf" argument is set "x" is zero.

--------------------------------------------------
# Notes

--------------------------------------------------
"""
function stepFunc(x::Real; zeroHalf::Bool = false)
    if zeroHalf && x == 0
        result = 0.5
    else
        result = x >= 0
    end
    
    return result
end




"""
Calculate a 3D rotation matrix
--------------------------------------------------
Author: Achilles Kappis
e-mail: axilleaz@protonmail.com

Date: 10/08/2024 (DD/MM/YYYY)

Copyright: MIT
--------------------------------------------------
Functionality: Calculate a 3D rotation matrix.
--------------------------------------------------
# Input

xAng::Real: Rotation angle around the x-axis.

yAng::Real (Optional): Rotation angle around the y-axis. [Default: 0].

zAng::Real (Optional): Rotation angle around the z-axis. [Default: 0].

deg::Bool (Optional) [keyword]: This value declares if the provided angles are in degrees or not. If not, radians are assumed. [Default: false].
--------------------------------------------------
# Output

rotMat::Matrix{Real}: A 3x3 matrix which applies the rotation.

xRotMat::Matrix{Real}: A 3x3 matrix which applies the rotation around the x-axis.

yRotMat::Matrix{Real}: A 3x3 matrix which applies the rotation around the y-axis.

zRotMat::Matrix: A 3x3 matrix which applies the rotation around the z-axis.
--------------------------------------------------
# Notes

--------------------------------------------------
"""
function rotMat3d(xAng::Real, yAng::Real = 0, zAng::Real = 0; deg::Bool = false)
    # Check for units of the angles
    if deg
        xAng = deg2rad(xAng)
        yAng = deg2rad(yAng)
        zAng = deg2rad(zAng)
    end
    
    # Generate the respective matrices
    xRotMat = [1 0 0; 0 cos(xAng) -sin(xAng); 0 sin(xAng) cos(xAng)]
    yRotMat = [cos(yAng) 0 sin(yAng); 0 1 0; -sin(yAng) 0 cos(yAng)]
    zRotMat = [cos(zAng) -sin(zAng) 0; sin(zAng) cos(zAng) 0; 0 0 1]
    
    # Calculate the "total" matrix
    rotMat = xRotMat * yRotMat * zRotMat
    
    # Return
    return rotMat, xRotMat, yRotMat, zRotMat
end





"""
Pick a value based on its probability
--------------------------------------------------
Author: Achilles Kappis
e-mail: axilleaz@protonmail.com

Date: 19/08/2024 (DD/MM/YYYY)

Copyright: MIT
--------------------------------------------------
Functionality: Pick numbers from an array based on their probabilities
--------------------------------------------------
# Input

vals::Vector{Number}: All values from which to pick some.

probs::Vector{Real}: These are the probabilities of each element being picked. It must has as many elements as the "vals" argument. Its elements must lie in the range [0, 1] and their sum must equal 1 (with a tolerance of about 1000 epsilon of Float64).

nVals::Real (Optional): The number of values to return. [Default: 1]

--------------------------------------------------
# Output

result::Vector{Number}: The picked value(s).

--------------------------------------------------
# Notes

--------------------------------------------------
"""
function pickValProb(vals::Vector{<:Real}, probs::Vector{<:Real}, nVals::Real = 1)
    # Validate input arguments
    if length(vals) != length(probs)
        error("pickValProb(): The length of the values and probabilities vectors must be the same.")
    elseif !isapprox(sum(convert.(Float64, probs)), 1, atol = 1e3 * eps(Float64))
        error("pickValProb(): The probabilities must sum to unity with a tolerance of about 1000 times epsilon of Float64.")
    end


    # Calculate cumulative probability
    P = cumsum(probs/sum(probs));

    # Generate random values
    randIdx = rand(nVals, 1) * ones(1, length(vals));

    # Get the correct elements
    randIdx = [sum(P .< randIdx[i, :]) + 1 for i = 1:nVals]
    
    # Get and return the corresponding values
    result = vals[randIdx]
    return result
end






"""
Calculate the distance between two points in space
--------------------------------------------------
Author: Achilles Kappis
e-mail: axilleaz@protonmail.com

Date: 19/08/2024 (DD/MM/YYYY)

Copyright: MIT
--------------------------------------------------
Functionality: Calculate distance between positions in 3D space.
--------------------------------------------------
Input arguments

sPts::Matrix{Real}: The 3D position vectors of the starting points. The rows represent points and the columns their Cartesian coordinates [x,y,z] resulting in an Nx3 matrix with N being the number of points.

ePts::Matrix{Real}: The 3D position vectors of the end points. The rows represent points and the columns their Cartesian coordinates [x,y,z] resulting in an Mx3 matrix with M being the number of points.

--------------------------------------------------
Output arguments

dist::Matrix{Real}: The distance between each start and end point. This is an NxM matrix where each row corresponds to the distance between the nth start point and each of the M end points.

--------------------------------------------------
Notes

--------------------------------------------------
"""
function twoPtDist(sPts::Matrix{<:Real}, ePts::Matrix{<:Real})
    # Validate arguments
    if size(sPts, 2) != 3
        error("twoPtDist(): Wrong dimensions of the starting point coordinates. This must be an Nx3 matrix where N is the number of points and the columns correspond to the Cartesian coordinates.")
    elseif size(ePts, 2) != 3
        error("twoPtDist(): Wrong dimensions of the ending point coordinates. This must be an Nx3 matrix where N is the number of points and the columns correspond to the Cartesian coordinates.")
    end
    
    # Calculate distances
    dist = [norm(sPts[sIdx, :] - ePts[eIdx, :]) for sIdx = 1:size(sPts, 1), eIdx = 1:size(ePts, 1)]
    return dist
end
end # Module end
