%% Calculate positions for specific receiver geometries
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 15/12/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate receiver positions for specific receiver
%                geometries.
% --------------------------------------------------
% Input
% 
% gType [char/string]: The type of the receiver geometry. At the moment the
%                      available options are "Single", "ULA", "UCA" and
%                      "Log".
% 
% nSens [numeric] (Optional): The number of sensors. This value is
%                             ignored for the "Single" geometry.
%                             [Default: 4].
% 
% elemDist [numeric] (Optional): The distance between the elements of
%                                the array configuration. For "Single"
%                                it is ignored, for "ULA" corresponds
%                                to the distance between successive
%                                elements, for "UCA" it corresponds to
%                                the radius of the circle on which the
%                                sensors are placed. [Default: 0.1].
% 
% dmaElemDist [numeric] (Optional): The distance between the elements
%                                   of the directional configurations.
%                                   The available configurations are
%                                   "Figure-of-Eight", "Triangle" and
%                                   "Box", where the first comprises of
%                                   two elements with their axis
%                                   parallel to the y-axis, the
%                                   "Triangle" has an addition element
%                                   with positive z-coordinate forming
%                                   an equilateral triangle and the
%                                   "Box" configuration consists of
%                                   three "Figure-of-Eights", one on
%                                   each axis. If the given value is
%                                   not positive or left empty the
%                                   respective output variables are
%                                   empty. [Default: 0.05].
% 
% trans [numeric] (Optional): This is a real vector with three elements,
%                             representing the translation of the geometry
%                             along the Cartesian axes.
%                             [Default: zeros(3, 1)].
% 
% rot [numeric] (Optional): This is a real vector holding the rotation for
%                           the geometry in degrees, along each Cartesian
%                           axis. The rotations are performed clockwise.
%                           [Default: zeros(3, 1)].
% 
% rotOrd [char/string] (Optional): The order the rotations will be applied.
%                                  This can be some permutation of the
%                                  string "xyz" and is not case-sensistive.
%                                  The order corresponds to multiplication
%                                  on the left (i.e. R * v, where "R" is
%                                  the  rotation matrix and "v" a vector to
%                                  be rotated). [Default: "xyz"].
% 
% --------------------------------------------------
% Output
% 
% omniPos [numeric]: This matrix contains the coordinates of the
%                    omnidirectional receivers and has dimensions 3xN,
%                    where N is the number of receivers and the first
%                    dimension corresponds to the x, y and z Cartesian
%                    coordinates.
% 
% fig8Pos [numeric] (Optional): This matrix contains the coordinates of the
%                               Figure-of-Eight elements (two sensors that
%                               can be used to create a figure-of-eight
%                               pattern when combined as a first order
%                               DMA). The dimensions of the matrix are
%                               3xNx2 where N is the number of sensors, the
%                               first dimension corresponds to the x, y
%                               and z Cartesian coordinates and the last
%                               dimension corresponds to the
%                               "Figure-of-Eight" elements. For the
%                               "Single" and "ULA" geometries, the elements
%                               are positioned so that their axis is
%                               parallel to the y-axis and for the "UCA"
%                               geometry, the elements axis passes through
%                               the origin of the geometry.
% 
% triPos [numeric] (Optional): This matrix contains the coordinates of the
%                              "Triangle" geometry where in addition to the
%                              "figure-of-eight" elements, one more is
%                              placed in the middle of ther axis and higher
%                              in order to create an equilateral triangle.
%                              The matrix has dimensions 3xNx3 where N is
%                              the number of microphone positions, the
%                              first dimension corresponds to the x, y and
%                              z Cartesian coordinates and the third
%                              dimension to individual elements. For the
%                              "Single" and "ULA geometries, the axis of
%                              the "Figure-of-Eight" elements on the x-y
%                              plane is parallel to the y-axis. For the
%                              "UCA" geometry the axis passes through the
%                              origin of the geometry. For "Single" and
%                              "ULA", the first element is the one with
%                              positive y-coordinate and for "UCA" the
%                              furthest from the origin. For all
%                              configurations the element with postitive
%                              z-coordinate is the last.
% 
% boxPos [numeric] (Optional): This matrix contains the coordinates of
%                              three "Figure-of-Eight" sensors (two sensors
%                              which can be combined as a first order DMA
%                              to create a figure-of-eight sensor). The
%                              dimensions of the matrix are 3xNx2x3 where N
%                              is the number of sensor positions, the
%                              first dimension corresponds to the x, y and
%                              z Cartesian coordinates. The third dimension
%                              corresponds to the elements of each
%                              figure-of-eight sensor with the first
%                              element being the one having a positive
%                              coordinate value. The last dimension
%                              corresponds to the figure-of-eight sensors
%                              with the first being parallel to the x-axis,
%                              the second to the y-axis and the third to
%                              the z-axis.
% 
% box2DPos [numeric] (Optional): This matrix contains the coordinates of
%                                the elements of "boxPos" lying on the x-y
%                                plane (the z-axis elements are discarded).
%                                The dimensions of the matrix are 3xNx2x2.
% 
% tetPos [numeric]: This matrix contains the positions of a normal
%                   tetrahedron, with four vertices (points) non-parallel
%                   to the Cartesian axes. The dimensions of the matrix are
%                   3xNx4 where N is the number of measurement positions,
%                   the first dimension is the x, y, and z Cartesian
%                   coordinates and the last dimension corresponds to the
%                   elements of the configuration. The element indices are:
%                   1) Positive x, y and z, 2) negative x, positive y,
%                   zero z, 3) negative x and y, zero z and 4) negative x,
%                   y and z.
% 
% fig8Vec [numeric]: This is an 3xN matrix containing the positions of
%                    the figure-of-eight elements like:
%                    Pos_1_Elem_1_x, Pos_1_Elem_2_x, Pos_2_Elem_1_x ...
%                    Pos_1_Elem_1_y, Pos_1_Elem_2_y, Pos_2_Elem_1_y ...
%                    Pos_1_Elem_1_z, Pos_1_Elem_2_z, Pos_2_Elem_1_z ...
% 
% triVec [numeric]: This is an 3xN matrix containing the positions of
%                   the triangle elements like:
%                   Pos_1_Elem_1_x, Pos_1_Elem_2_x, Pos_1_Elem_3_x ...
%                   Pos_1_Elem_1_y, Pos_1_Elem_2_y, Pos_1_Elem_3_y ...
%                   Pos_1_Elem_1_z, Pos_1_Elem_2_z, Pos_1_Elem_3_z ...
% 
% boxVec [numeric]: This is an 3xN matrix containing the positions of
%                   the box configuration elements like:
%                   Pos_1_X_Elem_1_x, Pos_1_X_Elem_2_x, Pos_1_Y_Elem_1_x...
%                   Pos_1_X_Elem_1_y, Pos_1_X_Elem_2_y, Pos_1_Y_Elem_1_y...
%                   Pos_1_X_Elem_1_z, Pos_1_X_Elem_2_z, Pos_1_Y_Elem_1_z...
% 
% box2DVec [numeric]: This is an 3xN matrix containing the positions of
%                     the 2D box configuration elements like:
%                     Pos_1_X_Elem_1_x, Pos_1_X_Elem_2_x, Pos_1_Y_Elem_1_x.
%                     Pos_1_X_Elem_1_y, Pos_1_X_Elem_2_y, Pos_1_Y_Elem_1_y.
%                     Pos_1_X_Elem_1_z, Pos_1_X_Elem_2_z, Pos_1_Y_Elem_1_z.
% 
% tetVec [numeric]: This is an 3xN matrix containg the positions of the
%                   normal tetrahedron configuration like:
%                   Pos_1_Elem_1_x, Pos_1_Elem_2_x, Pos_1_Elem_3_x ...
%                   Pos_1_Elem_1_y, Pos_1_Elem_2_y, Pos_1_Elem_3_y ...
%                   Pos_1_Elem_1_z, Pos_1_Elem_2_z, Pos_1_Elem_3_z ...
% 
% --------------------------------------------------
% Notes
% 
% - Dependencies: rotMat3d() to rotate the configurations
% 
% --------------------------------------------------
function [omniPos, fig8Pos, triPos, boxPos, box2DPos, tetPos, fig8Vec, triVec, boxVec, box2DVec, tetVec] = rcvGeo(gType, nSens, elemDist, dmaElemDist, trans, rot, rotOrd)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 7);
    nargoutchk(0, 11);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(gType, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Geometry type", 1);
    validatestring(gType, ["Single", "ULA", "UCA", "Log"], mfilename, "Geometry type", 1);

    % Validate optional arguments
    if nargin < 7 || (nargin > 6 && isempty(rotOrd))
        rotOrd = "xyz";
    end
        
    if nargin > 5 && ~isempty(rot)
        validateattributes(rot, {'numeric'}, {'vector', 'nonempty', 'nonnan', 'finite', 'real', 'numel', 3}, mfilename, "Geometry rotation around the Cartesian axes", 6);
    else
        rot = zeros(3, 1);
    end

    if nargin > 4 && ~isempty(trans)
        validateattributes(trans, "numeric", {'vector', 'real', 'nonnan', 'finite', 'nonempty', 'numel', 3}, mfilename, "The translation of the the geometry", 7);
    else
        trans = zeros(3, 1);
    end

    if nargin > 3 && ~isempty(dmaElemDist)
        validateattributes(dmaElemDist, "numeric", {'scalar', 'real', 'nonnan', 'finite'}, mfilename, "Directional configurations inter-element distance", 4);
    else
        dmaElemDist = 0.05;
    end

    if nargin > 2 && ~isempty(elemDist)
        validateattributes(elemDist, "numeric", {'scalar', 'real', 'positive', 'nonnan', 'finite'}, mfilename, "Array inter-element distance", 3);
    else
        elemDist = 0.1;
    end
    
    if nargin > 1 && ~isempty(nSens)
        validateattributes(nSens, "numeric", {'scalar', 'positive', 'real', 'nonnan', 'finite'}, mfilename, "Number of sensors", 2);
    else
        nSens = 4;
    end


    % ====================================================
    % Calculate omnidirectional sensor positions
    % ====================================================
    % Omni sensor coordinates
    switch lower(gType)
        case "single"
            omniPos = [0; 0; 0];
        case "ula"
            omniPos = elemDist * ((nSens - 1)/2) * linspace(-1, 1, nSens);
            omniPos = [omniPos; zeros(2, nSens)];
        case "uca"
            angsOfRot = (0:1/nSens:(1 - 1/nSens)) * 2 * pi;
            [x, y] = pol2cart(angsOfRot, elemDist);
            omniPos = [x; y; zeros(size(x))];
    end

    % ====================================================
    % Calculate directional configurations sensor positions
    % ====================================================
    % Check whether we have to calculate the directional configuration sensor position
    if dmaElemDist <= 0 || isempty(dmaElemDist)
        fig8Pos = []; fig8Vec = [];
        triPos = []; triVec = [];
        boxPos = []; boxVec = [];
        box2DPos = []; box2DVec = [];
        tetPos = []; tetVec = [];
        
        % Translate
        if nargout > 0
            omniPos = omniPos + trans(:);
        end
        return;
    end

    % Figure-of-Eight positions
    if nargout > 1
        switch lower(gType)
            case {"single", "ula"}
                fig8Pos = omniPos + [0; dmaElemDist/2; 0];
                fig8Pos = cat(3, fig8Pos, omniPos - [0; dmaElemDist/2; 0]);
            case "uca"
                [x, y] = pol2cart(angsOfRot, elemDist + dmaElemDist/2);
                fig8Pos = [x; y; zeros(size(x))];
                [x, y] = pol2cart(angsOfRot, elemDist - dmaElemDist/2);
                fig8Pos = cat(3, fig8Pos, [x; y; zeros(size(x))]);
        end
    end
    
    % Triangle positions
    if nargout > 2
        triPos = cat(3, fig8Pos, omniPos + [0; 0; dmaElemDist * sqrt(3)/2]); % Add an element to create an equilateral triangle
        triPos = triPos - [0; 0; dmaElemDist * tand(30)/2]; % Move it down so that the array centre will be at the coordinates of the omni microphone
    end

    % Box positions
    if nargout > 3
        switch lower(gType)
            case {"single", "ula"}
                % X-axis figure-of-eight elements
                tmpBox = omniPos + [dmaElemDist/2; 0; 0];
                tmpBox = cat(3, tmpBox, omniPos - [dmaElemDist/2; 0; 0]);
                boxPos = cat(4, tmpBox, fig8Pos);
            case "uca"
                % X-Y plane perpendicular to the radial direction
                % figure-of-eight elements (the "X-axis figure-of-eight")
                [x, y] = pol2cart(angsOfRot + deg2rad(90), dmaElemDist/2);
                tmpBox = [x; y; zeros(size(x))];
                [x, y] = pol2cart(angsOfRot - deg2rad(90), dmaElemDist/2);
                tmpBox = cat(3, tmpBox, [x; y; zeros(size(x))]);
                tmpBox = omniPos + tmpBox;
                boxPos = cat(4, tmpBox, fig8Pos);
        end

        % Z-axis figure-of-eight elements (common to all geometries)
        tmpBox = omniPos + [0; 0; dmaElemDist/2];
        tmpBox = cat(3, tmpBox, omniPos - [0; 0; dmaElemDist/2]);
        boxPos = cat(4, boxPos, tmpBox);
    end

    % 2D box positions
    if nargout > 4
        box2DPos = boxPos; % Get all elements from box
        box2DPos(:, :, :, 3:3:end) = []; % Get rid of the z-axis elements
    end

    % Normal tetrahedron positions
    if nargout > 5
        % Generate normal tetrahedron with lower face parallel to x-y plane
        % and edge length sqrt(8/3) [from: https://en.wikipedia.org/wiki/Tetrahedron]
        tmpPos = [sqrt(8/9), -sqrt(2/9), -sqrt(2/9), 0; ...
                   0, sqrt(2/3), -sqrt(2/3), 0; ...
                   -1/3, -1/3, -1/3, 1];

        % Set edge length equal to the given DMA inter-element distance
        tmpPos = dmaElemDist * tmpPos/vecnorm(tmpPos(:, 1) - tmpPos(:, 2));

        switch lower(gType)
            case "ula"
                % Rotate the tetrahedrals on the positive side of the y-axis
                rotIdx = sum(omniPos(1, :) > 0); % Calculate how many tetrahedrals we have to rotate

                % Go through the tetrahedrals
                for measPosIdx = size(omniPos, 2):-1:1
                    % Calculate rotation angle
                    if measPosIdx > rotIdx
                        tetRot = 60;
                    else
                        tetRot = 0;
                    end

                    % Rotate and position
                    tetPos(:, measPosIdx, :) = omniPos(:, measPosIdx) + rotMat3d(0, 0, tetRot, [], true) * tmpPos;
                end
            case "uca"
                % Get the angle of the positions on the circle
                az = rad2deg(cart2sph(omniPos(1, :), omniPos(2, :), omniPos(3, :)));
                
                % Rotate and position tetrahedrals
                for measPosIdx = numel(az):-1:1
                    tetPos(:, measPosIdx, :) = omniPos(:, measPosIdx) + rotMat3d(0, 0, az(measPosIdx), [], true) * tmpPos;
                end
            case "single"
                tetPos = omniPos + rotMat3d(30, 30, 0, [], true) * tmpPos;
        end
    end

    % ====================================================
    % Translate and rotate geometries
    % ====================================================
    for mIdx = size(omniPos, 3):-1:1
        omniPos(:, :, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) * omniPos(:, :, mIdx) + trans(:);
    end

    if exist("fig8Pos", "var")
        for mIdx = size(fig8Pos, 3):-1:1
            fig8Pos(:, :, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) * fig8Pos(:, :, mIdx) + trans(:);
        end
    
        for mIdx = size(triPos, 3):-1:1
            triPos(:, :, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) * triPos(:, :, mIdx) + trans(:);
        end
    
        for mIdx = size(boxPos, 4):-1:1
            for mmIdx = size(boxPos, 3):-1:1
                boxPos(:, :, mmIdx, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) *  boxPos(:, :, mmIdx, mIdx) + trans(:);
            end
        end
    
        for mIdx = size(box2DPos, 4):-1:1
            for mmIdx = size(box2DPos, 3):-1:1
                box2DPos(:, :, mmIdx, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) * box2DPos(:, :, mmIdx, mIdx) + trans(:);
            end
        end
    
        for mIdx = size(tetPos, 3):-1:1
            tetPos(:, :, mIdx) = rotMat3d(rot(1), rot(2), rot(3), rotOrd, true) * tetPos(:, :, mIdx) + trans(:);
        end

        % ====================================================
        % Calculate additional output arguments
        % ====================================================
        % Provide a Nx3 "Figure-of-Eight" matrix
        if nargout > 6
            fig8Vec = reshape(permute(fig8Pos, [1, 3, 2]), 3, []);
        end
    
        % Provide a Nx3 "Triangle" matrix
        if nargout > 7
            triVec = reshape(permute(triPos, [1, 3, 2]), 3, []);
        end
    
        % Provide a Nx3 "Box" matrix
        if nargout > 8
            boxVec = reshape(permute(boxPos, [1, 3, 4, 2]), 3, []);
        end
        
        % Provide a Nx3 "2DBox" matrix
        if nargout > 9
            box2DVec = reshape(permute(box2DPos, [1, 3, 4, 2]), 3, []);
        end
    
        % Provide a Nx3 Tetrahedron matrix
        if nargout > 10
            tetVec = reshape(permute(tetPos, [1, 3, 2]), 3, []);
        end
    end
end