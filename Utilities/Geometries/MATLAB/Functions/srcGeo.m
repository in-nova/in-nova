%% Calculate positions for specific source geometries
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 14/11/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate source positions for specific source geometries.
% --------------------------------------------------
% Input
% 
% gType [char/string]: The type of the source geometry. At the moment the
%                      available options are "Single", "Causal",
%                      "Anti-Causal", "Side", "Diagonal", "Diffuse" and
%                      "Directional", "Circle".
% 
% srcLen [numeric]: It is used for all but the "Diffuse" and "Circle"
%                   geometries. For the "Single" geometry it specifies
%                   the x coordinate of the source and for all other
%                   geometries the length of the source array.
% 
% originDist [numeric]: For the "Diffuse" and "Circle" geometries, this is
%                       the radius of the sphere on whose surface the
%                       sources are placed. For the rest of the geometries,
%                       it specifies the distance between the origin and
%                       the centre of the source array.
% 
% ang [numeric] (Optional): It is used for the creation of "Diagonal" and
%                           "Diffuse" geometries (for the rest is ignored
%                           and can even be set to []). For "Diagonal" is
%                           the angle for which the source array is rotated
%                           (clockwise on the x-y plane) and for the
%                           "Diffuse" field it is the angle between sources
%                           on the sphere. Currently, this is the ONLY way
%                           to create a "Diffuse" field and so it is
%                           necessary for this geometry. The value is in
%                           degrees.
%                           [Diagonal default: 45, Diffuse default: 10]
% 
% nSrc [numeric] (Optional): Used for all but the "Diffuse" and
%                            "Single" geometries and dictates the
%                            number of sources that will comprise the
%                            source array. It is ignored if "Diffuse"
%                            or "Single" is chosen as the gType. For
%                            the "directional" configuration, it
%                            defines the number of neighbouring sources
%                            picked for the dominant sources.
%                            [Default: 5]
% 
% azim [numeric] (Optional): Used for the directional source configuration.
%                            It is a vector with the azimuthal angles, in
%                            degrees, of the "central" dominant sources.
%                            [Default: 90]
% 
% elev [numeric] (Optional): Used for the directional source configuration.
%                            It is a vector with the elevation angles of
%                            the "central" dominant sources. It must have
%                            the same number of elements as the "azim"
%                            parameter. [Default: 0]
% 
% SNR [numeric] (Optional): Used for the directional source configuration.
%                           It scales the dominant and background source
%                           strengths so that the resulting SNR at the
%                           origin is equal to the given value.
%                           [Default: Inf]
% 
% --------------------------------------------------
% Output
% 
% sPos [numeric]: The matrix with the source coordinates in the Cartesian
%                 system. The matrix has dimensions 3xN, where N is the
%                 number of sources and the first dimension correspons
%                 to the x, y and z Cartesian coodrinates.
% 
% Q [numeric]: The source strengths for the directional source
%              configuration. For all other configurations this parameter
%              is empty.
% 
% domIdx [numeric]: The dominant source indices in the directional source
%                   configuration. This may be used to extract the
%                   Cartesian coordinates of the dominant sources as well
%                   as their strenghts.
% 
% 
% --------------------------------------------------
% Notes
% 
% - The "Directional" source configuration creates a diffuse field
%   configuration and from those sources, the ones that fall closer to the
%   given azimuthal and elevation angles are picked to be the "dominant"
%   ones.
%   The "nSrc" parameter defines how many neighbouring sources will be
%   used for the generation of the "directional" field. For example if one
%   azimuthal and elevation angle is chosen and sourceLen is equal to 3,
%   the source with azimuthal and elevation angles closer to the given ones
%   will be chosen and two neighbouring (on the horizontal plane) will be
%   chosen to act as "dominant" sources too. This allows for "distributed"
%   sources to be used and decrease the coherency of a "directional"
%   source.
% 
% Dependencies: - ptsOnSphere(): For the calculation of the "Diffuse" and
%                                "Directional" geometry source positions.
%               - rotMat3d(): To rotate the source configurations.
% 
% --------------------------------------------------
function [sPos, Q, domIdx] = srcGeo(gType, srcLen, originDist, ang, nSrc, azim, elev, SNR)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(3, 8);
    nargoutchk(0, 3);


    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(gType, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, "Geometry type", 1);
    validatestring(gType, ["Single", "Causal", "Anti-Causal", "AntiCausal", "Side", "Diagonal", "Diffuse", "Directional", "Circle"], mfilename, "Geometry type", 1);

    validateattributes(srcLen, "numeric", {'scalar', 'real', 'nonnan', 'nonnegative', 'finite', 'nonempty'}, mfilename, "Source length", 2);
    validateattributes(originDist, "numeric", {'scalar', 'nonnegative', 'real', 'nonnan', 'finite', 'nonempty'}, mfilename, "Distance from the origin", 3);

    % Validate optional arguments
    if nargin > 3 && ~isempty(ang)
        validateattributes(ang, "numeric", {'scalar', 'real', 'nonnan', 'finite', 'nonempty'}, mfilename, "Angle value", 4);

        if strcmpi(gType, "Diffuse")
            if ang == 0
                error("For the Diffuse geometry a non-zero value must be given for angle.");
            elseif ang < 0
                warning("For the Diffuse geometry the angle must be positive. A negative value is given so its absolute value will be used.");
            end
        end
    else
        if strcmpi(gType, "Diffuse") || strcmpi(gType, "Directional")
            ang = 10;
        else
            ang = 45;
        end
    end

    if nargin > 4 && ~isempty(nSrc)
        validateattributes(nSrc, "numeric", {'scalar', 'real', 'nonnegative', 'nonnan', 'finite'}, mfilename, "Number of sources", 5);
    else
        nSrc = 5;
    end

    if nargin > 5 && ~isempty(azim)
        validateattributes(azim, "numeric", {'vector', 'real', 'nonnan', 'finite'}, mfilename, "Directional field azimuthal angles", 6);

        azim(azim > 180) = 360 - azim(azim > 180);
        azim(azim < -180) = 360 + azim(azim < -180);
        azim = deg2rad(azim);
    else
        azim = 0;
    end

    if nargin > 6 && ~isempty(elev)
        validateattributes(elev, "numeric", {'vector', 'real', 'nonnan', 'finite'}, mfilename, "Directional field elevation angles", 7);

        if numel(elev) ~= numel(azim)
            error("Elevation angles vector must have the same number of elements as the azimuth angles vector parameter.");
        end

        elev(elev > 90) = 90 - elev(elev > 90);
        elev(elev < -90) = 90 + elev(elev < -90);
        elev = deg2rad(elev);
    else
        elev = zeros(length(azim), 1);
    end

    if nargin > 7 && ~isempty(SNR)
        validateattributes(SNR, "numeric", {'scalar', 'real', 'nonnan'}, mfilename, "Directional field SNR", 8);
    else
        SNR = Inf;
    end


    % ====================================================
    % Calculate source positions
    % ====================================================
    if strcmpi(gType, "Single")
        sPos = [srcLen; originDist; 0];
        domIdx = [];
    elseif sum(strcmpi(gType, ["Causal", "Anti-Causal", "AntiCausal", "Side", "Diagonal"])) > 0
        if srcLen ~= 0
            sPos = [linspace(-srcLen/2, srcLen/2, nSrc); originDist * ones(1, nSrc); zeros(1, nSrc)];
        else
            sPos = [srcLen; originDist; 0];
        end

        if nSrc ~= 0
            switch lower(gType)
                case {"anti-causal", "anticausal"}
                    sPos = rotMat3d(0, 0, 180, [], true) * sPos; % Rotate 180 degrees (anti-clockwise)
                case "side"
                    sPos = rotMat3d(0, 0, 90, [], true) * sPos; % Rotate 90 degrees (anti-clockwise)
                case "diagonal"
                    sPos = rotMat3d(0, 0, -90 + ang, [], true) * sPos; % Rotate specified degrees (anti-clockwise)
            end
        end
        domIdx = [];
    elseif sum(strcmpi(gType, ["Diffuse", "Directional"]) > 0)
        sPos = ptsOnSphere(originDist, ang); % Calculate the source positions on the surface of the sphere

        if strcmpi(gType, "Directional") && nSrc ~= 0
            % Get angles of sources
            [az, el] = cart2sph(sPos(:, 1), sPos(:, 2), sPos(:, 3));

            domIdx = [];
            % Go through the angles
            for angIdx = length(azim):-1:1
                closestVal = abs(elev(angIdx) - el);
                elIdx = find((closestVal - 1e-5) <= min(closestVal));

                closestVal = abs(azim(angIdx) - az(elIdx));
                closestIdx = elIdx((closestVal - 1e-5) <= min(closestVal));
                closestIdx = closestIdx(1); % Make sure to get only one index if there are two closest value candidates

                % Take care of "distributed" sources
                if nSrc == 1
                    domIdx = cat(1, domIdx, closestIdx);
                else
                    for distSrcIdx = -ceil(nSrc/2)+1:floor(nSrc/2)
                        tempIdx = closestIdx + distSrcIdx;

                        if tempIdx > max(elIdx)
                            tempIdx = min(elIdx) + mod(tempIdx, max(elIdx)) - 1;
                        elseif tempIdx < min(elIdx)
                            tempIdx = max(elIdx) + mod(tempIdx, -min(elIdx)) + 1;
                        end
                        domIdx = cat(1, domIdx, tempIdx);
                    end
                end
            end
            domIdx = unique(domIdx); % Make sure there's no duplicates
        else
            domIdx = [];
        end
    elseif strcmpi(gType, "circle")
            sPos = (0:2 * pi/nSrc:2 * pi - 1/nSrc);
            [sPosX, sPosY] = sph2cart(sPos, 0, originDist);
            sPos = [sPosX; sPosY; zeros(size(sPosX))];
            domIdx = [];
    end

    % Calculate source strengths
    if nargout > 1
        % Set the amplitude of the "weak" sources
        Q = (10^(-SNR/10))/sum(~ismember(1:size(sPos, 2), domIdx)) * ones(size(sPos, 2), 1);

        % Set the amplitude of the dominant sources
        if ~isinf(SNR)
            Q(domIdx) = 1./length(domIdx);
        else
            Q(domIdx) = 1;
        end
    else
        Q = [];
    end
end