%% Windowed-sinc fractional delay
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 21/10/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate a fractional delay of a signal with
%                windowed-sinc FIR filter.
% --------------------------------------------------
% Input
% 
% del [numeric]: The delay in samples. This must be a real scalar.
% 
% len [numeric] (Optional): This is the length of the filter. This must be
%                           a positive real integer. If this value is less
%                           than the absolute value of "del", the filter    
%                           length equals the absolute value of the "del",
%                           rounded up. [Default: ceil(abs(del))].
% 
% winFun [string/char, numeric] (Optional): Windowing functions to be
%                                           applied to the truncated sinc
%                                           function. The available options
%                                           are "Rectangular", "Hann" and
%                                           "Hamming" and are not
%                                           case-sensitive. Additionally, a
%                                           non-negative scalar can be
%                                           given to be used as the
%                                           parameter for a Kaiser window.
%                                           [Default: "Rectangular"].
% 
% sig [numeric] (Optional): The signal(s) to be delayed in the time-domain.
%                           This can be a matrix with each column being a
%                           different signal. Only real-valued signals are
%                           accepted. [Default: []].
% 
% sigLen [string/char] (Optional): The length of the resulting signal. This
%                                  argument can be either "Full" or "Same"
%                                  (not case-sensitive). If "Same", the
%                                  delayed signals will have the same
%                                  length as the original. If "Full" is
%                                  provided, the full result of the
%                                  convolution is provided. If the number
%                                  of output arguments is less than 3, this
%                                  argument is ignored. [Default: "Full"].
% 
% --------------------------------------------------
% Output
% 
% sincFilt [numeric]: The fractional delay windowed-sinc FIR filter(s).This
%                     argument is a vector with the FIR filter. This is the
%                     causal version of the filter. If the non-causal
%                     version is required, shift to the left by "causDel".
% 
% causDel [numeric]: The delay required to make the filter causal. This
%                    value corresponds to half the length of the filter.
%                    The causal part of the filter are at indices
%                    (causDel:len).
% 
% dSig [numeric]: The delayed signal(s). The length of each signal is equal
%                 to that of the original. If the delay is longer than the
%                 original signal vector, a zeroed vector is returned (with
%                 possible artefacts from the filtering process). Only
%                 returned if a signal is provided, otherwise an empty
%                 array is returned.
% 
% --------------------------------------------------
% Notes
% 
% - The function uses time-domain convolution, which can be quite slow for
%   long signals. If the "dSig" is not required this calculation is
%   omitted so, when the signals are not needed (or want to perform
%   frequency-domain convolution manually), use only one output argument.
% 
% --------------------------------------------------
function [sincFilt, causDel, dSig] = winSincFracDel(del, len, winFun, sig, sigLen)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 5);
    nargoutchk(0, 3);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(del, "numeric", {'scalar', 'real', 'nonnan', 'nonempty', 'finite'}, mfilename, "Delay in samples", 1);

    % Validate optional arguments
    if nargin > 1 && ~isempty(len)
        validateattributes(len, "numeric", {'scalar', 'integer', 'real', 'finite', 'nonnan', 'nonnegative'}, mfilename, "Filter length", 2);

        if len < del
            warning("winSincFracDel(): The length of the filter cannot be less than the delay, setting equal to 'delay',");
            len = ceil(abs(del));
        end
    else
        len = ceil(abs(del));
    end

    if nargin > 2 && ~isempty(winFun)
        validateattributes(winFun, {'char', 'string', 'numeric'}, {'nonempty'}, mfilename, "Window to be applied to the truncated sinc function (or the alpha parameter for a Kaiser window)", 3);

        if ischar(winFun) || isStringScalar(winFun)
            validatestring(winFun, ["Rectangular", "Hann", "Hamming"], mfilename, "Window to be applied to the truncated sinc function", 3);
            winFun = string(winFun);
        elseif isscalar(winFun)
            validateattributes(winFun, {'numeric'}, {'nonempty', 'nonnan', 'finite', 'nonnegative', 'real', 'scalar'}, mfilename, "Alpha parameter for a Kaiser window to be applied to the truncated sinc function", 3);
        end
    else
        winFun = "rectangular";
    end

    if nargin > 3 && ~isempty(sig)
        validateattributes(sig, "numeric", {'2d', 'nonempty', 'real'}, mfilename, "Signal(s) to be delayed", 4);

        if size(winFun, 2) == 1
            winFun = repmat(winFun, 1, size(sig, 2));
        else
            if size(winFun, 2) ~= size(sig, 2)
                error("Number of windows must match the number of signals provided");
            end
        end
    else
        sig = zeros(len, 1);
    end

    if nargin > 4 && ~isempty(sigLen)
        validateattributes(sigLen, {'string', 'char'}, {'scalartext', 'nonempty'}, mfilename, "Length of delayed signal vectors/matrix", 5)
        validatestring(sigLen, ["Full", "Same"], mfilename, "Length of delayed signal vectors/matrix", 5);
    else
        sigLen = "Full";
    end

    
    % ====================================================
    % Calculate some parameters
    % ====================================================
    fracDel = rem(del, 1); % Fractional part of the delay


    % ====================================================
    % Generate FIR filter for fractional delay
    % ====================================================
    idx = floor(-len/2 + 1):floor(len/2); % Filter sample indices
    
    causDel = -idx(1) + 1; % The causal delay of the filter
    sincFilt = sinc(idx - fracDel); % Calculate the filter


    % ====================================================
    % Apply windowing
    % ====================================================
    % Generate window
    if isstring(winFun)
        switch lower(winFun)
            case "rectangular"
                win = ones(1, length(idx));
            case "hann"
                win = sin(pi * (idx - fracDel + causDel + 1)/len).^2;
            case "hamming"
                alpha = 25/46;
                win = alpha - (1 - alpha) * cos(2 * pi * (idx - fracDel + causDel + 1)/len);    
        end
    else
        % Kaiser window
        win = besseli(0, pi * winFun * sqrt(1 - (2 * (idx - abs(fracDel))/len).^2))/besseli(0, pi * winFun);
    end
    
    % Apply window to the filter
    sincFilt = sincFilt .* win;
    sincFilt = sincFilt(:);

    % Add the integral delay
    if del >= 0
        sincFilt = [zeros(fix(del), 1); sincFilt(1:end - fix(del))];
    else
        sincFilt = [sincFilt(abs(floor(del)):end); zeros(abs(fix(del)), 1)];
    end


    % ====================================================
    % Filter the signals
    % ====================================================
    if nargout > 2
        for idx = size(sig, 2):-1:1
            dSig(:, idx) = conv(sig, sincFilt.', lower(sigLen));
        end
    end
end