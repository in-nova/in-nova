%% Divide the (linear) spectrum into frequency bands
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 10/05/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Divide the linear frequency spectrum to frequency bands.
% --------------------------------------------------
% Input
% 
% freqVec [numeric]: This is a vector with the frequencies to be divided
%                    into bands. The vector must contain only positive real
%                    frequencies.
% 
% fracBand [numeric] (Optional): The fraction of an octave in which the
%                                given frequencies will be divided. At the
%                                moment this can be either 1 or 3 for
%                                octave or 1/3 octave bands. [Default: 1].
% 
% --------------------------------------------------
% Output
% 
% bandFreq [numeric]: The frequencies defining the bands with the centre
%                     frequencies included. The values are sorted and start
%                     from the lower frequenct of the first band. The
%                     frequencies with index [2:2:end] correspond to the
%                     band centre frequencies and those with [1:2:end] to
%                     the "edge" frequencies. The values correspond to the
%                     "actual" frequency values and may NOT be present in
%                     the original frequency vector ("freqVec" parameter").
%                     All values are rounded to one decimal place even for
%                     internal calculations.
% 
% bandFreqIdx [numeric]: The indices of the frequencies of "bandFreq" in
%                        the original frequency vector ("freqVec"
%                        parameter). The indices may have fractional values
%                        that correspond to the (fractional) indices of the
%                        values in the "bandFreq" array.
% 
% bandFreqRound [numeric]: The frequencies corresponding to the frequencies
%                          in the original frequency vector ("freqVec")
%                          after the indices in "bandFreqIdx" have been
%                          rounded to the nearest integer.
% 
% bandIdxRound [numeric]: The indices corresponding to the frequencies in
%                         the array "bandFreqRound".
% 
% --------------------------------------------------
% Notes
% 
% - The formula for the calculation of the octave and 1/3-octave band
%   central frequencies is taken from "E.M. Salomons, Computational
%   Atmospheric Acoustics", Appendix B4, Kluwer Academic Publishers, 2001.
% 
% - The values returned, as well as the corresponding indices correspond to
%   the "actual" values of the frequencies of interest (centre and edge
%   frequencies that define the bands). This is to allow the user the
%   freedom to treat them as they see fit. Rounding the indices and using
%   them with the original array may be the best option but this is to be
%   decided by the user.
% --------------------------------------------------
function [bandFreq, bandFreqIdx, bandFreqRound, bandIdxRound] = freqBands(freqVec, fracBand)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(1, 3);
    nargoutchk(0, 4);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(freqVec, "numeric", {'vector', 'real', 'nonnegative', 'nonnan', 'nonempty', 'finite'}, mfilename, "The frequency vector", 1);

    % Validate optional arguments
    if nargin > 1 && ~isempty(fracBand)
        validateattributes(fracBand, "numeric", {'scalar', 'nonempty', 'nonnan', 'real', 'positive', 'finite'}, mfilename, "Fraction of octave bands", 2);
        if fracBand ~= 1 && fracBand ~= 3
            error("fracBand must be either '1' or '3'...");
        end
    else
        fracBand = 1;
    end


    % ====================================================
    % Calculate central and edge frequencies
    % ====================================================
    if fracBand == 1
        bandFreq = 1e3 * 2.^(-10 + (10.5:1.5:46.5)/3);
    else
        bandFreq = 1e3 * 2.^(-10 + (9.5:0.5:45.5)/3);
    end
    bandFreq = round(bandFreq, 1);

    % Get rid of frequencies outside the given spectrum
    idx = find(freqVec(end) <= bandFreq, 1, "first");
    if bandFreq(idx) == freqVec(end)
        idx = idx + 1;
    end
    bandFreq(idx:end) = [];

    idx = find(freqVec(1) >= bandFreq, 1, "last");
    if bandFreq(idx) == freqVec(1)
        idx = idx - 1;
    end
    bandFreq(1:idx) = [];

    % Linearly interpolate indices to get the "actual" (fractional) index
    for idx = length(bandFreq):-1:1
        tmpMaxIdx = find(bandFreq(idx) <= freqVec, 1, "first");
        tmpMinIdx = find(bandFreq(idx) >= freqVec, 1, "last");

        if tmpMinIdx ~= tmpMaxIdx
            bandFreqIdx(idx) = interVal([tmpMinIdx, tmpMaxIdx], [freqVec(tmpMinIdx), freqVec(tmpMaxIdx)], bandFreq(idx));
        else
            bandFreqIdx(idx) = tmpMaxIdx;
        end
    end
    

    % Get integral values for the indices and corresponding frequencies
    bandIdxRound = round(bandFreqIdx);
    bandFreqRound = freqVec(bandIdxRound);
end


%% Utility functions
function iVal = interVal(x, y, yi)
    iVal = x(1) + ((yi - y(1)) * (x(2) - x(1)))/(y(2) - y(1));
end