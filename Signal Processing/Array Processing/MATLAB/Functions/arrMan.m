%% Calculate the array manifold (steering vector) of an arbitrary array
% --------------------------------------------------
% Author: Achilles Kappis
% e-mail: axilleaz@protonmail.com
%
% Date: 05/10/2024 (DD/MM/YYYY)
%
% Copyright: MIT
% --------------------------------------------------
% Functionality: Calculate the array manifold (steering vector) of an
%                arbitrary array.
% --------------------------------------------------
% Input
% 
% mPos [numeric]: The positions of the sensors in Cartesian coordinates.
%                 This must be an 3xM matrix where M denotes the number of
%                 sensors and for each sensor the coordinates are in
%                 [x; y; z] format.
% 
% dirs [numeric]: These are the directions for which the array manifold
%                 will be calculated. This must be an 2xN matrix where N is
%                 the number of directions and for each direction the
%                 azimuth and elevation/inclination is given. The values
%                 must be provided in radians. The directions must be
%                 pointing towards the incoming plane waves and not towards
%                 the array/origin.
% 
% k [numeric]: The wavenumbers of interest. This must be a vector
%              containing all the wavenumbers for which the array manifold
%              will be calculated.
% 
% --------------------------------------------------
% Output
% 
% am [numeric]: The array manifold/steering vector of the array for the
%               directions/angles and wavenumbers provided. The dimensions
%               of the argument are MxNxK where M is the number of sensors,
%               N the number of directions/angles and K the number of
%               frequencies/wavenumbers.
% 
% --------------------------------------------------
% Notes
% 
% --------------------------------------------------
function am = arrMan(mPos, dirs, k)
    % ====================================================
    % Check for number of arguments
    % ====================================================
    narginchk(3, 3);
    nargoutchk(0, 1);

    % ====================================================
    % Validate input arguments
    % ====================================================
    % Validate mandatory arguments
    validateattributes(mPos, "numeric", {'2d', 'finite', 'nonnan', 'nonempty', 'real', 'nrows', 3}, mfilename, "Position of sensors", 1);
    validateattributes(dirs, "numeric", {'2d', 'finite', 'nonnan', 'nonempty', 'real', 'nrows', 2}, mfilename, "Directions of interest", 2);
    validateattributes(k, "numeric", {'vector', 'finite', 'nonnan', 'nonempty', 'nonnegative', 'real'}, mfilename, "Wavenumbers of interest", 3);

    % =============================================
    % Calculate the array manifold
    % =============================================
    % Get Cartesian coordinates of directions
    [x, y, z] = sph2cart(dirs(1, :), dirs(2, :), ones(size(dirs(1, :))));

    % Inner product of directions and sensor position
    am = [x; y; z].' * mPos;

    % Multiply with wavenumbers
    am = am.' .* permute(k(:), [3, 2, 1]);

    % Calculate the exponential
    am = exp(1i * am);
end