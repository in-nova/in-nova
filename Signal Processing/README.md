# Signal Processing
The *Signal Processing* part of the codebase is somewhat generic. It can accommodate all algorithms and implementations that do not fall under the *Utilities* category but are generic enough to be used in various cases. Currently a limited number of functions are implemented. To accommodate the somewhat wide field of *Array Processing* a different section is created in the *Signal Processing* section.

## Sub-sections

At the moment there are two sub-sections. The first is the "Generic" which accommodates generic Signal Processing functions and algorithms and the second is the "Array Processing" which accommodates algorithms that are related to the processing of signals from transducer (either receivers or transmistters) arrays.

### Current structure

The current structure of the section is summarised below.

- Generic
  - Windowed Sinc Fractional Delay FIR filter
  - Octave band frequencies calculation
  - Fractional octave band smoothing of spectra
- Array Processing
  - First order Differential Microphone Array
- Measurements
  - Estimation of impulse response from sweep measurements
